# ----------------------------------------------------------------------------

# 
#  \file    CSO2Mask.py
#  \author     Hakim Achterberg
#  \date    2018-06-14
#
#  

# ----------------------------------------------------------------------------

import argparse
import json
import os
from mevis import *


def find_csos(data, result=None, key=None, desired_type=None):
    if result is None:
        result = []
    
    if isinstance(data, dict):
        if 'csos' in data and (desired_type is None or key == desired_type):
            # FOUND CSO!
            print('Found CSO!')
            result.append(data['csos'])
        else:
            print('No CSO!')
            for key, value in data.items():
                print('Checking {}'.format(key))
                find_csos(value, result, key=key, desired_type=desired_type)

    if isinstance(data, list):
        for item in data:
            find_csos(item, result, desired_type=desired_type)

    return result


def draw_cso(cso_data):
    cso_gliosis_list = ctx.module("csos_glio").field("outCSOList").object()
    interpolation_update = ctx.module("CSOShapeBasedInterpolation").field('update')

    cso_gliosis_list.removeCSOs()
    for value in cso_data:
        cso_gliosis_list.addClosedSpline(value['seed_points'], True, "spline")
    cso_gliosis_list.notifyObservers(cso_gliosis_list.NOTIFICATION_CSO_FINISHED)
    interpolation_update.touch()


def load_reference_image(filename):
    reader = ctx.module("itkImageFileReader")
    reader.field('fileName').value = os.path.abspath(filename)
    reader.field('open').touch()
    
    
def save_cso(cso_data, filename):
    draw_cso(cso_data)
    writer = ctx.module("itkImageFileWriter")
    writer.field('fileName').value = filename
    writer.field('save').touch()
    
    
def run(args, path):
    parser = argparse.ArgumentParser(prog='CSO2Mask', description="Convert CSOs from fields file to binary masks")
    parser.add_argument("--fields", metavar="FIELDS.json", required=True, help="JSON file containing the fields information")
    parser.add_argument("--reference", metavar="REFERENCE.nii.gz", required=True, help="The reference image defining the image space used")
    parser.add_argument("--outdir", metavar="DIR", required=True, help="Output directory")
    args = parser.parse_args(args)
    
    with open(args.fields) as fin:
        data = json.load(fin)
        
    load_reference_image(args.reference)
        
    csos_loss = find_csos(data, desired_type="infarct_segmentation_tissue_loss")
    csos_glio = find_csos(data, desired_type="infarct_segmentation_gliosis")
    
    for nr, (cso_loss, cso_glio) in enumerate(zip(csos_loss, csos_glio)):
        if cso_loss:
            save_cso(cso_loss, os.path.join(args.outdir, '{:03d}_loss_mask.nii.gz'.format(nr)))
        if cso_glio:
            save_cso(cso_glio, os.path.join(args.outdir, '{:03d}_glio_mask.nii.gz'.format(nr)))