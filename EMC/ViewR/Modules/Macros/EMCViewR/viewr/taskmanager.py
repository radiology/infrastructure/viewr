from collections import MutableSequence
from urllib import parse as up

import requests

from . import globals, log
from .globals import ctx, MLAB
from .helpers import get_username, starting_template, end_template, update_netrc_entry, get_netrc
from .task import Task
import taskclient
from taskclient.exceptions import TaskManagerResponseError, TaskManagerConnectionError, TaskManagerNoAuthError


class TaskManager(MutableSequence):
    def __init__(self, min_history=0, max_history=2, min_future=2, max_future=4):
        self._log = log.config_logger('TaskManager', level=20, filename=None)
        self._tasks = []
        self._current_item = None
        self.min_history = min_history
        self.max_history = max_history
        self.min_future = min_future
        self.max_future = max_future
        self.tags_map = None
        self.auth_window = None
        self.current_item = 'START'

        self._task_client = None

    @property
    def task_client(self):
        # Create taskmanager connection
        url = self.get_url()

        if self._task_client is None or self._task_client.original_uri != url:
            if self._task_client is not None:
                self._log.info('Changing task_client from {} to {}'.format(self._task_client.original_uri, url))
            self._log.info('Creating Task Client for {}'.format(url))

            # Get the netrc to validate it, this is needed to avoid potential errors in the taskclient
            get_netrc()
            self._task_client = taskclient.connect(url, user_agent_prefix=globals.user_agent_prefix, logger=self._log)

        return self._task_client

    def clear(self):
        # Make sure the current task is properly destroyed (contexts cleared and everything)
        self.transition(None, self.current_item)

        # Release all tasks
        self.release_all()
        self._current_item = None
        self._tasks = []
    
    # Methods to implement this as a MutableSequence
    def __len__(self):
        return len(self._tasks)

    def __getitem__(self, item):
        return self._tasks[item]

    def __setitem__(self, key, value):
        self._tasks[key] = value
        self._acquire_release()

    def __delitem__(self, key):
        self._log.warning('Removing item {}'.format(key))
        if self._tasks[key] is self.current_item:
            self._log.debug('Call next_item from self.__delitem__')
            self.next_item()

        # Release and remove task
        self._tasks[key].release()
        del(self._tasks[key])
        self._acquire_release()

    def index(self, task):
        return self._tasks.index(task)

    def insert(self, index, value):
        self._tasks.insert(index, value)
        self._acquire_release()

    def extend(self, values):
        self._tasks.extend(values)
        self._acquire_release()

    # Other methods
    def get_tags(self):
        self._log.info("Getting tags")
        url = self.get_url()

        if not url:
            return None

        try:
            tags = self.task_client.get_tags()
        except taskclient.exceptions.TaskManagerNoAuthError:
            self.auth_window = ctx.showWindow("TaskAuthWindow")
            self.auth_window.control("taskmanager_hostname").setTitle(self.get_url())
            self.auth_window.control("callback_field").setTitle("LoadTags")
            return
        except (TaskManagerResponseError, TaskManagerConnectionError):
            return None

        self.tags_map = {x.name: x.uri for x in tags}
        tags_names = [(x.name, x.uri) for x in tags]
        return tags_names
    
    def get_url(self):
        return ctx.field("TaskmanagerURL").value

    def get_project(self):
        project = ctx.field("ProjectFilter").value
        return project or None  # Empty string is replaced by None

    @property
    def current_item(self):
        return self._current_item

    @current_item.setter
    def current_item(self, value):
        self._log.info('SETTING CURRENT ITEM: {}'.format(value))
        if value not in ['START', 'END'] and value not in self._tasks:
            raise ValueError('New item should be in current tasks')
        self._current_item = value
        self._acquire_release()

    def _acquire_release(self):
        self._log.info('ACQUIRE_RELEASE')
        current_index = self.current_index()

        # First check forward tasks so we load in forward direction first
        offset = 0
        for item in self._tasks[max(current_index, 0):]:
            if offset <= self.min_future:
                success = item.acquire()
                if success:
                    offset += 1
            else:
                if offset > self.max_future:
                    item.release()
                offset += 1

        # Second load the history if required
        offset = 1
        for item in self._tasks[max(current_index - 1, 0)::-1]:
            if offset <= self.min_history:
                success = item.acquire()
                if success:
                    offset += 1
            else:
                if offset > self.max_history:
                    item.release()
                offset += 1

    def release_all(self):
        for task in self._tasks:
            if task.acquired:
                task.release()

    def current_index(self):
        if self.current_item == 'START' or self.current_item is None:
            return -1
        elif self.current_item == 'END':
            return len(self._tasks)
        else:
            return self._tasks.index(self.current_item)

    def next_item(self, skip_parent=None, skip_status=None):
        self._log.info('NEXT_ITEM')
        current_index = self.current_index()
        new_index = current_index + 1

        if skip_status is None:
            skip_status = ("aborted", "done")

        # Search forward for first acquirable task
        for task_option in self._tasks[new_index:]:
            if skip_parent and task_option.parent == skip_parent:
                self._log.debug('Skipping task with same parent {} (next_parent=True)'.format(task_option))
                continue

            task_option_status = task_option.status
            # FIXME: this seems double, skip status and != queued?
            if task_option_status in skip_status:
                self._log.debug('Skipping task with unwanted status {} (status in {})'.format(task_option, skip_status))
                continue

            if task_option_status != 'queued':
                self._log.debug('Skipping task with non-queued parent status: {}'.format(task_option))
                continue

            if not task_option.acquire():
                self._log.debug('Could not acquire {}'.format(task_option))
                continue

            return task_option

        # No task found, we hit the end
        return 'END'

    def previous_item(self, skip_parent=None, skip_status=None):
        self._log.info('PREVIOUS_ITEM')
        current_index = self.current_index()
        new_index = current_index - 1

        if skip_status is None:
            skip_status = ("aborted",)

        # Search backward for first acquirable task
        if new_index >= 0:
            for task_option in self._tasks[new_index::-1]:
                if skip_parent and task_option.parent == skip_parent:
                    self._log.debug('Skipping task with same parent {} (next_parent=True)'.format(task_option))
                    continue

                if task_option.status in skip_status:
                    self._log.debug('Skipping aborted task {}'.format(task_option))
                    continue

                if task_option.parent.status != 'queued':
                    self._log.debug('Skipping task with non-queued parent status: {}'.format(task_option))
                    continue

                if not task_option.acquire():
                    self._log.debug('Could not acquire {}'.format(task_option))
                    continue

                return task_option

        # No task found, we hit the start
        return 'START'

    def init_task_transition(self, task, finish=True):
        """
        Initializae the actual transition to a task.
                     
        :param str/int task: The integer index of the task in the taskmanager or END/START
        :param bool finish: Mark current task as finished
        """
        # Make sure editor is not active
        if not globals.editor.deactivate_editor():
            return

        # If required, mark old task as done and handle possible changes in the
        # task list (due to parent being done)
        if finish:
            # Set state (this will possibly resolve the task parent)
            self.current_item.status = 'done'

        # Find next item AFTER task status is changed
        if task == 'NEXT':
            task = self.next_item()
        elif task == 'PREVIOUS':
            task = self.previous_item()

        # Make sure we have an index
        if isinstance(task, Task):
            task = self.index(task)

        # Call the transition via FieldListener to make sure the window is available
        if isinstance(task, int):
            ctx.field('ChangeToTask').value = task
        elif task == 'START':
            self.transition(task, self.current_item)
            self.current_item = task
            starting_template()
        elif task == 'END':
            self.transition(task, self.current_item)
            self.current_item = task
            end_template()
        else:
            self._log.warning('Trying to transition to illegal task value: [{}] {}]'.format(type(task).__name__, task))

    def auth_callback(self):
        hostname = ctx.control("taskmanager_hostname").title()
        realm = ctx.control("taskmanager_realm").title()
        username = ctx.control("taskmanager_username").text()
        password = ctx.control("taskmanager_password").text()
        callback_field = ctx.control("callback_field").title()

        response = requests.get(hostname, auth=(username, password), timeout=(2.0, 5.0))
        if response.status_code != 200:
            html_content = """
              <html>
              <head>
                <style type="text/css">
                  body {{
                    margin: 0px;
                    padding: 0px;
                    font-family: "arial,sans-serif";
                    font-size: 100%;
                  }}
                  .error {{
                    border: 3px solid #f00;
                    background: #ffc0cb;
                    margin: 0 0 0 0;
                    padding: 4px;
                    height: 18px;
                  }}
                </style>
              </head>
              <body>
                <div class='error'>Login information for {host} was incorrect</div>
              </body></html>
            """.format(host=hostname)
            self.auth_window.control("task_login_error_panel").setHtmlFromString(html_content)
            self.auth_window.control("task_login_error_panel").setVisible(True)
            self.auth_window.control("taskmanager_password").setText("")
            return
        else:
            self.auth_window.control("task_login_error_panel").setVisible(False)

        update_netrc_entry(up.urlparse(hostname).netloc, username, password)

        self.auth_window.close()
        self.auth_window = None
        callback_field = ctx.field(callback_field)
        self._log.info('Trigger callback field {}'.format(callback_field))
        callback_field.touch()

    def init_data(self, items=None,
                  tags=None,
                  transition_to_start: bool = True):
        self._log.info('INIT_DATA: tags {}'.format(tags))

        project = self.get_project()
        self._log.debug('project: {}'.format(project))
        self._log.debug('items: {}'.format(items))

        # Track old tasks to keep/remove
        old_parents = {x.uri: x for x in self._tasks}
        reused_task = set()

        if items is None:
            items = []

            if ctx.field("IgnoreUsername").value:
                username = None
            else:
                username = get_username()

            try:
                remote_tasks = self.task_client.get_tasks(
                    user=username,
                    status='queued',
                    project=project,
                    application_name='ViewR'
                )
            except TaskManagerResponseError as exception:
                if exception.status_code == 401:
                    self.auth_window = ctx.showWindow("TaskAuthWindow")
                    self.auth_window.control("taskmanager_hostname").setTitle(self.get_url())
                    self.auth_window.control("callback_field").setTitle("ReloadTasks")
                    return
                else:
                    raise

            self._log.info('Remote tasks: {}'.format(remote_tasks))
            for remote_task in remote_tasks:
                self._log.info('Trying to load task: {}'.format(remote_task))

                # Filter tags
                if tags is not None and not any(tag.uri in tags for tag in remote_task.tags):
                    continue

                if remote_task.uri not in old_parents:
                    self._log.info(f'Creating new Task around {remote_task}')
                    task = Task(remote_task)
                    items.append(task)
                else:
                    # Add old task and mark it as re-used
                    self._log.info(f'Re-using Task around {remote_task}')
                    task = old_parents[remote_task.uri]
                    reused_task.add(remote_task.uri)
                    items.append(task)

        # Release all tasks that will not be re-used (we'll loose access to them after assigning the new tasks)
        for task in self:
            if task.uri not in reused_task and task.acquired:
                self._log.info('[DEBUG] releasing task {}'.format(task.name))
                task.release()

        # Assign new tasks
        self._tasks = list(x if isinstance(x, Task) else Task(**x) for x in items)

        if not isinstance(self.current_item, Task):
            self._log.info('Cannot remove old image readers for {}'.format(self.current_item))
        elif transition_to_start:
            # Clear out currently loaded task
            self._log.info('Removing old image readers for {}'.format(self.current_item))
            self.transition('START', self.current_item)
        elif self.current_item not in self._tasks:
            self._log.info('+++ Need to transition!')

            # Clear out currently loaded task for first task in list
            self._log.info('Removing old image readers for {}'.format(self.current_item))
            if len(self._tasks) > 0:
                new_task = self._tasks[0]
            else:
                new_task = 'START'
            self._log.info(f'Transition to new task {new_task}')
            self.init_task_transition(new_task, finish=False)
        else:
            self._log.info(f'+++ Task {self.current_item} is still in new taskmanager, can stay safely')

    def transition(self, new_item, old_item=None):
        ctx.field('EnableNavigation').value = False
        ctx.field('EnableEditing').value = False

        # Get all resources used by the tasks
        if old_item is not None and isinstance(old_item, Task):
            old_item.template.remove_contexts()
            old_resources = old_item.image_resources()
        else:
            old_resources = set()

        if new_item is not None and isinstance(new_item, Task):
            new_item.template.create_contexts()
            new_resources = new_item.image_resources()
        else:
            new_resources = set()

        # Find the resources that either need to be created or removed, do not touch rest
        to_remove = old_resources - new_resources
        to_create = new_resources - old_resources

        self._log.info('Removing image readers: {}'.format(to_remove))
        self._log.info('Create image_readers: {}'.format(to_create))
        self._log.info('Re-using image readers: {}'.format(old_resources & new_resources))

        for resource in to_remove:
            resource.remove_image_reader()

        for resource in to_create:
            resource.create_image_reader()

        if new_item is not None and isinstance(new_item, Task):
            new_item.mask_annotations()

    def resolve_task(self, task=None, status='aborted'):
        self._log.warning('Updating Task parent {} to {}'.format(task, status))
        if task is None:
            task = self.current_item

        if not isinstance(task, Task):
            self._log.error('Cannot abort a task that is not a valid Task')
            return

        # Set task status
        task.status = status

    def check_safety_new_task(self, task_index):
        # First catch begin and end
        if task_index in ['START', 'END', 'NEXT', 'PREVIOUS']:
            return task_index

        self._log.info('==> TASK INDEX: {}'.format(task_index))
        self._log.info('==> TASKMANAGER LENGTH: {}'.format(len(self)))

        if isinstance(task_index, int):
            task = self[task_index]
        else:
            task = task_index

        if task.parent.status != 'queued':
            # Check if we are moving in a forward of backward direction and move
            # further along that same direction
            new_index = self.index(task)

            # Return next safe task in desired direction
            if new_index >= self.current_index():
                new_item = self.next_item(skip_parent=task.parent)
            else:
                new_item = self.previous_item(skip_parent=task.parent)

            if isinstance(new_item, Task):
                new_item = self.index(new_item)

            return new_item
        else:
            # Parent is safe, just return
            return task_index

    def __repr__(self):
        return '----------\nTasklist:\n{}\n----------\n'.format('\n'.join(str(x) if x is not self.current_item else '{} **'.format(x) for x in self._tasks))

    def __str__(self):
        return repr(self)
