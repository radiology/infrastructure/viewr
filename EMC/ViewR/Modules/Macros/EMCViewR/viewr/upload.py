import datetime
import json
import os
import io
import sys
import time
import urllib.parse

# Add MeVisLab package path
extra_python_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                                  '..', '..', '..', 'Scripts', 'python'))
sys.path = [extra_python_path] + sys.path

# Import xnat
import isodate
import xnat


def upload(source_path, destination_url, resource_data):
    errors = []
    if not os.path.isfile(source_path):
        errors.append('File to upload is not valid file ({})'.format(source_path))

    # Find the correct netrc file to use (also for windows)
    netrc_file = os.path.join('~', '_netrc' if os.name == 'nt' else '.netrc')
    netrc_file = os.path.expanduser(netrc_file)

    # Find host to connect to
    split_url = urllib.parse.urlparse(destination_url)
    host = urllib.parse.urlunparse(split_url._replace(path='', params='', query='', fragment=''))
    path = split_url.path

    timestamp = isodate.datetime_isoformat(datetime.datetime.now())
    timestamp = '_' + timestamp
    path = path.format(timestamp=timestamp)

    with xnat.connect(host, netrc_file=netrc_file, verify=False) as session:
        # Check if resource exists
        resource_path, _ = path.split('/files/', 1)
        try:
            session.get(resource_path)
        except xnat.exceptions.XNATResponseError:
            # Create the resource
            _, resource_name = resource_path.rsplit('/', 1)

            query = {
                'xsiType': "xnat:resourceCatalog",
                'label': resource_name,
                'req_format': 'qa',
            }

            try:
                session.put(resource_path, query=query)
            except xnat.exceptions.XNATResponseError:
                errors.append("Error when trying to create the resource at {}".format(resource_path))

        # Upload file
        try:
            session.upload(path, file_=source_path)
        except Exception as e:
            errors.append("Error encountered when uploading data: {}".format(e))

        # Download file to check validity
        data = io.BytesIO()
        try:
            session.download_stream(path, data)
        except ValueError as e:
            errors.append("Error encountered when downloading data again: {}".format(e))

        # Compare uploaded data with intended data
        with open(source_path, 'rb') as fin:
            original_data = fin.read()
        round_trip_data = data.getvalue()
        validated = original_data == round_trip_data

        if not validated:
            message = "Round trip data transport did not yield correct result!"
            print(message)
            errors.append(message)
        else:
            print("Round trip data validation passed successfully!")

        # Update resource data
        resource_data['original_filename'] = source_path
        resource_data['uploaded'] = timestamp
        resource_data['errors'] = errors
        resource_data['success'] = len(errors) == 0
        resource_data['edited_filename'] = None
        resource_data['validated'] = validated


def main():
    if len(sys.argv) == 3:
        _, order, result = sys.argv
        print('Reading {}'.format(order))
        with open(order) as order_in:
            resource_data = json.load(order_in)
    else:
        # Create a task and download scans
        print('Reading stdin')
        resource_data = json.load(sys.stdin)

    print('Found resource data: {}'.format(resource_data))

    # Upload the data
    upload(resource_data['edited_filename'],
           resource_data['source_url'],
           resource_data)

    print('Upload successful: {}'.format(os.path.isfile(resource_data['original_filename'])))

    if len(sys.argv) == 3:
        for _ in range(3):
            print('Writing result data to {}'.format(result))
            with open(result, 'w') as result_out:
                json.dump(resource_data, result_out)

            for _ in range(10):
                if os.path.exists(result):
                    break
                time.sleep(0.5)
            else:
                print('Could not find result file after 10 tries')

            if os.path.exists(result):
                break
            print('Writing appears to have failed.')
        else:
            print('Could NOT write data to disk! ABORTING')
    else:
        print('Writing result data to stdout')
        print(json.dumps(resource_data))


if __name__ == '__main__':
    main()
