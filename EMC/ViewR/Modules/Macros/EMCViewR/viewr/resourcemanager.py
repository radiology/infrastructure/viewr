from collections import deque
import json
import netrc
import os
import posixpath
import subprocess
import tempfile
from urllib import parse
import shutil

import xnat


from .globals import ctx, DOWNLOAD_SCRIPT, UPLOAD_SCRIPT, POLL_INTERVAL, PYTHON_BIN
from . import globals, log
from .helpers import update_netrc_entry, get_netrc
from .task import Task


# Get the info needed for calling the download script
class ResourceUpload(object):
    def __init__(self, resource, callback, kwargs=None):
        self.resource = resource
        self.callback = callback
        self.kwargs = kwargs or {}

        # For storing the result of an upload command
        self.success = False
        self.validated = False
        self.timestamp = None
        self.errors = []

    def update(self, data):
        # Get information from an upload result
        self.success = data.pop('success', False)
        self.validated = data.pop('validated', False)
        self.errors = data.pop('errors', [])
        self.timestamp = data.get('uploaded')

        # Update resource to new data
        self.resource.__setstate__(data)


class ResourceManager(object):
    group_name = 'Image Readers'

    def __init__(self):
        self._log = log.config_logger('ResourceManager', level=20, filename=None)
        self.data = {}
        self.download_queue = deque()
        self.upload_queue = deque()
        self.auth_window = None
        self.download_process = None
        self.upload_process = None
        self.current_download = None

        # Keep track of image readers
        self.image_reader_modules = dict()

        # Create clean data directory
        self.data_directory = os.path.join(tempfile.gettempdir(), 'viewr_data')
        if os.path.exists(self.data_directory):
            shutil.rmtree(self.data_directory)
        os.mkdir(self.data_directory)

        # Start the polling
        ctx.callLater(0.0, self._poll_upload, [])
        ctx.callLater(0.0, self._poll_download, [])

    def create(self, source_url, type=None, name=None, callback=None):
        if source_url not in self.data:
            # Create resource and queue to download
            resource_dir = os.path.join(self.data_directory, '{:016X}'.format(abs(hash(source_url))))
            self.data[source_url] = Resource(source_url=source_url, type=type, listeners=0, directory=resource_dir, name=name, callback=callback)
        elif callback:
            # Add additional callback to current resource
            resource = self.data[source_url]
            if callback not in resource.callbacks:
                resource.callbacks.append(callback)
        return self.data[source_url]

    def acquire(self, resource, priority=False):
        source_url = resource.source_url

        if not os.path.isdir(resource.directory):
            os.mkdir(resource.directory)

        # Increase the listener count
        self.data[source_url].listeners += 1
        self._log.info('Acquired {} [{}] (new listeners {})'.format(resource.name, source_url, self.data[source_url].listeners))

        if not self.data[source_url].downloaded:
            if self.data[source_url] in self.download_queue:
                if priority:
                    self._log.warning('{} already queued, moved to front of the queue'.format(source_url))
                    # Move by deleting, and adding at the front if the deleting was successful
                    try:
                        self.download_queue.remove(self.data[source_url])
                    except ValueError:
                        pass
                    else:
                        self.download_queue.appendleft(self.data[source_url])
                else:
                    self._log.warning('{} already queued'.format(source_url))
            elif priority:
                self.download_queue.appendleft(self.data[source_url])
            else:
                self.download_queue.append(self.data[source_url])

    def _check_auth(self, url):
        if self.auth_window is not None:
            # Auth window is open, return immediately
            return False

        # Parse url into hostname
        parsed_url = parse.urlparse(url)
        hostname = parsed_url.netloc
        server_path = parsed_url.path.split('/data/', 1)[0]
        server_url = parse.urlunparse((parsed_url.scheme,
                                        parsed_url.netloc,
                                        server_path,
                                        None,
                                        None,
                                        None))

        self._log.info('Checking auth for {}'.format(server_url))

        # Check if netrc auth is available
        auth = get_netrc().authenticators(hostname)

        if auth is not None:
            auth_success, auth_message = self._check_xnat_access(server=server_url,
                                                                 username=auth[0],
                                                                 password=auth[2],
                                                                 url=url)
        else:
            auth_success = False
            auth_message = 'Could not find login credentials in netrc file'

        if not auth_success:
            self._log.error('Could not find login information for {}: {}'.format(
                server_url,
                auth_message
            ))

            if self.auth_window is None:
                self.auth_window = ctx.showWindow('LoginWindow')
                self.auth_window.control("hostname").setTitle(hostname)
                self.auth_window.control("url").setTitle(url)
                self.auth_window.control("server").setTitle(server_url)
                if auth is not None:
                    self.auth_window.control("username").setText(auth[0])

            return False
        else:
            return True

    def _check_xnat_access(self, server, username, password, url=None):
        url, _ = url.split('/experiments/', 1)
        self._log.info('Removed experiment and further down from url, updated to {}'.format(url))

        try:
            with xnat.connect(server=server, user=username, password=password, logger=self._log, no_parse_model=True) as connection:
                self._log.info('Logged in as {}, expected {}'.format(connection.logged_in_user, username))
                if connection.logged_in_user != username:
                    return False, 'Logged in as {}, expected {}'.format(connection.logged_in_user, username)

                return True, 'Auth successful'

        except (xnat.exceptions.XNATAuthError, xnat.exceptions.XNATResponseError) as exception:
            return False, str(exception)

    def create_auth_callback(self):
        hostname = self.auth_window.control("hostname").title()
        url = self.auth_window.control("url").title()
        server = self.auth_window.control("server").title()
        username = self.auth_window.control("username").text()
        password = self.auth_window.control("password").text()

        # Check if auth is valid
        self._log.debug('Testing auth using {}'.format(server))

        auth_success, auth_message = self._check_xnat_access(server=server,
                                                             username=username,
                                                             password=password,
                                                             url=url)

        self._log.info('Auth result: [{}] {}'.format(auth_success, auth_message))

        if not auth_success:
            html_content = """
              <html>
              <head>
                <style type="text/css">
                  body {{
                    margin: 0px;
                    padding: 0px;
                    font-family: "arial,sans-serif";
                    font-size: 100%;
                  }}
                  div {{
                    heigth: 100%;
                  }}
                  .error {{
                    border: 3px solid #f00;
                    background: #ffc0cb;
                    margin: 0;
                    padding: 3px;
                  }}
                </style>
              </head>
              <body>
                <div class='error'>Login information for {host} was incorrect: {msg}</div>
              </body></html>
            """.format(host=hostname, msg=auth_message)
            self.auth_window.control("login_error_panel").setHtmlFromString(html_content)
            self.auth_window.control("login_error_panel").setVisible(True)
            self.auth_window.control("password").setText("")
            return
        else:
            self.auth_window.control("login_error_panel").setVisible(False)

        update_netrc_entry(hostname, username, password)

        self.auth_window.close()
        self.auth_window = None

    def release(self, resource):
        source_url = resource.source_url

        # Reduce the listener count
        resource = self.data[source_url]
        resource.listeners -= 1
        self._log.info('Released {} [{}] (new listeners {})'.format(resource.name, source_url, resource.listeners))

        # Clean resource if needed
        self._clean(resource)

    def _clean(self, resource):
        # Check if resource is still needed
        if resource.listeners < 1:
            # Check if resource is in the queue
            if resource in self.download_queue:
                self._log.debug('Removing {} from queue'.format(resource.source_url))
                self.download_queue.remove(resource)

            # Check if download process is still alive
            if self.download_process:
                return_code = self.download_process.poll()

                if return_code:
                    self._log.info('Processes killed, continue cleaning')
                    self.download_process = None

            # Check if resource is currently being downloaded
            if resource is self.current_download and self.download_process is not None:
                self._log.info('Kill current download for {}'.format(resource.source_url))
                process = self.download_process
                try:
                    process.kill()
                except OSError as exception:
                    # Process already died before attempting to kill it
                    if exception.errno != 3:
                        raise

                # Windows kill is not instant, so call again and see if
                # the process is actually dead so we can clean up
                ctx.callLater(0.1, self._clean, [resource])
            else:
                resource.clean()

    def get_filename(self, url):
        if url not in self.data:
            raise ValueError('No resource for {}'.format(url))

        if self.data[url].downloaded:
            return self.data[url].filename
        else:
            return None

    def _download(self, resource):
        self.current_download = resource
        self.download_process = self._subprocess(resource=resource,
                                                 script=DOWNLOAD_SCRIPT,
                                                 order=resource.download_order,
                                                 result=resource.download_result,
                                                 stderr=resource.download_stderr,
                                                 stdout=resource.download_stdout)

    def _upload(self, resource_upload):
        self.current_upload = resource_upload
        resource = resource_upload.resource

        self.upload_process = self._subprocess(resource=resource,
                                               script=UPLOAD_SCRIPT,
                                               order=resource.upload_order,
                                               result=resource.upload_result,
                                               stderr=resource.upload_stderr,
                                               stdout=resource.upload_stdout)

    def _subprocess(self, resource, script, order, result, stderr,stdout):
        # Create a subprocess for downloading
        self._log.debug('Calling "{} {}" for {}'.format(PYTHON_BIN,
                                                        script,
                                                        resource.source_url))

        # On windows make sure the subprocess does not open a new console window
        if os.name == 'nt':
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        else:
            startupinfo = None  # Ignore on other systems

        with open(order, 'w') as order_out:
            self._log.debug('RESOURCE STATE:')
            self._log.debug(resource.__getstate__())
            json.dump(resource.__getstate__(), order_out)

        self._log.info('Wrote order for {} to {}'.format(script, order))
        self._log.debug('Command: {}'.format([PYTHON_BIN, '-u', script, order, result]))

        child_process = subprocess.Popen([PYTHON_BIN, '-u', script, order, result],
                                         stdout=open(stdout, 'wb'),
                                         stderr=open(stderr, 'wb'),
                                         startupinfo=startupinfo)

        return child_process

    def _poll_upload(self):
        delay = POLL_INTERVAL

        try:
            if self.upload_process is None:
                if len(self.upload_queue) > 0:
                    # There is a new item, upload it
                    self._log.info('Start new upload!')
                    delay = 0.25
                    next_resource = self.upload_queue[0].resource
                    if self._check_auth(url=next_resource.source_url):
                        self._upload(self.upload_queue.popleft())

            elif self.upload_process.poll() is not None:
                # Upload has just finished, process the result
                self._log.info('Upload seems done, process result')
                upload_resource = self.current_upload
                resource = upload_resource.resource

                # Set process to None
                self.upload_process = None
                self.current_upload = None

                # Process result
                try:
                    with open(resource.upload_result) as result_in:
                        result = result_in.read()
                except IOError:
                    self._log.info('Could not find upload result file: {}'.format(resource.upload_result))
                    if os.path.exists(resource.upload_stdout) and os.path.exists(resource.upload_stderr):
                        with open(resource.upload_stdout) as stdout, open(resource.upload_stderr) as stderr:
                            self._log.error('UPLOAD STDOUT: {}'.format(stdout.read()))
                            self._log.error('UPLOAD STDERR: {}'.format(stderr.read()))
                    raise

                try:
                    result = json.loads(result)
                    self._log.info('RESOURCE STATE AFTER UPLOAD: {}'.format(result))
                    upload_resource.update(result)
                except ValueError:
                    self._log.error('Could not parse result from file: {}'.format(resource.upload_result))
                    self._log.error('File content: {}'.format(result))
                    raise

                # Accept it if the file is already gone somehow
                try:
                    os.remove(resource.upload_result)
                except OSError:
                    pass

                try:
                    os.remove(resource.upload_order)
                except OSError:
                    pass

                self.release(resource)

                # Callback
                if upload_resource.callback is not None:
                    ctx.callLater(0.0, upload_resource.callback, [upload_resource])
        finally:
            ctx.callLater(delay, self._poll_upload, [])

    def _poll_download(self):
        delay = POLL_INTERVAL

        try:
            if self.download_process is None:
                if self.upload_process is None and len(self.download_queue) > 0:
                    # There is a new item, download it
                    delay = 0.25
                    resource_to_download = self.download_queue[0]
                    if self._check_auth(url=resource_to_download.source_url):
                        self._download(self.download_queue.popleft())

            elif self.download_process.poll() is not None:
                # Download has just finished, process the result
                self._log.info('Download seems done, processing result')
                resource = self.current_download
                self.current_download = None

                # Process result
                try:
                    with open(resource.download_result) as result_in:
                        result = json.load(result_in)
                    resource.__setstate__(result)
                except (ValueError, IOError):
                    self._log.error('Could not parse result from file: {} [{}]'.format(resource.download_result, resource))

                    # Mark that the download process has run (since download script values are not recieved)
                    resource.download_attempted = True
                    resource.download_success = False

                # Set process to None
                self.download_process = None

                # Accept it if the file is already gone somehow
                try:
                    os.remove(resource.download_result)
                except OSError:
                    pass

                try:
                    os.remove(resource.download_order)
                except OSError:
                    pass

        finally:
            ctx.callLater(delay, self._poll_download, [])

    def add_image_reader(self, name, filename):
        self._log.debug("Adding image reader: name = {}, filename = {}".format(name, filename))

        mod = ctx.addModule('itkImageFileReader')
        mod.name = 'reader_{}'.format(name)
        mod.field("autoDetermineDataType").value = True
        mod.addToGroup(self.group_name)

        bypass_mod = ctx.addModule('HistogramNormalize')
        bypass_mod.name = "reader_{}_bypass".format(name)
        bypass_mod.addToGroup(self.group_name)

        mask_mod = ctx.addModule('Mask')
        mask_mod.name = "reader_{}_mask".format(name)
        mask_mod.addToGroup(self.group_name)
        
        # Connect the bypass to the reader by default
        bypass_mod.field("input0").connectFrom(mod.field("output0"))

        if filename is not None:
            mod.field("fileName").value = filename
        self.image_reader_modules[name] = {"reader": mod, "bypass": bypass_mod, "mask": mask_mod}
        return mod, bypass_mod, mask_mod

    def remove_image_reader(self, name):
        self._log.debug("Removing reader: name = {}".format(name))
        try:
            to_remove = self.image_reader_modules.pop(name)

            to_remove['reader'].remove()
            to_remove['bypass'].remove()
            to_remove['mask'].remove()
        except KeyError:
            self._log.warning("scan reader named '{}' does not exist and therefore cannot not be removed.".format(name))


class Resource(object):
    def __init__(self, source_url, type=None, listeners=1, directory=None, output=None, name=None, callback=None):
        self._log = log.config_logger('Resource', level=20, filename=None)

        if name is None:
            name = posixpath.basename(source_url)

        # Use part of hash to avoid name clashes if same basename
        self.name = '{}_{}'.format(''.join(c for c in name if c.isalnum()),
                                   str(hash(source_url))[-6:])

        self.source_url = source_url
        if type is not None:
            self.type = type
        else:
            self.type = 'scan'
        self.directory = directory
        self.listeners = listeners
        self.original_filename = os.path.join(self.directory, posixpath.basename(self.source_url))
        self.edited_filename = None
        self.uploaded = None
        self.output = output
        self.reader = None
        self.bypass = None
        self.mask = None
        self.callbacks = []

        if callback:
            self.callbacks.append(callback)

        self.download_attempted = False
        self.download_success = None
        self.download_errors = []

    @property
    def download_stdout(self):
        return os.path.join(self.directory, '__download_stdout__')

    @property
    def download_stderr(self):
        return os.path.join(self.directory, '__download_stderr__')

    @property
    def download_order(self):
        return os.path.join(self.directory, '__download_order__.json')

    @property
    def download_result(self):
        return os.path.join(self.directory, '__download_result__.json')

    @property
    def upload_stdout(self):
        return os.path.join(self.directory, '__upload_stdout__')

    @property
    def upload_stderr(self):
        return os.path.join(self.directory, '__upload_stderr__')

    @property
    def upload_order(self):
        return os.path.join(self.directory, '__upload_order__.json')

    @property
    def upload_result(self):
        return os.path.join(self.directory, '__upload_result__.json')

    @property
    def downloaded(self):
        return self.download_success and os.path.isfile(self.original_filename)

    def __getstate__(self):
        state = dict(self.__dict__)
        del state['_log']
        del state['reader']
        del state['output']
        del state['callbacks']
        del state['listeners']
        if 'reader' in state:
            del state['reader']
        if 'bypass' in state:
            del state['bypass']
        if 'mask' in state:
            del state['mask']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.trigger_load()

    def __repr__(self):
        return "<Resource {}: {}>".format(self.name, self.source_url)

    def clean(self):
        self.download_attempted = False
        if os.path.isfile(self.original_filename):
            os.remove(self.original_filename)
        if os.path.isdir(self.directory):
            shutil.rmtree(self.directory)
        if self.reader is not None:
            self.remove_image_reader()

    def trigger_load(self):
        # Trigger all callbacks
        self.create_image_reader()
        if self.callbacks:
            for callback in self.callbacks:
                if not callback:
                    continue
                ctx.callLater(0.0, callback, [self])
    
    def mask_with(self, mask_with_resource=None):
        # Disconnect mask links before adding them again
        self.mask.field("input0").disconnectAll()
        self.mask.field("input1").disconnectAll()

        if mask_with_resource is not None:
            self._log.info('Masking {} with {}'.format(self.name, mask_with_resource.name))
            self.mask.field("input0").connectFrom(self.reader.field("output0"))
            self.mask.field("input1").connectFrom(mask_with_resource.output)
            self.bypass.field("input0").connectFrom(self.mask.field("output0"))
        else:
            self.bypass.field("input0").connectFrom(self.reader.field("output0"))

    def create_image_reader(self):
        self._log.debug('Called create_image_reader for {} ({})'.format(self.name, self))
        self._log.debug('Resource type: {}'.format(self.type))
        self._log.debug('Resource __dict__: {}'.format(self.__dict__))

        if self.type == 'qafields':
            return

        # Check if the resource is part of the current task
        current_task = globals.task_manager.current_item
        if not isinstance(current_task, Task):
            self._log.info('No current task and so no readers required. No creating image reader')
            return

        if self not in current_task.image_resources():
            self._log.info('Resource not in use by current task (must be preloaded). Not creating image reader.')
            return

        if self.download_attempted and not self.downloaded:
            self._log.info('Cannot create reader for {}, download failed!')
            globals.errors.add_error(
                'Encountered a problem downloading {}!'.format(self.name),
                'error',
                details='\n\n'.join(self.download_errors)
            )
        else:
            self._log.info('Creating image reader for {} ({})'.format(self.name, self.original_filename))

        if self.type == 'scan' or self.type == 'pixel':
            self._create_image_reader()
        else:
            self._log.error("No known type ({}) for this resource ({})".format(self.type, self.name))

        # TODO: Fix this! ->  update_loading_message()

    def _create_image_reader(self):
        self._log.debug("Trying to load image from: {}".format(self.original_filename))
        if self.reader is None:
            if self.downloaded:
                self.reader, self.bypass, self.mask = globals.resource_manager.add_image_reader(self.name, self.original_filename)
            else:
                self.reader, self.bypass, self.mask = globals.resource_manager.add_image_reader(self.name, None)
            self.output = self.bypass.field("output0")
        else:
            if self.downloaded:
                if self.reader.field("fileName").value != self.original_filename:
                    self.reader.field("fileName").value = self.original_filename

    def remove_image_reader(self):
        if self.reader is not None:
            globals.resource_manager.remove_image_reader(self.name)
        self.reader = None
        self.bypass = None
        self.mask = None
        self.output = None

    def sync(self, callback, kwargs):
        if self.edited_filename is None:
            globals.errors.add_error("Cannot save resource {} because edited_filename could not be obtained.".format(self.name), "error")
            return

        globals.resource_manager.acquire(self)

        upload_order = ResourceUpload(self, callback=callback, kwargs=kwargs)
        self._log.info("Added {} to upload queue".format(upload_order))
        globals.resource_manager.upload_queue.append(upload_order)

        self._log.info("Added {} to upload queue".format(self.name))

