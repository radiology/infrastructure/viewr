from. import Control
from ..import globals, helpers

import requests
import unicodedata


class JSONDisplay(Control):
    def __init__(self, context, name, field):
        super(JSONDisplay, self).__init__(context, name, field)
        self.content = field['content']
        self.rows = field.get('textrows', 3)
        self.uri = None

    def text_field_mdl(self, name, label):
        return """Horizontal {{
          TextView {{
            name = "{name}"
            autoApply = true
            title = "{label}"
            edit = false
            visibleRows = {rows}
            expandY = false
            vscroller = On
          }}
          {base_mdl}
        }}""".format(name=name, label=label, base_mdl=self.base_mdl, rows=self.rows)

    def boolean_field_mdl(self, name, label):
        return """CheckBox {{
                    name = "{name}"
                    title = "{label}"
                    editable = false
                  }}""".format(
            name=name, label=label
        )

    def field_mdl(self, name, label, type):
        if type == 'text':
            return self.text_field_mdl(name, label)
        if type == 'boolean':
            return self.boolean_field_mdl(name, label)

    def set_field(self, name, value, type):
        control = globals.MAIN_WINDOW.control('{}__{}'.format(self.control_name, name))

        if type == 'text':
            if isinstance(value, list):
                value = ', '.join(value)
            elif isinstance(value, str):
                value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
            else:
                value = str(value)

            control.setText(value)
        elif type == 'boolean':
            control.checked = bool(value)

    @property
    def mdl(self):
        content_mdl = '\n'.join(self.field_mdl('{}__{}'.format(self.control_name, k), v['label'], v['type']) for k, v in self.content.items())

        return """Box {{ title="{label}" x = {x} y = {y} name = {control_name}
        Vertical {{ {content} }}
        {base_mdl}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*"
        }}""".format(
            context=self.context_name,
            name=self.name,
            control_name=self.control_name,
            content=content_mdl,
            label=self.label,
            base_mdl=self.base_mdl,
            x=self.x,
            y=self.y,)

    @property
    def value(self):
        return self.uri

    @value.setter
    def value(self, value):
        self.uri = value
        if self.uri:
            self._log.info('Getting JSON Display data from {}'.format(self.uri))
            response = requests.get(self.uri, verify=False)
            self._log.warning('Got response [{}]: {}'.format(response.status_code, response.text))
            try:
                self.data = response.json()
            except ValueError:
                self._log.error('Could not download JSON data')
                self.data = {key: '#NO DATA#' for key in self.content.keys()}
            self._log.warning('Got data {} from {}'.format(self.data, self.uri))
            for key, value in self.content.items():
                if key in self.data:
                    self.set_field(key, self.data[key], value['type'])

    def wakeup(self):
        context = helpers.get_context(self.context_name)
        self.value = context.qa_fields.get(self.name, None).value
        self.control.setStyleSheetFromString("""
    QScrollBar:vertical {
        border: 1px solid #999999;
        background:white;
        width:10px;
        margin: 0px 0px 0px 0px;
    }
    
    QScrollBar::handle:vertical {
        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
        stop: 0  rgb(32, 47, 130), stop: 0.5 rgb(32, 47, 130),  stop:1 rgb(32, 47, 130));
        min-height: 0px;
    }
    
    QScrollBar::add-line:vertical {
        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
        stop: 0  rgb(32, 47, 130), stop: 0.5 rgb(32, 47, 130),  stop:1 rgb(32, 47, 130));
        height: px;
        subcontrol-position: bottom;
        subcontrol-origin: margin;
    }
    
    QScrollBar::sub-line:vertical {
        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
        stop: 0  rgb(32, 47, 130), stop: 0.5 rgb(32, 47, 130),  stop:1 rgb(32, 47, 130));
        height: 0px;
        subcontrol-position: top;
        subcontrol-origin: margin;
    }
    """)
