from . import FieldControl


class CheckBox(FieldControl):
    def __init__(self, context, name, field):
        field['type'] = 'Bool'
        super(CheckBox, self).__init__(context, name, field)
        self.editable = field.get('editable', True)
        self.alert_on_checked = field.get('alert_on_checked', False)
        self.alert_on_unchecked = field.get('alert_on_unchecked', False)

    def arguments(self):
        arg = self.base_arguments()
        arg['editable'] = 'Yes' if self.editable else 'No'
        return arg

    @property
    def mdl(self):
        return """CheckBox {field} {{
          name = "{control_name}"
          title = "{label}"
          editable = {editable}
          x = {x} y = {y}
          {base_mdl}
        }}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*" """.format(**self.arguments())

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self.field.setBoolValue(value)
        if self.control:
            if self.has_alert():
                self.control.setStyleSheetFromString("font-weight: bold; color: red;")
            else:
                self.control.setStyleSheetFromString("font-weight: normal; color: black;")

    def wakeup(self):
        self.set_initial_value()

    def has_alert(self, value=None):
        """
        Indicate if there is an alert in this widget (or in a subwidget)
        """
        if value is None:
            value = self.value

        return self.alert_on_checked if value else self.alert_on_unchecked
