from . import FieldControl


class CalculatedScore(FieldControl):
    def __init__(self, context, name, field):
        super(CalculatedScore, self).__init__(context, name, field)
        self.hint_text = field.get('hint_text', '')
        self.variables = field.get('variables', {})
        self.formula = field.get('formula', 'None')

    def arguments(self):
        arg = self.base_arguments()
        arg['field'] = self.field.fullName()
        arg['hint_text'] = self.hint_text
        return arg

    @property
    def mdl(self):
        arguments = self.arguments()

        callback = "get_control('{context}', '{name}').update()".format(context=self.context_name, name=self.name)
        arguments['field_listeners'] = "\n".join(self.context().qa_fields[x].listener_mdl(callback) for x in self.variables.values())

        return """
        Horizontal {{ x = {x} y = {y}
        Field {field} {{
            name = "{control_name}"
            edit = No
            hintText = "{hint_text}"
        }}
        SpacerX {{}}
        {base_mdl}
        }}

        {field_listeners}
        Execute = "*py:get_control('{context}', '{name}').update()*"

        """.format(**arguments)

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self._log.info('Cannot set a CalculatedScore field, it is read only (auto calculated). Ignoring new value.')

    def update(self):
        values = {key: self.context().qa_fields[value].value for key, value in self.variables.items()}
        result = eval(self.formula, {}, values)
        self.field.value = result
