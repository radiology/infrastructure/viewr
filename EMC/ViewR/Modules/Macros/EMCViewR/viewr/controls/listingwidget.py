import copy

from .. import globals
from . import Control

ctx = globals.ctx


class ListingWidget(Control):
    def __init__(self, context, name, field):
        super(ListingWidget, self).__init__(context, name, field)
        self.content = field['content']
        self.label = field['label']
        self.columns = field['columns']
        self.data = []
        self.current_index = None
        self.updating = False
        # Add content to the list of qa fields.
        context.add_qa_fields(self.content)

    def update(self, caller):
        if not self.updating:
            self._log.info("updated called from: {}".format(caller))
            self.change_current_item(self.current_index)
        else:
            self._log.info("update from {} ignored because a change is already in progress".format(caller))

    @property
    def mdl(self):
        callback = "get_control('{context}', '{name}').update({{caller}})".format(context=self.context_name, name=self.name)
        content_mdl = '\n'.join(self.context().qa_fields[qa_field].mdl for qa_field in self.content.keys())

        field_listeners = "\n".join(self.context().qa_fields[x].listener_mdl(callback) for x in self.content.keys())

        return """Box {{ title="{label}" x = {x} y = {y} name = {control_name}
        Horizontal {{ dependsOn = EnableNavigation
          ListView {{
            name = {control_name}_list
            selectionMode = Single
            clickedCommand = "*py:get_control('{context}', '{name}').change_current_item()*"
            expandx = Yes
            enabled = Yes
          }}
          Vertical {{
              expandx = No
              alignY = Top
              ToolButton {{
                  {enabled_mdl} 
                  image = "{local}/Modules/Resources/Icons/add-16.png"
                  scaleIconSetToMinSize = False
                  autoRaise = True
                  
                  mw = 18
                  mh = 18
                  tooltip = "Add item"
                  command = "*py:get_control('{context}', '{name}').add_item()*"
              }}
              ToolButton {{
                  {enabled_mdl} 
                  image = "{local}/Modules/Resources/Icons/remove-16.png"
                  scaleIconSetToMinSize = False
                  autoRaise = True
                  mw = 18
                  mh = 18
                  tooltip = "Remove item"
                  command = "*py:get_control('{context}', '{name}').remove_item()*"
              }}
          }}
        }}
        Label {{ name = {control_name}_selected }}
        Grid {{ {content} }}
        {visible_mdl}
        {field_listeners}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*"
        }}""".format(
            context=self.context_name,
            name=self.name,
            control_name=self.control_name,
            content=content_mdl,
            label=self.label,
            x=self.x,
            y=self.y,
            local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
            field_listeners=field_listeners,
            enabled_mdl='{}\n{}'.format(self.enabled_mdl, self.depends_on_mdl),
            visible_mdl='{}\n{}'.format(self.visible_mdl, self.visible_on_mdl)
        )

    @property
    def value(self):
        data = copy.deepcopy(self.data)

        if self.current_index is not None:
            # Set data before updating widgets
            new_data = {qa_field: self.context().qa_fields[qa_field].value for qa_field in self.content.keys()}
            # Make sure the data is not referenced, as there might be updates
            # later which would effect saved data
            new_data = copy.deepcopy(new_data)
            data[self.current_index].update(new_data)

        return data

    @value.setter
    def value(self, value):
        if value is None:
            value = []
        self.data = value
        self.current_index = None
        self._populate_list()
        self._log.info("Set Value to {}".format(value))

    @property
    def list_control(self):
        return globals.MAIN_WINDOW.control(self.control_name + "_list")

    def _init_list(self):
        self.list_control.clearItems()
        self.list_control.removeColumns()
        self.list_control.addColumn("")
        self.list_control.addColumn("Id")
        for column in self.columns:
            self.list_control.addColumn(self.content[column]['label'])

    def _populate_list(self):
        context = self.context()

        self.list_control.clearItems()
        selected_item = None
        for nr, item_data in enumerate(self.data):
            selected_item_data = ([item_data.get(x, '') for x in ['', 'Id']] +
                                  [context.qa_fields[x].display_value(item_data.get(x, '')) for x in self.columns])
            # Change None into empty string for display
            selected_item_data = [x if x is not None else '' for x in selected_item_data]
            item = self.list_control.appendItem(None, selected_item_data)

            # Check if item has alert and set pixmap if needed
            alert = any(context.qa_fields[qa_field].has_alert(item_data[qa_field]) for qa_field in self.content if qa_field in item_data)
            if alert:
                icon_file = ctx.expandFilename("$(MLAB_EMC_ViewR)/Modules/Resources/Icons/dialog-warning.png")
                item.setPixmapFile(0, icon_file)

            # Set selected
            if nr == self.current_index:
                selected_item = item

        self.list_control.resizeColumnsToContents()
        if selected_item is not None:
            self.list_control.ensureItemVisible(selected_item)
            selected_item.setSelected(True)

    def wakeup(self):
        globals.MAIN_WINDOW.control(self.control_name + "_selected").setTitle('No item selected!')
        self._init_list()
        self._populate_list()
        if self.current_index is None:
            for key in self.content.keys():
                self.context().qa_fields[key].control.setEnabled(False)

    def item_to_index(self, item):
        id_ = int(item.text(1))
        index = next(nr for nr, x in enumerate(self.data) if x['Id'] == id_)
        return index

    def add_item(self):
        self._log.debug("Add item")
        item_data = {key: self.context().qa_fields[key].get_initial_value() for key in self.content.keys()}
        item_data['Id'] = (max(x['Id'] for x in self.data) + 1) if len(self.data) else 0
        self.data.append(item_data)

        # Change current item to newly added item
        self.change_current_item(len(self.data) - 1)

    def remove_item(self):
        self._log.debug("Remove item")
        # Get the index in the data that corresponds with the Id
        index_to_remove = self.item_to_index(self.list_control.selectedItem())

        # Remove the entry from the data
        if index_to_remove == self.current_index:
            self.current_index = None
        elif index_to_remove < self.current_index:
            self.current_index -= 1
        del self.data[index_to_remove]
        self.list_control.selectedItem().setSelected(False)
        self.change_current_item()

    def update_selected(self):
        selected_item_id = self.list_control.selectedItem().text(0)
        self._log.debug("selected: {}".format(selected_item_id))

    def change_current_item(self, new_item=None):
        self.updating = True  # Block updates during changes
        context = self.context()
        self._log.info('CUR INDEX: {}  NEW ITEM: {}  DATA: {}'.format(self.current_index, new_item, self.data))
        # Current item is changed, new selected item
        if new_item is None:
            new_item = self.list_control.selectedItem()

        # Save widget data before change to current item
        if self.current_index is not None:
            # Set data before updating widgets
            new_data = {qa_field: context.qa_fields[qa_field].value for qa_field in self.content.keys()}
            # Make sure the data is not referenced, as there might be updates
            # later which would effect saved data
            new_data = copy.deepcopy(new_data)
            self.data[self.current_index].update(new_data)

        # Now set the new item as current
        if new_item is not None:
            if isinstance(new_item, int):
                new_index = new_item
            else:
                new_index = self.item_to_index(new_item)

            self._log.info('New index: {}'.format(new_index))
            selected_item_data = self.data[new_index]
            self.current_index = new_index
            globals.MAIN_WINDOW.control(self.control_name + "_selected").setTitle('Selected item Id: {}'.format(selected_item_data['Id']))

            # Update widgets to new item data
            for qa_field in self.content:
                value = selected_item_data.get(qa_field, None)
                self._log.info("Enabling {} -> {}".format(qa_field, value))
                context.qa_fields[qa_field].value = value
                context.qa_fields[qa_field].control.setEnabled(True)
                if context.qa_fields[qa_field].depends_on:
                    for child in context.qa_fields[qa_field].control.children():
                        if hasattr(child, 'evaluate'):
                            child.evaluate()
        else:
            globals.MAIN_WINDOW.control(self.control_name + "_selected").setTitle('No item selected!')
            self.current_index = None

            for key in self.content.keys():
                context.qa_fields[key].value = None
                context.qa_fields[key].control.setEnabled(False)
        self._populate_list()

        # Broadcast a trigger for possible listeners
        self.field.touch()
        self.updating = False

    def has_alert(self, value=None):
        """
        Indicate if there is an alert in this widget (or in a subwidget)
        """
        if value is None:
            value = self.value

        return any(
            any(
                self.context().qa_fields[qa_field].has_alert(item[qa_field]) for qa_field in self.content.keys() if qa_field in item
            ) for item in value
        )
