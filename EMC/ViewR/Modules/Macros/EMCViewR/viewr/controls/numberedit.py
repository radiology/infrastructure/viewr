from . import FieldControl


class NumberEdit(FieldControl):
    def __init__(self, context, name, field):
        super(NumberEdit, self).__init__(context, name, field)

    def arguments(self):
        arg = self.base_arguments()
        return arg

    @property
    def mdl(self):
        return """Horizontal {{ x = {x} y = {y}
        Label = "{label}"
        NumberEdit {field} {{
         name = "{control_name}" }}
         {base_mdl}
        }}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*" """.format(**self.arguments())

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self.field.value = float(value or 0.0)

    def wakeup(self):
        self.set_initial_value()
