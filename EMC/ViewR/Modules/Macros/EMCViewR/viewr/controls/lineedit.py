from . import FieldControl


class LineEdit(FieldControl):
    def __init__(self, context, name, field):
        super(LineEdit, self).__init__(context, name, field)
        self.hint_text = field.get('hint_text', '')

    def arguments(self):
        arg = self.base_arguments()
        arg['hint_text'] = self.hint_text
        return arg

    @property
    def mdl(self):
        self._log.info(self.base_mdl)
        return """Horizontal {{ x = {x} y = {y}
        Label = "{label}"
        LineEdit {field} {{
            name = "{control_name}"
            hintText = "{hint_text}"
            }}
        {base_mdl}
        }}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*" """.format(**self.arguments())

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self.field.setStringValue(value or "")

    def wakeup(self):
        self.set_initial_value()
