import collections
import json

from ..import globals, log

from . import EditControl, EditorModule, parse_color
from .boxwidget import BoxWidget
from .tabswidget import TabsWidget
from .listingwidget import ListingWidget

ctx = globals.ctx


class MarkerEditorModule(EditorModule):
    def __init__(self, module_name, context, all_markers=False):
        self._log = log.config_logger('MarkerEditorModule', level=10, filename=None)
        self.module_name = module_name

        # Get marker styles from context
        self.style_module = context.marker_style_module
        self.active_style_module = context.active_marker_style_module

        # Create required modules
        self.module = ctx.addModule("SoView2DMarkerEditor")
        self.module.name = self.module_name
        self.module.field("textMode").value = "TEXT_STYLENAME"
        self.module.field("textPosition").value = "TEXT_POSITION_BELOW"
        self.module.field("editingOn").value = False

        # Connect style to editor
        if all_markers:
            self.module.field("stylePalette").connectFrom(self.style_module.field('outStylePalette'))
        else:
            self.module.field("stylePalette").connectFrom(self.active_style_module.field('outStylePalette'))

        self.module.field("drawMarkerShapesWithLines").value = True

        # Add to correct group
        group = context.group_name
        self.module.addToGroup(group)

    @staticmethod
    def add_style_module(name):
        style_module = ctx.addModule("StylePalette")
        style_module.name = name

        for index in range(1, 13):
            style_module.field('markerType{}'.format(index)).value = 'Square'
            style_module.field('color{}'.format(index)).value = (1, 1, 1)
            style_module.field('markerSize{}'.format(index)).value = 6
            style_module.field('antiAlias{}'.format(index)).value = True
            style_module.field('lineWidth{}'.format(index)).value = 2
            style_module.field('name{}'.format(index)).value = ''

        return style_module

    def clean(self):
        if self.module:
            self.module.remove()
            self.module = None

    @property
    def editor_module(self):
        return self.module

    @property
    def marker_field(self):
        return self.module.field("outXMarkerList")

    @property
    def marker_object(self):
        return self.marker_field.object()

    @property
    def output(self):
        return self.editor_module.field("self")

    def trigger_update(self):
        self.editor_module.field("update").touch()

    @property
    def editing_field(self):
        return self.editor_module.field("editingOn")

    @property
    def editing_on(self):
        return self.editing_field.value

    @editing_on.setter
    def editing_on(self, value):
        self.editing_field.value = value

    @property
    def current_type(self):
        return self.editor_module.field("currentType").value

    @current_type.setter
    def current_type(self, value):
        self.editor_module.field("currentType").value = value

    @property
    def markers(self):
        return [{'pos': m.pos, 'vec': m.vec, 'type': m.type} for m in self.marker_object.getMarkers()]

    @markers.setter
    def markers(self, value):
        if not isinstance(value, list):
            self._log.error('Markers should be a list!')
            return

        self.remove_markers()
        for marker in value:
            self.marker_object.add(marker['pos'], marker['vec'], marker['type'])
        self.deselect()

    def deselect(self):
        # Add and remove a dummy marker to deselect the last marker added
        self.marker_object.add((0, 0, 0, 0, 0, 0), (0, 0, 0), 1)
        self.marker_object.remove(self.marker_object.getCurrentIndex())

    def save_markers(self, filename):
        self._log.info('Saving marker json file to: {}'.format(filename))
        with open(filename, 'w') as fh:
            json.dump(self.markers, fh)
        return filename

    def load_markers(self, filename):
        self._log.info("Loading markers from: {}".format(filename))

        with open(filename, 'r') as fh:
            self.markers = json.load(fh)

    def remove_markers(self):
        # Remove all markers from the list
        for index in range(self.marker_object.size() - 1, -1, -1):
            self.marker_object.remove(index)

    def set_style(self, index, value):
        if index > 11:
            self._log.warning('Cannot set style higher than index 11, only 12 slots! Ignoring!')

        index_label = index + 1

        color = value.get('color')
        if color:
            color = parse_color(color)
            self.style_module.field('color{}'.format(index_label)).value = color
            active_color = value.get('active_color')
            if not active_color:
                active_color = (1, 1, 1)  # tuple(0.5 + 0.5 * x for x in color)
            self.active_style_module.field('color{}'.format(index_label)).value = active_color

        line_width = value.get('line_width')
        if line_width:
            self.style_module.field('lineWidth{}'.format(index_label)).value = line_width

            active_line_width = value.get('active_line_width')
            if not active_line_width:
                active_line_width = line_width
            self.active_style_module.field('lineWidth{}'.format(index_label)).value = active_line_width

        marker_type = value.get('marker_type')
        if marker_type:
            if marker_type not in ['None', 'Dot', 'Asterisk', 'Circle', 'Square', 'Triangle', 'Plus', 'Cross']:
                marker_type = 'None'
            self.style_module.field('markerType{}'.format(index_label)).value = marker_type

            active_marker_type = marker_type
            self.active_style_module.field('markerType{}'.format(index_label)).value = active_marker_type

        marker_size = value.get('marker_size')
        if marker_size:
            self.style_module.field('markerSize{}'.format(index_label)).value = marker_size

            active_marker_size = value.get('active_marker_size')
            if not active_marker_size:
                active_marker_size = marker_size + line_width
            self.active_style_module.field('markerSize{}'.format(index_label)).value = active_marker_size

        label = value.get('label')
        if label:
            self.style_module.field('name{}'.format(index_label)).value = label
            self.active_style_module.field('name{}'.format(index_label)).value = label


class MarkerEdit(EditControl):
    def __init__(self, context, name, field):
        super(MarkerEdit, self).__init__(context, name, field)
        self.all_markers = context.marker_editor
        self.so_group = context.marker_so_group
        self.max_number = field['max_number']
        self.marker_type = field.get('marker_type', 1)
        self.data = None
        self.marker_editor = MarkerEditorModule('{}_{}_editor'.format(context.name, self.name), context)
        self.marker_editor.editor_module.field("overflowMode").value = "RemoveLast"
        self.marker_editor.editor_module.field("maxSize").value = self.max_number
        self.marker_editor.editing_on = False
        if isinstance(self.marker_type, int):
            self.marker_editor.current_type = self.marker_type

        groups = field.get('groups', None)
        if isinstance(groups, list):
            self.groups = collections.OrderedDict((x['label'], x) for x in groups)
        elif isinstance(groups, dict):
            self.groups = collections.OrderedDict(groups.items())
        else:
            self.groups = None

        if isinstance(self.groups, dict):
            for index, group_data in enumerate(self.groups.values()):
                self.marker_editor.set_style(index, group_data)

    @property
    def editor(self):
        return self.marker_editor

    def clean(self):
        # Remove all old marker for a clean update
        self.all_markers.editor_module.field('deleteAll').touch()
        self.marker_editor.clean()

    def update_all_markers(self):
        self.context().update_all_markers()

    @property
    def mdl(self):
        if isinstance(self.marker_type, str):
            type_field = self.context().qa_fields.get(self.marker_type)

            if type_field is None:
                self._log.error('Marker type qa field "{}" cannot be found!'.format(self.marker_type))
                field_listener = ""
            else:
                caller = type_field.field.fullName()
                self._log.info('Caller defined as: {}'.format(caller))
                field_listener = """FieldListener {caller} {{
                 command = "*py:get_control('{context}', '{name}').update("{caller}")*"
                }}""".format(caller=caller, context=self.context_name, name=self.name)
        else:
            field_listener = ""

        if self.label:
            label = 'Label = "{label}"'.format(label=self.label)
        else:
            label = ""

        return """Horizontal {{ x = {x} y = {y}
              name = {control_name}
              alignX = Left
              {label}
              ToolButton {editing_field} {{
                  name = {control_name}_place
                  image = "{local}/Modules/Resources/Icons/marker.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Place marker"
                  command = "*py:get_control('{context}', '{name}').place_marker()*"
                  {enabled_mdl}
              }}
              ToolButton {{
                  name = {control_name}_jump
                  image = "{local}/Modules/Resources/Icons/edit-redo.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Jump to marker"
                  command = "*py:get_control('{context}', '{name}').jump()*"
                  enabled = Yes
              }}
              Label {{ name = {control_name}_label }}
              FieldListener {listentarget} {{ command = "*py:get_control('{context}', '{name}').placed()*" }}
              FieldListener {editing_field} {{ command = "*py:get_control('{context}', '{name}').update_icon()*" }}
              {field_listener}
              {visible_mdl}
              Execute = "*py:get_control('{context}', '{name}').wakeup()*"
        }}""".format(name=self.name,
                     control_name=self.control_name,
                     context=self.context_name,
                     local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
                     label=label,
                     x=self.x,
                     y=self.y,
                     listentarget=self.marker_editor.marker_field.fullName(),
                     editing_field=self.marker_editor.editing_field.fullName(),
                     field_listener=field_listener,
                     enabled_mdl='{}\n{}'.format(self.enabled_mdl, self.depends_on_mdl),
                     visible_mdl='{}\n{}'.format(self.visible_mdl, self.visible_on_mdl))

    @property
    def value(self):
        return self.data

    @value.setter
    def value(self, value):
        self._log.info('Setting markerEdit value to: {}'.format(value))
        if value is None:
            value = []

        self.data = value
        self.update("<MarkerEdit value.setter>")
        self.update_label()
        self.activate()

    def activate(self):
        for field in self.so_group.children():
            if field.objectName == 'child':
                field.disconnectAll()

        self.update_all_markers()
        self.so_group.field("child").connectFrom(self.marker_editor.output)
        self.so_group.field("child").connectFrom(self.all_markers.output)

    def place_marker(self):
        if self.marker_editor.editing_on:
            self._log.info("Place marker ON")
            self.start_edit()
        else:
            self._log.info("Place marker OFF")
            self.stop_edit()

    def update_icon(self):
        if self.marker_editor.editing_on:
            globals.MAIN_WINDOW.control(self.control_name + "_place").setPixmapFile(ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker-green.png")
        else:
            globals.MAIN_WINDOW.control(self.control_name + "_place").setPixmapFile(ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker.png")

    def placed(self):
        if not self.marker_editor.editing_on:
            self._log.info("Placed called when editor is not active, ignoring!")
            return
        self.stop_edit()

        self._log.info("Marker placed")
        globals.MAIN_WINDOW.control(self.control_name + "_place").setPixmapFile(ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker.png")

        self.data = self.marker_editor.markers
        self.update("<MarkerEdit placed>")

    def jump(self):
        if self.data is None or len(self.data) == 0:
            self._log.warning('Cannot jump to non-existing marker!')
            return

        if len(self.data) > 1:
            self._log.warning('Multiple markers found, jumping to first marker')

        position = self.data[0]["pos"]
        self._log.info('Marker position in world coordinates: {}'.format(position))
        self.context().set_position(position)

        self.marker_editor.trigger_update()

    def display_value(self, value):
        if value is None or len(value) == 0:
            message = "No marker placed!"
        elif len(value) == 1:
            message = 'x={x[0]:.1f}, y={x[1]:.1f}, z={x[2]:.1f}'.format(x=value[0]["pos"])
        else:
            message = "{} markers".format(len(value))

        return message

    def update_label(self):
        globals.MAIN_WINDOW.control(self.control_name + "_label").setTitle(self.display_value(self.value))

    def update(self, caller):
        self._log.info('Calling update from {}!'.format(caller))
        if isinstance(self.marker_type, int):
            type_ = self.marker_type
        elif isinstance(self.marker_type, str):
            type_ = self.context().qa_fields[self.marker_type].index
            if type_ is None:  # In case nothing is selected default to first type
                type_ = 0

        # Set current type in marker editor
        self.marker_editor.current_type = type_

        if self.data is not None:
            for marker in self.data:
                marker['type'] = type_

            # Update data in marker editor
            self.marker_editor.markers = self.data

        self.marker_editor.trigger_update()
        self.update_label()
        self.field.touch()
        self.update_all_markers()

    def wakeup(self):
        self.update_label()
        self.update_all_markers()
        globals.MAIN_WINDOW.control(self.control_name + "_jump").setEnabled(True)


class MarkerView(MarkerEdit):
    def __init__(self, context, name, field):
        super(MarkerView, self).__init__(context, name, field)
        self.type_names = field.get('type_names', None)

    @property
    def mdl(self):
        if isinstance(self.marker_type, str):
            type_field = self.context().qa_fields.get(self.marker_type)
            caller = type_field.field.fullName()
            field_listener = """FieldListener {caller} {{
             command = "*py:get_control('{context}', '{name}').update("{caller}")*"
            }}""".format(caller=caller, context=self.context_name, name=self.name)
        else:
            field_listener = ""

        if self.label:
            label = 'Label = "{label}"'.format(label=self.label)
        else:
            label = ""

        return """Horizontal {{ x = {x} y = {y}
              name = {control_name}
              alignX = Left
              {label}
              ToolButton {{
                  name = {control_name}_jump
                  image = "{local}/Modules/Resources/Icons/edit-redo.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Jump to marker"
                  command = "*py:get_control('{context}', '{name}').jump()*"
              }}
              Label {{ name = {control_name}_label }}
              {field_listener}
              {visible_mdl}
              Execute = "*py:get_control('{context}', '{name}').wakeup()*"
        }}""".format(name=self.name,
                     control_name=self.control_name,
                     context=self.context_name,
                     local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
                     label=label,
                     x=self.x,
                     y=self.y,
                     field_listener=field_listener,
                     visible_mdl='{}\n{}'.format(self.visible_mdl, self.visible_on_mdl),
                     listentarget=self.marker_editor.marker_field.fullName(),
                     )

    def display_value(self, value):
        if value is None or len(value) == 0:
            message = "No marker!"
        elif len(value) == 1:
            type_ = value[0]["type"]
            if isinstance(self.marker_type, str):
                type_name = self.context().qa_fields[self.marker_type].value
            elif self.type_names:
                type_name = self.type_names[type_]
            else:
                type_name = str(type_)
            message = 'type: {}'.format(type_name)
        else:
            message = "{} markers".format(len(value))

        return message
