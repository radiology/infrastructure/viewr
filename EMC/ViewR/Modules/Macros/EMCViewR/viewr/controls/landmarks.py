import collections
import json

from ..import globals, log

from . import EditControl, EditorModule, parse_color
from .boxwidget import BoxWidget
from .tabswidget import TabsWidget
from .listingwidget import ListingWidget
from ..helpers import ensure_valid_name

ctx = globals.ctx


class LandmarksEditorModule(EditorModule):
    def __init__(self, module_name, context, all_markers=False):
        self._log = log.config_logger('LandmarksEditorModule', level=10, filename=None)
        self.module_name = module_name

        # Get marker styles from context
        self.style_module = context.marker_style_module
        self.active_style_module = context.active_marker_style_module

        # Create required modules
        self.module = ctx.addModule("SoView2DMarkerEditor")
        self.module.name = self.module_name
        self.module.field("textMode").value = "TEXT_STYLENAME"
        self.module.field("textPosition").value = "TEXT_POSITION_BELOW"
        self.module.field("editingOn").value = False

        # Connect style to editor
        if all_markers:
            self.module.field("stylePalette").connectFrom(self.style_module.field('outStylePalette'))
        else:
            self.module.field("stylePalette").connectFrom(self.active_style_module.field('outStylePalette'))

        self.module.field("drawMarkerShapesWithLines").value = True

        # Add to correct group
        group = context.group_name
        self.module.addToGroup(group)

    @staticmethod
    def add_style_module(name):
        style_module = ctx.addModule("StylePalette")
        style_module.name = name

        for index in range(1, 13):
            style_module.field('markerType{}'.format(index)).value = 'Square'
            style_module.field('color{}'.format(index)).value = (1, 1, 1)
            style_module.field('markerSize{}'.format(index)).value = 6
            style_module.field('antiAlias{}'.format(index)).value = True
            style_module.field('lineWidth{}'.format(index)).value = 2
            style_module.field('name{}'.format(index)).value = ''

        return style_module

    def clean(self):
        if self.module:
            self.module.remove()
            self.module = None

    @property
    def editor_module(self):
        return self.module

    @property
    def marker_field(self):
        return self.module.field("outXMarkerList")

    @property
    def marker_object(self):
        return self.marker_field.object()

    @property
    def output(self):
        return self.editor_module.field("self")

    def trigger_update(self):
        self.editor_module.field("update").touch()

    @property
    def editing_field(self):
        return self.editor_module.field("editingOn")

    @property
    def editing_on(self):
        return self.editing_field.value

    @editing_on.setter
    def editing_on(self, value):
        self.editing_field.value = value

    @property
    def current_type(self):
        return self.editor_module.field("currentType").value

    @current_type.setter
    def current_type(self, value):
        self.editor_module.field("currentType").value = value

    @property
    def markers(self):
        return [{'pos': m.pos, 'vec': m.vec, 'type': m.type, 'id': m.id, 'name': m.name} for m in
                self.marker_object.getMarkers()]

    @markers.setter
    def markers(self, value):
        if not isinstance(value, list):
            self._log.error('Markers should be a list!')
            return

        self.remove_markers()
        for marker in value:
            self.marker_object.add(marker['pos'], marker['vec'], marker['type'])
            self.marker_object.getMarker(self.marker_object.getCurrentIndex()).name = marker['name']
        self.deselect()

    def deselect(self):
        # Add and remove a dummy marker to deselect the last marker added
        self.marker_object.add((0, 0, 0, 0, 0, 0), (0, 0, 0), 1)
        self.marker_object.remove(self.marker_object.getCurrentIndex())

    def save_markers(self, filename):
        self._log.info('Saving marker json file to: {}'.format(filename))
        with open(filename, 'w') as fh:
            json.dump(self.markers, fh)
        return filename

    def load_markers(self, filename):
        self._log.info("Loading markers from: {}".format(filename))

        with open(filename, 'r') as fh:
            self.markers = json.load(fh)

    def remove_markers(self):
        # Remove all markers from the list
        for index in range(self.marker_object.size() - 1, -1, -1):
            self.marker_object.remove(index)

    def set_style(self, index, value):
        if index > 11:
            self._log.warning('Cannot set style higher than index 11, only 12 slots! Ignoring!')

        index_label = index + 1

        color = value.get('color')
        if color:
            color = parse_color(color)
            self.style_module.field('color{}'.format(index_label)).value = color
            active_color = value.get('active_color')
            if not active_color:
                active_color = color  # tuple(0.5 + 0.5 * x for x in color)
            self.active_style_module.field('color{}'.format(index_label)).value = active_color

        line_width = value.get('line_width')
        if line_width:
            self.style_module.field('lineWidth{}'.format(index_label)).value = line_width

            active_line_width = value.get('active_line_width')
            if not active_line_width:
                active_line_width = line_width
            self.active_style_module.field('lineWidth{}'.format(index_label)).value = active_line_width

        marker_type = value.get('marker_type')
        if marker_type:
            if marker_type not in ['None', 'Dot', 'Asterisk', 'Circle', 'Square', 'Triangle', 'Plus', 'Cross']:
                marker_type = 'None'
            self.style_module.field('markerType{}'.format(index_label)).value = marker_type

            active_marker_type = marker_type
            self.active_style_module.field('markerType{}'.format(index_label)).value = active_marker_type

        marker_size = value.get('marker_size')
        if marker_size:
            self.style_module.field('markerSize{}'.format(index_label)).value = marker_size

            active_marker_size = value.get('active_marker_size')
            if not active_marker_size:
                active_marker_size = marker_size + 1
            self.active_style_module.field('markerSize{}'.format(index_label)).value = active_marker_size

        label = value.get('label')
        if label:
            self.style_module.field('name{}'.format(index_label)).value = label
            self.active_style_module.field('name{}'.format(index_label)).value = label


# Main Landmarks class
class Landmarks(EditControl):
    # Initialize class
    def __init__(self, context, name, field):
        super(Landmarks, self).__init__(context, name, field)
        self.all_markers = context.marker_editor
        self.so_group = context.marker_so_group
        self.marker_spec = field.get('markers', [])
        self.markers_per_row = field.get('markers_per_row', 6)
        for item in self.marker_spec:
            if 'title' not in item:
                item['title'] = item['name']
            item['name'] = ensure_valid_name(item['name'])
        self.data = None
        self.active_marker_name = None
        self.marker_editor = LandmarksEditorModule(f"{context.name}_{self.name}_editor", context)
        self.marker_editor.editor_module.field("overflowMode").value = "RemoveNew"
        self.marker_editor.editor_module.field("maxSize").value = len(self.marker_spec) + 1
        self.marker_editor.editor_module.field("textMode").value = 'TEXT_ITEMNAME'
        self.marker_editor.editing_on = False

        groups = field.get('groups', None)
        self._log.info('$$$$ GROUPS: {}'.format(groups))
        if isinstance(groups, list):
            self.groups = groups
        else:
            self.groups = None

        if isinstance(self.groups, list):
            for index, group_data in enumerate(self.groups):
                self._log.info('%%%% SETTING STYLE {} TO {}'.format(index, group_data))
                self.marker_editor.set_style(index, group_data)

        self.marker_names = [x['name'] for x in self.marker_spec]

        self.marker_cat = []
        for item in self.marker_spec:
            if item['category'] not in self.marker_cat:
                self.marker_cat.append(item['category'])

        self.activeCategory = self.marker_cat[0]

    def qa_macro_field(self):
        """
        If the child does not have a field, create a trigger field which can
        be listened on. The control should fire the trigger when there has been
        an update

        :return: string with the MDL to create a field
        :rtype: str
        """
        qa_macro_field = [super(Landmarks, self).qa_macro_field()]

        for category in self.marker_cat:
            qa_macro_field.append('Field "{name}__{category}" {{ type = Bool }}'.format(name=self.control_name,
                                                                                        category=category))
        data = '\n'.join(qa_macro_field)
        return data

    @property
    def editor(self):
        return self.marker_editor

    def clean(self):
        # Remove all old marker for a clean update
        self.all_markers.editor_module.field('deleteAll').touch()
        self.marker_editor.clean()

    def find_markers(self, data=None):
        context = self.context()
        if data is None:
            data = {k: v.value for k, v in context.toplevel_fields.items()}

        markers = []
        for key, value in data.items():
            # If this value is not in the control, ignore it
            if key not in context.qa_fields or value is None:
                continue

            control = context.qa_fields[key]
            if isinstance(control, Landmarks):
                if value is not None:
                    markers.extend(value)

            elif isinstance(control, (TabsWidget, BoxWidget)):
                markers.extend(self.find_markers(value))
            elif isinstance(control, ListingWidget):
                for entry in value:
                    entry = dict(entry)  # Make a copy
                    entry.pop('Id')  # Remove Id field that is a non-qa-field
                    markers.extend(self.find_markers(entry))
        return markers

    def update_all_markers(self):
        self.all_markers.markers = self.find_markers()

    @property
    def mdl(self):
        if self.label:
            label = 'Label = "{label}"'.format(label=self.label)
        else:
            label = ""

        end_list = ["""Vertical {{ x = {x} y = {y}
                  name = {control_name}
                  """.format(control_name=self.control_name,
                             label=label,
                             x=self.x,
                             y=self.y)]
        for category in self.marker_cat:
            end_list.append("""Box {{
                        title = "{cat_title}"
                        checkable = Yes
                        checkedField = {qa_macro}.{control_name}__{cat}
                        
                        FieldListener {qa_macro}.{control_name}__{cat} {{ 
                            command = "*py:get_control('{context}', '{name}').close_other_cat('{cat}')*" 
                        }}
                        Horizontal {{
                            expandX = Yes
                            visibleOn = {qa_macro}.{control_name}__{cat}
                        """.format(cat_title=category.replace('_', ''), 
                                   cat=category,
                                   name=self.name,
                                   control_name=self.control_name,
                                   qa_macro=self.context().qa_macro.name, context=self.context_name))
            width_counter = 0  # If more than 6 markers per category, new horizontal must be created
            for name_cat in self.marker_spec:
                if name_cat['category'] == category:
                    width_counter += 1
                    if width_counter > self.markers_per_row:
                        end_list.append("""}}
                                    Horizontal {{
                                        alignX = Left
                                        expandX = yes
                                        visibleOn = {qa_macro}.{control_name}__{cat}
                                    """.format(cat=category,
                                               control_name=self.control_name,
                                               qa_macro=self.context().qa_macro.name))
                        width_counter = 1
                    end_list.append("""
                          ToolButton {{
                              title = "{marker_title}"
                              name = {control_name}_place_{marker_name}
                              image = "{local}/Modules/Resources/Icons/marker.png"
                              scaleIconSetToMinSize = False
                              mw = 18
                              mh = 18
                              tooltip = "Place {marker_title} marker"
                              command = "*py:get_control('{context}', '{name}').place_marker('{marker_name}')*"
                          }}""".format(marker_name=name_cat['name'],
                                       marker_title=name_cat['title'],
                                       name=self.name,
                                       control_name=self.control_name,
                                       local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
                                       context=self.context_name,
                                       cat=name_cat['category']))
            end_list.append("""
                          HSpacer { expandY = True } 
                        }
                    }
                        """)

        end_list.append("""Horizontal {{ 
                  ToolButton {{
                      name = {control_name}_jump
                      image = "{local}/Modules/Resources/Icons/edit-redo.png"
                      scaleIconSetToMinSize = False
                      mw = 18
                      mh = 18
                      tooltip = "Jump to marker"
                      command = "*py:get_control('{context}', '{name}').jump()*"
                  }}
                  ToolButton {{
                      name = {control_name}_delete
                      image = "{local}/Modules/Resources/Icons/eraser-16.png"
                      scaleIconSetToMinSize = False
                      mw = 18
                      mh = 18
                      tooltip = "Delete selected marker"
                      command = "*py:get_control('{context}', '{name}').delete_marker()*"
                  }}
                  SpacerY {{ expandX = Yes }}
                  }}
                  Label {{ name = {control_name}_label }}
                  FieldListener {listentarget} {{ command = "*py:get_control('{context}', '{name}').placed()*" }}
                  FieldListener {editing_field} {{ command = "*py:get_control('{context}', '{name}').update_icon()*" }}
                  {base_mdl}
                  Execute = "*py:get_control('{context}', '{name}').wakeup()*"
            }}""".format(name=self.name,
                         control_name=self.control_name,
                         context=self.context_name,
                         local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
                         label=label,
                         x=self.x,
                         y=self.y,
                         listentarget=self.marker_editor.marker_field.fullName(),
                         editing_field=self.marker_editor.editing_field.fullName(),
                         base_mdl=self.base_mdl))
        mdl = ''.join(end_list)
        return mdl

    @property
    def value(self):
        return self.data

    @value.setter
    def value(self, value):
        self._log.info('Setting markerEdit value to: {}'.format(value))
        if value is None:
            value = []

        self.data = value
        self.update("<MarkerEdit value.setter>")
        self.update_label()
        self.update_icon()
        self.activate()

    def activate(self):
        for field in self.so_group.children():
            if field.objectName == 'child':
                field.disconnectAll()

        self.update_all_markers()
        self.so_group.field("child").connectFrom(self.marker_editor.output)
        self.so_group.field("child").connectFrom(self.all_markers.output)

    def place_marker(self, name):
        self.marker_editor.editing_on = not self.marker_editor.editing_on
        if self.marker_editor.editing_on:
            self._log.info("Place marker ON; Marker: {}".format(name))
            self.active_marker_name = name
            self.start_edit()
        elif name != self.active_marker_name:
            self.marker_editor.editing_on = not self.marker_editor.editing_on
            self._log.info("Switching marker; Place marker ON; Marker: {}".format(name))
            self.active_marker_name = name
            self.start_edit()
        else:
            self._log.info("Place marker OFF")
            self.stop_edit()

        self.update_icon()

    def update_icon(self):
        if self.data is None:
            placed_markers = {}
        else:
            placed_markers = set(x['name'] for x in self.data)

        for name_cat in self.marker_spec:
            name = name_cat['name']
            if self.marker_editor.editing_on and name == self.active_marker_name:
                globals.MAIN_WINDOW.control("{}_place_{}".format(self.control_name, name)).setPixmapFile(
                    ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker-yellow.png")
            elif name in placed_markers:
                globals.MAIN_WINDOW.control("{}_place_{}".format(self.control_name, name)).setPixmapFile(
                    ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker-green.png")
            else:
                globals.MAIN_WINDOW.control("{}_place_{}".format(self.control_name, name)).setPixmapFile(
                    ctx.expandFilename("$(MLAB_EMC_ViewR)") + "/Modules/Resources/Icons/marker.png")

    def placed(self):
        if not self.marker_editor.editing_on:
            self._log.info("Placed called when editor is not active, ignoring!")
            return

        if self.data is not None and self.marker_editor.marker_object.size() == len(self.data):
            self._log.info("Placed called when no new marker was added, ignoring!")
            return
        self.stop_edit()

        # Get date from output of SoView2DMarkerEditor
        self.data = self.marker_editor.markers

        # Set marker name
        # Step 1: Get index of latest added marker
        marker_index = self.marker_editor.editor_module.field('actionIndex').value

        for marker in self.marker_spec:
            if marker["name"] == self.active_marker_name:
                marker_spec = marker
                break
        else:
            # Cannot figure out marker spec
            marker_spec = {}

        # Step 2: Give name and type
        self.data[marker_index]["name"] = self.active_marker_name
        self.data[marker_index]["type"] = marker_spec.get("type", 0)
        self._log.info("Gave {} the name {}".format(str(marker_index), self.active_marker_name))

        # Check for and remove doubles
        to_remove = []
        for index, marker in enumerate(self.data):
            self._log.info("Checking marker {} for doubles: {} vs {}".format(str(index), marker["name"],
                                                                             self.active_marker_name))
            if (marker["name"] == self.active_marker_name and index != marker_index) or not marker["name"]:
                to_remove.append(index)
                self._log.info("Old marker {} to be removed. Its index was: {}".format(self.active_marker_name,
                                                                                       str(index)))

        to_remove.reverse()
        for index in to_remove:
            del self.data[index]  # remove old marker
            self._log.info("Removed marker. Its index was: {}".format(str(index)))

        self.update("<MarkerEdit placed>")
        ctx.callLater(0.1, self.select_next, [])

    def jump(self):
        if self.data is None:
            self._log.warning('Cannot jump to marker, no data!')
            return

        if self.active_marker_name is None:
            self._log.warning('Cannot jump to marker, no marker active!')
            return

        for index, marker in enumerate(self.data):
            if marker["name"] == self.active_marker_name:
                position = index
                break
        else:
            self._log.warning('Cannot jump to non-existing marker!')
            return

        if len(self.data) > 1:
            self._log.warning('Multiple markers found, jumping to first marker')

        position = self.data[position]["pos"]
        self._log.info('Marker position in world coordinates: {}'.format(position))
        for vp in self.context().viewport_modules.values():
            vp.set_position(position)

        self.marker_editor.trigger_update()

    def display_value(self, value):
        message = "{} / {} markers".format(
            len(value) if value else 0,
            len(self.marker_spec) if self.marker_spec else 0
        )
        return message

    def update_label(self):
        globals.MAIN_WINDOW.control(self.control_name + "_label").setTitle(self.display_value(self.value))

    def update(self, caller):
        self._log.info('Calling update from {}!'.format(caller))

        # Update data in marker editor
        self.marker_editor.markers = self.data

        self.marker_editor.trigger_update()
        self.update_label()
        self.field.touch()
        self.update_all_markers()

    def select_next(self):
        # Select the next marker placer
        if self.active_marker_name is not None:
            marker_index = self.marker_names.index(self.active_marker_name)
        else:
            marker_index = 0

        if marker_index < len(self.marker_names) - 1:
            new_index = marker_index + 1
            self._log.info("Calling place_marker with name: {}".format(self.marker_names[new_index]))
            old_cat = self.marker_spec[marker_index]['category']
            new_cat = self.marker_spec[new_index]['category']
            self.get_qa_field('{control_name}__{cat}'.format(cat=new_cat, control_name=self.control_name)).value = True
            if old_cat != new_cat:
                self.get_qa_field('{control_name}__{cat}'.format(cat=old_cat, control_name=self.control_name)).value = False
            self.place_marker(self.marker_names[new_index])

    def close_other_cat(self, cat):
        # Close other categories
        if self.get_qa_field('{name}__{cat}'.format(cat=cat, name=self.control_name)).value:
            self._log.info("Close other than: " + cat)
            for category in self.marker_cat:
                if category != cat:
                    self.get_qa_field('{name}__{cat}'.format(cat=category, name=self.control_name)).value = False

    def delete_marker(self):
        # Delete selected marker
        if not self.marker_editor.editing_on or self.active_marker_name is None:
            self._log.info("Delete_marker called when no marker was selected; Ignoring")
            return

        for index, marker in enumerate(self.data):
            if marker["name"] == self.active_marker_name:
                del self.data[index]
                self.update("<Delete button>")
                break

    def wakeup(self):
        self.update_label()
        globals.MAIN_WINDOW.control(self.control_name + "_jump").setEnabled(True)
        self.get_qa_field('{name}__{cat}'.format(cat=self.marker_cat[0], name=self.control_name)).value = True
