from ..globals import ctx


class PixelEditorModule(object):
    def __init__(self, module_name, group=None):
        self.module_name = module_name

        self.overlay_connection = None
        self.viewport_pointer_connection = None
        self.viewport_image_connection = None

        # Create required modules
        self.save_module = ctx.addModule("itkImageFileWriter")
        self.save_module.name = self.save_module_name

        self.editor_module = ctx.addModule("Draw3D")
        self.editor_module.name = self.editor_module_name
        self.editor_module.field("drawMode").value = "Voxel"

        self.position_module = ctx.addModule("SoView2DPosition")
        self.position_module.name = self.position_module_name
        self.position_module.field("drawingModel").value = "DRAWINGMODEL_VOXEL_RECT"
        self.position_module.field("drawingModelSize").value = 9
        self.position_module.field("drawingModelSizeUnit").value = "SIZEUNIT_VOXEL"

        # Create links
        self.save_module.field('input0').connectFrom(self.editor_module.field('output0'))
        self.editor_module.field('positionInput').connectFrom(self.position_module.field('worldPosition'))

        # Add to correct group
        if group:
            self.save_module.addToGroup(group)
            self.editor_module.addToGroup(group)
            self.position_module.addToGroup(group)

    @property
    def save_module_name(self):
        return self.module_name + "Save"

    @property
    def editor_module_name(self):
        return self.module_name

    @property
    def position_module_name(self):
        return self.module_name + "Position"

    @property
    def pointer_output(self):
        return self.position_module.field("self")

    @property
    def output(self):
        return self.editor_module.field("output0")

    @property
    def input(self):
        return self.editor_module.field("input0")

    def undo_all(self):
        self.editor_module.field("undoAll").touch()

    def save_annotation(self, filename):
        pass

    def clean(self):
        if self.save_module:
            self.save_module.remove()
            self.save_module = None

        if self.editor_module:
            self.editor_module.remove()
            self.editor_module = None

        if self.position_module:
            self.position_module.remove()
            self.position_module = None
