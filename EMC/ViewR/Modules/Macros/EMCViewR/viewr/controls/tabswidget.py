from . import Control


class TabsWidget(Control):
    def __init__(self, context, name, field):
        super(TabsWidget, self).__init__(context, name, field)
        self.content = field['content']
        self.order = field.get('order', list(self.content.keys()))
        self.data = []
        self.current_index = None
        # Add content to the list of qa fields.
        for tab_content in self.content.values():
            context.add_qa_fields(tab_content)

    def update(self, caller):
        self._log.info("updated called from: {}".format(caller))
        data = self.value
        for tab_index, tab_name in enumerate(self.order):
            tab_content = self.content[tab_name]

            for qa_field in tab_content:
                control = self.context().qa_fields[qa_field]
                if control.has_alert(data[qa_field]):
                    self.control.setTabText(tab_index, tab_name + '*')
                    break
            else:
                self.control.setTabText(tab_index, tab_name)

        # Broadcast a trigger for possible listeners
        self.field.touch()

    @property
    def mdl(self):
        callback = "get_control('{context}', '{name}').update({{caller}})".format(context=self.context_name,
                                                                                  name=self.name)
        field_listeners = "\n".join(
            self.context().qa_fields[x].listener_mdl(callback) for tab_content in self.content.values() for x in tab_content
        )

        content = []

        for tab_name in self.order:
            tab_content = self.content[tab_name]
            tab_mdl = '\n'.join(self.context().qa_fields[qa_field].mdl for qa_field in tab_content.keys())

            content.append("""Grid "{control_name}_{tab_name}" {{
                           tabTitle = "{tab_name}"
                           {content}
                        }} """.format(control_name=self.control_name, tab_name=tab_name, content=tab_mdl))

        return """    TabView {{
        name = {control_name} x = {x} y = {y}

        {content}

        {field_listeners}

        }}
        """.format(
            control_name=self.control_name,
            content='\n'.join(content),
            field_listeners=field_listeners,
            x=self.x,
            y=self.y
        )

    @property
    def value(self):
        return {qa_field: self.context().qa_fields[qa_field].value for tab_name, tab_content in self.content.items() for qa_field in tab_content.keys()}

    @value.setter
    def value(self, value):
        for key in self.content:
            for qa_field in self.content[key]:
                control = self.context().qa_fields[qa_field]

                # Set to no data if value is None
                if value is None:
                    control.value = None
                elif qa_field in value:
                    control.value = value[qa_field]

        self.update("self")

    def has_alert(self, value=None):
        """
        Indicate if there is an alert in this widget (or in a subwidget)
        """
        if value is None:
            value = self.value

        return any(any(self.context().qa_fields[qa_field].has_alert(value[qa_field]) for qa_field in tab_content) for tab_content in self.content.values())
