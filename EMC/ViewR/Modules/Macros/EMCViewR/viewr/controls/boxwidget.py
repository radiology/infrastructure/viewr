from . import Control


class BoxWidget(Control):
    def __init__(self, context, name, field):
        super(BoxWidget, self).__init__(context, name, field)
        self.content = field['content']
        self.align_title = field.get('align_title', 'Left')

        # Add content to the list of qa fields.
        self.context().add_qa_fields(self.content)

    @property
    def mdl(self):
        content_mdl = '\n'.join(self.context().qa_fields[qa_field].mdl for qa_field in self.content.keys())

        return """Box "{title}" {{
                name = {control_name} x = {x} y = {y}
                title = "{title}"
                alignTitle = "{align_title}"
                expandx = Yes
                expandy = Yes

                Grid {{
                           {content}
                        }}

                {base_mdl}
                }}""".format(
            control_name=self.control_name,
            content=content_mdl,
            title=self.label,
            align_title=self.align_title,
            x=self.x,
            y=self.y,
            base_mdl=self.base_arguments()['base_mdl']
        )

    @property
    def value(self):
        return {qa_field: self.context().qa_fields[qa_field].value for qa_field in self.content.keys()}

    @value.setter
    def value(self, value):
        for qa_field in self.content:
            control = self.context().qa_fields[qa_field]

            if value is None:
                control.value = None
            elif qa_field in value:
                control.value = value[qa_field]

    def has_alert(self, value=None):
        """
        Indicate if there is an alert in this widget (or in a subwidget)
        """
        if value is None:
            value = self.value

        return any(self.context().qa_fields[qa_field].has_alert(value[qa_field]) for qa_field in self.content)
