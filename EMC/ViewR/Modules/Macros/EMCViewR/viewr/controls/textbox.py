from . import FieldControl


class TextBox(FieldControl):
    def __init__(self, context, name, field):
        field['type'] = 'String'
        super(TextBox, self).__init__(context, name, field)
        self.editable = field.get('editable', True)

    def arguments(self):
        arg = self.base_arguments()
        arg['editable'] = 'Yes' if self.editable else 'No'
        return arg

    @property
    def mdl(self):
        return """Horizontal {{ x = {x} y = {y}
          TextView {field} {{ name = "{control_name}"
                              autoApply = true
                              visibleRows = 5
                              title = "{label}"
                              edit = {editable}
          }}
          {base_mdl}
        }}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*" """.format(**self.arguments())

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self.field.setStringValue(value or "")

    def wakeup(self):
        self.set_initial_value()
