from . import FieldControl


class ComboBox(FieldControl):
    def __init__(self, context, name, field):
        field['type'] = 'Enum'
        super(ComboBox, self).__init__(context, name, field)
        self.options = field['options']
        self.value_to_option = {str(x): x for x in self.options}

    @property
    def valid(self):
        return self.value in self.options

    def get_initial_value(self):
        if self.initial_value:
            return self.initial_value
        else:
            return self.value

    def arguments(self):
        arg = self.base_arguments()
        arg['items'] = self.get_items()
        return arg

    def get_items(self):
        s = 'items { item "" { title = "" }'
        for option in self.options:
            s += 'item "{name}" {{ title = "{name}" }} '.format(name=option)
        s += ' }'
        return s

    @property
    def mdl(self):
        mdl = """
        Horizontal {{ x = {x} y = {y}
            Label = "{label}"

            ComboBox {field} {{
              name = {control_name}
              expandx = Yes
              editable = false

              activatedCommand = "*py:get_control('{context}', '{name}').drop_empty()*"

            }}
            {base_mdl}
        }}
    Execute = "*py:get_control('{context}', '{name}').wakeup()*"
    """
        return mdl.format(**self.arguments())

    def drop_empty(self):
        if self.control.currentItem() != 0 and self.control.itemText(0) == '':
            self.control.removeItem(0)

    @property
    def index(self):
        try:
            return self.options.index(self.value)
        except ValueError:
            return None

    @property
    def value(self):
        if self.control is not None:
            return self.value_to_option[self.field.value] if self.field.value in self.value_to_option else None

    @value.setter
    def value(self, value):
        if self.control is None:
            return

        if value is None:
            return

        if value not in self.options:
            self._log.error('Invalid value "{}" for options {}'.format(value, self.options))

            # Add the empty back (this should only happen from scripting)
            # self.control.setItems([""] + self.options)
            self.control.setCurrentItem(0)
            return

        # self.control.setItems(self.options)
        index = self.options.index(value)
        self.control.setCurrentItem(index)
        self.field.setValue(value)

    def wakeup(self):
        self.set_initial_value()
        self.drop_empty()
