from . import FieldControl


class RadioButtonGroup(FieldControl):
    def __init__(self, context, name, field):
        if not field['type']:
            field['type'] = 'Enum'
        super(RadioButtonGroup, self).__init__(context, name, field)
        self.options = field['options']
        self.orientation = field.get('orientation', 'Horizontal')

    @property
    def valid(self):
        return self.value is not None

    def get_items(self):
        s = "items { "
        for option in self.options:
            s += """item "{name}" {{ title = "{name}" }} """.format(name=option)
        s += " }"
        return s

    def get_orientation(self):
        return self.orientation

    def arguments(self):
        arg = self.base_arguments()
        arg['items'] = self.get_items()
        arg['orientation'] = self.get_orientation()
        return arg

    @property
    def mdl(self):
        mdl = """
    RadioButtonGroup {field} {{
      name = "{control_name}"
      title = "{label}"
      exclusiveButtons = Yes
      x = {x}
      y = {y}
      orientation = "{orientation}"

      {items}

      {base_mdl}
    }}
    Execute = "*py:get_control('{context}', '{name}').wakeup()*"
    """
        return mdl.format(**self.arguments())

    @property
    def value(self):
        if self.control is None:
            return None

        for button_name in self.control.getButtonNames():
            if self.control.isButtonChecked(button_name):
                return button_name
        else:
            self._log.warning("No button is checked.")
            return None

    @value.setter
    def value(self, value):
        if self.control is None:
            return

        if value in self.control.getButtonNames():
            self.control.setButtonChecked(value, True)
            self.field.setValue(value)
        elif value is None:
            self.control.setExclusiveButtons(False)
            for button in self.control.getButtonNames():
                self.control.setButtonChecked(button, False)
            self.control.setExclusiveButtons(True)
        else:
            self._log.error("initial value ({}) is not one of the available options: {}".format(value, ", ".join(self.control.getButtonNames())))

    def wakeup(self):
        self.set_initial_value()
