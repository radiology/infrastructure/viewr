from abc import ABCMeta, abstractproperty, abstractmethod
import weakref

from .. import globals, log

ctx = globals.ctx


def parse_color(value):
    if isinstance(value, str):
        if value[0] == '#':
            value = value[1:]

        # Convert hex to tuple of floats
        value = (value[:2], value[2:4], value[4:6])
        value = tuple(float.fromhex(x) / 255 for x in value)

    # Check if color is valid
    if not (isinstance(value, (list, tuple))
            and len(value) == 3
            and all(isinstance(x, (int, float)) for x in value)):
        raise ValueError('The color should be a hex string or a list of 3 numerical elements')

    # The color is defined in the 0 - 255 range rather than 0.0 - 1.0
    if all(isinstance(x, int) for x in value) or any(x > 1.0 for x in value):
        value = tuple(float(x) / 255.0 for x in value)

    return value


class Control(metaclass=ABCMeta):
    """
    The Control is the base class for all ViewR widgets. It takes care of the
    basic functionality shared by all widgets. To implement a widget, a few
    methods need to be implemented:

    ======   ========================================================
    method   description
    ======   ========================================================
    value    property for getting and setting the value of the widget
    ======   ========================================================

    Subclasses have access to the following convience methods/properties:

    ===============   ========================================================
    name              description
    ===============   ===================================================================
    context           weakref to the parent context object, retrieve using self.context()
    name              the name of the widget
    control_name      name of the MeVisLab control for this widget
    ===============   ===================================================================


    """

    def __init__(self, context, name, field):
        self.context = weakref.ref(context)

        self._log = log.config_logger(name, level=20, filename=None)
        self._log.info('Creating control [{} {}] {}'.format(name, type(self), field))
        self.name = name
        try:
            self.type = field['type']
        except KeyError:
            self.type = 'String'
            field['type'] = 'String'
            self._log.warning('{} has not type, defaulting to String'.format(self.name))
        self.label = field['label']
        self.initial_value = field.get('initial_value', None)
        self.x = field['x']
        self.y = field['y']
        self.depends_on = field.get('depends_on')
        self.enabled = field.get('enabled', True)
        self.visible = field.get('visible', True)
        self.visible_on = field.get('visible_on')
        self.documentation = field.get('documentation', '')

    @property
    def control_name(self):
        return '{}_{}'.format(self.context_name, self.name)

    @property
    def context_name(self):
        return self.context().name

    def display_value(self, value):
        return value

    def clean(self):
        """
        Allows fields to do some cleanup if required.
        """
        pass

    def has_alert(self, value=None):
        """
        Indicate if there is an alert in this widget (or in a subwidget)
        """
        return False

    @property
    def valid(self):
        return True

    def base_arguments(self):
        arg = dict()
        arg['name'] = self.name
        arg['control_name'] = self.control_name
        arg['context'] = self.context_name
        arg['label'] = self.label
        arg['x'] = self.x
        arg['y'] = self.y
        arg['base_mdl'] = self.base_mdl
        return arg

    @abstractproperty
    def value(self):
        """
        The value of this control, need getter and setter, the setter needs to
        accept None as an empty value and set the widget to a blank state.
        """

    @property
    def control(self):
        """
        The MeVisLab control underlying this python Control class
        """
        return globals.MAIN_WINDOW.control(self.control_name)

    def wakeup(self):
        pass

    def get_initial_value(self):
        return self.initial_value

    def set_initial_value(self):
        self.value = self.get_initial_value()

    def get_qa_field(self, name):
        """
        Get a field for a qa field by fieldname

        :param str name: name of the control (Control.control_name will give correct field name)
        :return:
        """
        return self.context().qa_macro.field(name)

    @property
    def field(self):
        """
        The MeVisLab Field connected to this Control class
        """
        return self.get_qa_field(self.name)

    @property
    def base_mdl(self):
        """
        The base MDL for a control (visibile_on and depends_on)
        """
        return "\n".join([self.enabled_mdl, self.depends_on_mdl, self.visible_mdl, self.visible_on_mdl])

    @property
    def enabled_mdl(self):
        """
        MDL for enabled/disabled widgets
        """
        if self.enabled:
            return "enabled = Yes"
        else:
            return "enabled = No"

    @property
    def visible_mdl(self):
        """
        The MDL for visible
        """
        if self.visible:
            return "visible = Yes"
        else:
            return "visible = No"

    @property
    def depends_on_mdl(self):
        """
        The MDL for depends on
        """
        if self.depends_on is None or not self.enabled:
            return ''
        elif '$' in self.depends_on:
            return 'dependsOn = "* {} *"'.format(self.depends_on.replace("$", "{}.".format(
                self.context().qa_macro.name
            )))
        else:
            return 'dependsOn = "* {}.{} *"'.format(
                self.context().qa_macro.name,
                self.depends_on
            )

    @property
    def visible_on_mdl(self):
        """
        The MDL for visible on
        """
        if self.visible_on is None or not self.visible:
            return ''
        elif '$' in self.visible_on:
            return 'visibleOn = "* {} *"'.format(self.visible_on.replace("$", "{}.".format(
                self.context().qa_macro.name,
            )))
        else:
            return 'visibleOn = "* {}.{} *"'.format(
                self.context().qa_macro.name,
                self.visible_on
            )

    def listener_mdl(self, callback):
        """
        The MDL required to create a listener on this control

        :param str callback: the function to call back
        :return: The MDL string for the FieldListener
        :rtype: str
        """
        callback = callback.format(caller="'{name}'".format(name=self.name))
        field_name = self.field.fullName()
        listener_mdl = """FieldListener {field_name} {{ command = "*py:{callback}*" }}""".format(
            field_name=field_name,
            callback=callback
        )
        return listener_mdl

    def qa_macro_field(self):
        """
        If the child does not have a field, create a trigger field which can
        be listened on. The control should fire the trigger when there has been
        an update

        :return: string with the MDL to create a field
        :rtype: str
        """
        return ("Field {name} {{ type = Trigger }}").format(name=self.name)


class FieldControl(Control):
    def __init__(self, context, name, field):
        super(FieldControl, self).__init__(context, name, field)

        # Add the option to the Enum type
        if field['type'] == 'Enum' and 'options' in field:
            self.options = field['options']
            ctx.callLater(0.0, self.set_enum)

    def base_arguments(self):
        args = super(FieldControl, self).base_arguments()
        args['field'] = self.field.fullName()
        return args

    def set_enum(self):
        self.field.addEnumItem("", "", 0)
        for nr, option in enumerate(self.options):
            self.field.addEnumItem(option, option, nr + 1)

    def qa_macro_field(self):
        return ("Field {name} {{ type = {type} }}").format(name=self.name, type=self.type)


class EditControl(Control):
    @abstractmethod
    def activate(self):
        """
        Make sure the editor is active and linked up properly
        """

    def start_edit(self):
        for control in self.context().qa_fields.values():
            if isinstance(control, EditControl):
                if control is not self:
                    control.editor.editing_on = False
                else:
                    control.editor.editing_on = True

        self.activate()
        ctx.field("EnableNavigation").value = False

    def stop_edit(self):
        self.editor.editing_on = False
        ctx.field("EnableNavigation").value = True

    @abstractproperty
    def editor(self):
        """
        The editor module of which the editing_on can be found
        """


class EditorModule(object):
    @abstractproperty
    def editing_on(self):
        """
        Field that controls if the editor editing is turned on. This should be
        a settable property which takes a boolean.
        """
