from . import FieldControl


class ValidationControl(FieldControl):
    def __init__(self, context, name, field):
        # ValidationControl is always a boolean!
        field['type'] = 'Bool'
        super(ValidationControl, self).__init__(context, name, field)
        self.hint_text = field.get('hint_text', '')

        # Specify condition to check
        self.variables = field.get('variables', {})
        self.condition = field.get('condition', 'None')

        # Define action to be taken
        self.action = field.get('action', 'validate')
        self.args = field.get('args', {})

    def arguments(self):
        arg = self.base_arguments()
        arg['field'] = self.field.fullName()
        arg['hint_text'] = self.hint_text
        return arg

    @property
    def mdl(self):
        arguments = self.arguments()

        callback = "get_control('{context}', '{name}').update()".format(context=self.context_name, name=self.name)
        arguments['field_listeners'] = "\n".join(self.context().qa_fields[x].listener_mdl(callback) for x in self.variables.values())

        return """
        Horizontal {{ x = {x} y = {y}
        Field {field} {{
            name = "{control_name}"
            edit = No
            hintText = "{hint_text}"
        }}
        SpacerX {{}}
        {base_mdl}
        }}

        {field_listeners}

        """.format(**arguments)

    @property
    def valid(self):
        if self.action == 'validate':
            return self.value
        else:
            return True

    @property
    def value(self):
        return self.field.value

    @value.setter
    def value(self, value):
        self._log.info('Cannot set a Validation field, it is read only (auto calculated). Ignoring new value.')

    def update(self):
        values = {key: self.context().qa_fields[value].value for key, value in self.variables.items()}
        result = eval(self.condition, {}, values)

        # Cast to bool
        self.field.value = bool(result)
