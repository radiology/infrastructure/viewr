import datetime
import markdown

import isodate

from .textbox import TextBox
from ..globals import ctx

class ConversationWidget(TextBox):
    def __init__(self, context, name, field):
        super(TextBox, self).__init__(context, name, field)
        self._history = None
        self.initial_value = None
        self.editable = field.get('editable', True)

    def arguments(self):
        arg = self.base_arguments()
        arg['editable'] = 'Yes' if self.editable else 'No'
        return arg

    @property
    def mdl(self):
        return """Box "{label}" {{ x = {x} y = {y}
          Vertical {{
            HyperText {{ name = "{control_name}_initial"
                         frameShape = Box
                         expandY = true
            }}
            TextView {field} {{ name = "{control_name}"
                                autoApply = true
                                visibleRows = 5
                                edit = {editable}
            }}
          }}
          {base_mdl}
        }}
        Execute = "*py:get_control('{context}', '{name}').wakeup()*" """.format(**self.arguments())

    @property
    def value(self):
        history = self._history or []

        if self.field.value:
            return history + [{
                'user': ctx.field('TaskmanagerUser').value,
                'timestamp': datetime.datetime.now().isoformat(),
                'value': self.field.value,
            }]
        else:
            return history

    @value.setter
    def value(self, value):
        # Adapt for baseline
        if isinstance(value, str):
            context = self.context()

            print('Network rater history: {}'.format(context.rater_history))
            if len(context.rater_history) > 0:
                user = context.rater_history[-1]['username']
                timestamp = context.rater_history[-1]['timestamp']
            else:
                user = 'unknown'
                timestamp = '2000-01-01T00:00:00.000000'

            value = [{
                'user': user,
                'timestamp': timestamp,
                'value': value,
            }]

        if value is None:
            value = []

        # If there is only one part, it means there is not a valid split in
        # there yet, so it must be initial data
        if len(value) == 0:
            self._history = []
            ctx.control('{}_initial'.format(self.control_name)).setText("")
            self.field.setStringValue("")
            return

        # Set initial value
        task_loaded = ctx.field('TaskLoaded').value
        task_loaded = isodate.parse_datetime(task_loaded)

        # Check last entry value
        last_split = isodate.parse_datetime(value[-1]['timestamp'])

        if task_loaded < last_split:
            self._history = value[:-1]
            current_text = value[-1]['value']
        else:
            self._history = value[:]
            current_text = ''

        # Set the actual values
        parts = ['\n\n'.join([
            "### {}".format(x['user']),
            "_{}_".format(x['timestamp'][:19].replace('T', ' ')),
            x['value']]) for x in self._history]
        html_text = markdown.markdown('\n\n---\n\n'.join(parts))
        ctx.control('{}_initial'.format(self.control_name)).setText(html_text)
        self.field.setStringValue(current_text)

    def wakeup(self):
        self.set_initial_value()
