import collections

from .. import log, globals

from . import EditorModule, EditControl, parse_color

ctx = globals.ctx


class CSOEditorModule(EditorModule):
    def __init__(self, module_name, group):
        self.module_name = module_name
        self.group_mapping = {}

        # Create CSO editor
        self.manager_module = ctx.addModule("CSOManager")
        self.manager_module.name = self.manager_module_name

        # Create spline editor
        self.spline_editor_module = ctx.addModule("SoCSOSplineEditor")
        self.spline_editor_module.name = self.spline_editor_module_name
        self.spline_editor_module.field('addCSOToGroupMode').value = 'GROUP_BY_ID'

        # Create CSO extensible editor
        self.editor_module = ctx.addModule("SoView2DCSOExtensibleEditor")
        self.editor_module.name = self.editor_module_name

        # Add correct connections
        self.editor_module.field("inCSOList").connectFrom(self.manager_module.field("outCSOList"))
        self.editor_module.field("inExtensions").connectFrom(self.spline_editor_module.field("self"))

        # Add to correct group
        if group:
            self.manager_module.addToGroup(group)
            self.spline_editor_module.addToGroup(group)
            self.editor_module.addToGroup(group)

        # Start editor deactivated
        self.editing_on = False

        self._log = log.config_logger('CSOEditorModule', level=10, filename=None)
        self.manager_module.field("useFinishedCSONotification").value = True

    def clean(self):
        # Clean created modules
        if self.manager_module_name:
            self.manager_module.remove()
            self.manager_module = None
        if self.spline_editor_module_name:
            self.spline_editor_module.remove()
            self.spline_editor_module = None
        if self.editor_module_name:
            self.editor_module.remove()
            self.editor_module = None

    @property
    def csos(self):
        csos = []
        for cso in self.cso_list.getCSOs():
            # Get seed points and set them as triplets instead of flat list
            seed_points = cso.getSeedPoints()
            iterator = iter(seed_points)

            data = {
                'seed_points': list(zip(iterator, iterator, iterator)),
            }
            csos.append(data)

        return csos

    @csos.setter
    def csos(self, value):
        self.remove_csos()
        if value:
            for cso in value:
                self.cso_list.addClosedSpline(cso['seed_points'], True, 1.0, "spline")
                self.cso_list.notifyObservers(self.cso_list.NOTIFICATION_CSO_FINISHED)

    def add_cso(self, value, group=None):
        cso_obj = self.cso_list.addClosedSpline(value['seed_points'], True, 1.0, "spline")
        if group is not None:
            cso_obj.removeFromAllGroups()
            group_id = self.group_mapping[group].id
            cso_obj.addToGroup(group_id)
        self.cso_list.notifyObservers(self.cso_list.NOTIFICATION_CSO_FINISHED)

    @property
    def cso_field(self):
        return self.manager_module.field("outCSOList")

    @property
    def editing_field(self):
        return self.editor_module.field("editingOn")

    @property
    def editing_on(self):
        return self.editing_field.value

    @editing_on.setter
    def editing_on(self, value):
        self.editing_field.value = value

    @property
    def cso_list(self):
        return self.cso_field.object()

    @property
    def manager_module_name(self):
        if self.module_name is not None:
            return self.module_name + "Manager"
        else:
            return None

    @property
    def spline_editor_module_name(self):
        if self.module_name is not None:
            return self.module_name + "SplineEdit"
        else:
            return None

    @property
    def editor_module_name(self):
        if self.module_name is not None:
            return self.module_name + "Editor"
        else:
            return None

    @property
    def output(self):
        return self.editor_module.field("self")

    def remove_csos(self):
        self.cso_list.removeCSOs()

    def undo(self):
        self.manager_module.field("undo").touch()


class CSOEdit(EditControl):
    def __init__(self, context, name, field):
        super(CSOEdit, self).__init__(context, name, field)
        self.all_csos = context.cso_editor
        self.so_group = context.cso_so_group
        self.cso_type = field.get('cso_type', 0)

        self.data = None
        self.cso_editor = CSOEditorModule('{}_{}_editor'.format(context.name, self.name), context.group_name)

        self.cso_editor.cso_list.csoEventReceived.connect(self.cso_event_callback)

        groups = field.get('groups', None)
        if isinstance(groups, list):
            self.groups = collections.OrderedDict((x['label'], x) for x in groups)
        elif isinstance(groups, dict):
            self.groups = collections.OrderedDict(groups.items())
        else:
            self.groups = None

        self.group_label = field.get('group_label', 'group')
        self.has_combo_control = False

        if isinstance(self.groups, dict):
            for group_id, group_data in self.groups.items():
                group = self.cso_editor.cso_list.addGroup(group_id)
                group.setDescription(group_data.get('description', ''))
                self.cso_editor.group_mapping[self.name, group_id] = group

                # Also add to all_csos
                all_group = self.all_csos.cso_list.addGroup(group_id)
                all_group.setDescription(group_data.get('description', ''))
                self.all_csos.group_mapping[self.name, group_id] = all_group

                if 'color' in group_data:
                    color = parse_color(group_data['color'])

                    group.setPathPointColor(color)
                    group.setUsePathPointColor(True)
                    group.setSeedPointColor(color)
                    group.setUseSeedPointColor(True)

                    all_group.setPathPointColor(color)
                    all_group.setUsePathPointColor(True)
                    all_group.setSeedPointColor(color)
                    all_group.setUseSeedPointColor(True)

    @property
    def editor(self):
        return self.cso_editor

    def qa_macro_field(self):
        return "Field {name} {{ type = String }}\nField {name}_update {{ type = Trigger }}".format(name=self.name)

    def cso_event_callback(self, event):
        # Filter events
        if event.isPostCommand():
            if event.eventType() in [45, 46]:
                ctx.callLater(0.0, self.cso_updated)

    def clean(self):
        self.all_csos.remove_csos()
        self.cso_editor.clean()

    def display_value(self, value):
        if value is None:
            return None

        return value['group']

    def find_csos(self, ignore=None):
        return self.context().find_csos(ignore=ignore)

    @property
    def mdl(self):
        combo_box = ""
        field_listener = ""
        if isinstance(self.cso_type, str):
            type_field = self.context().qa_fields.get(self.cso_type)
            caller = type_field.field.fullName()
            field_listener = """FieldListener {caller} {{
             command = "*py:get_control('{context}', '{name}').update()*"
            }}""".format(caller=caller, context=self.context_name, name=self.name)
        elif isinstance(self.groups, dict) and len(self.groups) > 1:
            self.has_combo_control = True
            items = '\n'.join(
                'item {{ title = "{name}" }} '.format(name=x['label']) for x in self.groups.values()
            )

            combo_box = """
            Horizontal {{
                Label = "{label}"
                ComboBox {field} {{
                  name = {control_name}_type
                  expandx = Yes
                  editable = false
                  activatedCommand = "*py:get_control('{context}', '{name}')._update_group()*"

                  items {{
                    {items}
                  }}
                }}
            }}
            """.format(items=items, context=self.context_name, control_name=self.control_name,
                       name=self.name, label=self.group_label, field=self.field.fullName())

        if self.label:
            label = 'Label = "{label}"'.format(label=self.label)
        else:
            label = ""

        return """
          Vertical {{ x = {x} y = {y}
            name = {control_name}
            Horizontal {{
              alignX = Left
              {label}
              ToolButton {editing_field} {{
                  name = {control_name}_edit
                  image = "{local}/Modules/Resources/Icons/applications-graphics.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Edit CSO"
                  command = "*py:get_control('{context}', '{name}').toggle_edit()*"
                  {enabled_mdl}
              }}
              ToolButton {{
                  name = {control_name}_jump
                  image = "{local}/Modules/Resources/Icons/edit-redo.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Jump to CSO (first seed point)"
                  command = "*py:get_control('{context}', '{name}').jump()*"
              }}
              ToolButton {{
                  name = {control_name}_delete
                  image = "{local}/Modules/Resources/Icons/trash-16.png"
                  scaleIconSetToMinSize = False
                  mw = 18
                  mh = 18
                  tooltip = "Remove all CSO data for current finding"
                  command = "*py:get_control('{context}', '{name}').remove_csos()*"
                  {enabled_mdl}
              }}
              Label {{ name = {control_name}_label }}

              Execute = "*py:get_control('{context}', '{name}').wakeup()*"
            }}

            {combo_box}
            {base_mdl}

            {field_listener}
            FieldListener {listenlabel} {{ command = "*py:get_control('{context}', '{name}')._update_group()*" }}
            FieldListener {listenlabel} {{ command = "*py:get_control('{context}', '{name}')._update_label()*" }}
            FieldListener {listenlabel}_update {{ command = "*py:get_control('{context}', '{name}')._update_group()*" }}
            FieldListener {listenlabel}_update {{ command = "*py:get_control('{context}', '{name}')._update_label()*" }}
            
            {visible_mdl}
          }}
        """.format(context=self.context_name,
                   name=self.name,
                   control_name=self.control_name,
                   local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
                   label=label,
                   x=self.x,
                   y=self.y,
                   field_listener=field_listener,
                   listenlabel=self.field.fullName(),
                   editing_field=self.cso_editor.editing_field.fullName(),
                   combo_box=combo_box,
                   base_mdl=self.base_mdl,
                   enabled_mdl='{}\n{}'.format(self.enabled_mdl, self.depends_on_mdl),
                   visible_mdl='{}\n{}'.format(self.visible_mdl, self.visible_on_mdl),
                   )

    @property
    def value(self):
        return {
            'csos': self.data,
            'group': self.current_group,
        }

    @value.setter
    def value(self, value):
        self._log.info('Setting CSOEdit value to: {}'.format(value))

        if value is None:
            cso_value = []
        else:
            # TODO: clean this code
            cso_value = value['csos']

        self.data = cso_value
        self.cso_editor.csos = self.data
        self.update()
        self.activate()

    def activate(self):
        for field in self.so_group.children():
            if field.objectName == 'child':
                field.disconnectAll()

        self._update_all_csos()
        self.so_group.field("child").connectFrom(self.cso_editor.output)
        self.so_group.field("child").connectFrom(self.all_csos.output)

    def cso_updated(self):
        self.data = self.cso_editor.csos
        self.context().qa_macro.field('{}_update'.format(self.name)).touch()

    def toggle_edit(self):
        if self.editor.editing_on:
            # Turn editing off
            self.start_edit()
        else:
            # Turn editing on
            self.stop_edit()

    def jump(self):
        if self.data is None or len(self.data) == 0:
            self._log.warning('Cannot jump to non-existing CSO!')
            return

        position = self.data[0]['seed_points'][0]
        self._log.info('CSO first seed position in world coordinates: {}'.format(position))
        self.context().set_position(position[:3])

    def remove_csos(self):
        self.cso_editor.remove_csos()
        self.cso_updated()
        self.field.touch()

    @property
    def group_control(self):
        if self.has_combo_control:
            return globals.MAIN_WINDOW.control("{}_type".format(self.control_name))
        else:
            return None

    @property
    def current_group(self):
        if isinstance(self.cso_type, int):
            return self.groups.keys()[self.cso_type]
        elif isinstance(self.cso_type, str):
            context = self.context()
            if self.cso_type in context.qa_fields:
                return context.qa_fields[self.cso_type].value
            else:
                return self.cso_type

    def _update_group(self):
        if isinstance(self.groups, dict) and self.groups:
            current_group = self.current_group
            if current_group is not None:
                group_index = self.cso_editor.cso_list.getGroupByLabel(current_group).id
            else:
                group_index = 0

            self.cso_editor.spline_editor_module.field('addCSOToGroupId').value = group_index

            cso_list = self.cso_editor.cso_list

            for cso in cso_list.getCSOs():
                cso.removeFromAllGroups()
                cso.addToGroup(group_index)

            cso_list.notifyObservers(cso_list.NOTIFICATION_GROUP_FINISHED)

    def _update_all_csos(self):
        ignore = self.value
        self.update_all_csos(ignore=ignore)

    def update_all_csos(self, ignore=None):
        self.context().update_all_csos(ignore=ignore)

    def _update_label(self):
        if self.data is None or len(self.data) == 0:
            message = "No CSO created!"
        else:
            message = "{} parts, {} points".format(len(self.data), sum(len(x['seed_points']) for x in self.data))
        globals.MAIN_WINDOW.control(self.control_name + "_label").setTitle(message)

    def update(self):
        self._update_group()
        self._update_label()
        self._update_all_csos()
