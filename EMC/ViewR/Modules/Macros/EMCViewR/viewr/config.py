import getpass
import json
import os
import netrc

from . import globals
from .helpers import update_netrc_entry, get_netrc

import taskclient
from taskclient.exceptions import TaskManagerConnectionError, TaskManagerNoAuthError

ctx = globals.ctx


class JSONConfig(object):
    def __init__(self):
        self.filename = None

        # Servers field
        self.servers = []

    def read_config(self, filename):
        self.filename = filename
        with open(self.filename, 'r') as fh:
            config_data = json.load(fh)

        self.servers = config_data.get('servers', [])

    def write_config(self):
        config_data = {
            'servers': self.servers
        }

        with open(self.filename, 'w') as fh:
            json.dump(config_data, fh, indent=2)

    @property
    def list_control(self):
        return ctx.control("config_server_list")

    def update_server_list(self, current_index=None):
        if current_index is None:
            current_index = self.current_index
        if current_index is None:
            current_index = 0
        selected_item = None

        # Add clean and add columns
        list_control = self.list_control
        list_control.clearItems()
        list_control.removeColumns()
        list_control.addColumn("Id")
        list_control.addColumn("Name")
        list_control.addColumn("URL")
        list_control.addColumn("User")

        # Add rows
        for id_, server in enumerate(self.servers):
            selected_item_data = [
                id_,
                server["name"],
                server["url"],
                server["user"],
            ]

            item = list_control.appendItem(None, selected_item_data)

            # Set selected
            if id_ == current_index:
                selected_item = item

        list_control.resizeColumnsToContents()
        if selected_item is not None:
            list_control.ensureItemVisible(selected_item)
            list_control.setCurrentItem(selected_item)

        ctx.field("ServersConfigUpdated").touch()

    @property
    def current_index(self):
        selected_item = self.list_control.selectedItem()
        if not selected_item:
            return None

        return int(selected_item.text(0))

    def change_server(self):
        index = self.current_index or 0

        if index < len(self.servers):
            self.set_server(self.servers[index])
        else:
            self.set_server({
                'name': '',
                'url': '',
                'user': '',
                'project': '',
                'require_tags': [],
                'exclude_tags': [],
            })

    def add_server(self):
        # Add empty server with some default stub
        empty_server = {
            "name": 'TaskManager {}'.format(len(self.servers)),
            "url": 'https://',
            "user": getpass.getuser(),
            "project": '',
            "require_tags": [],
            "exclude_tags": [],
        }

        self.servers.append(empty_server)
        self.update_server_list(current_index=len(self.servers) - 1)
        self.change_server()
        self.write_config()

    def remove_server(self):
        current_index = self.current_index
        self.servers.pop(current_index)
        new_index = min(current_index, len(self.servers) - 1)
        self.update_server_list(current_index=new_index)
        self.change_server()
        self.write_config()

    def test_server(self):
        server = self.get_server()
        server_url = server['url']
        
        try:
            task_client = taskclient.connect(server_url, user_agent_prefix=globals.user_agent_prefix, logger=globals.logger)
            result = '<b>Connection to server was successful</b><br>{}'.format(server_url)
        except TaskManagerNoAuthError as excp:
            globals.task_manager.auth_window = ctx.showWindow("TaskAuthWindow")
            globals.task_manager.auth_window.control("taskmanager_hostname").setTitle(server_url)
            globals.task_manager.auth_window.control("taskmanager_realm").setTitle(excp.payload)
            globals.task_manager.auth_window.control("callback_field").setTitle("TestServer")
            return
        except TaskManagerConnectionError:
            result = '<font color="red"><b>Could not connect to server</b><br>{}</font>'.format(server_url)
        
        # Set result
        ctx.field("ConfigTestResult").value = result
        ctx.showModalDialog("TestResultWindow")

    def import_server(self):
        filepath = globals.MLAB.MLABFileDialog.getOpenFileName(os.path.expanduser('~'), "JSON server files (*.json)", "Open file")
        try:
            with open(filepath, 'r') as fh:
                data = json.load(fh)

            if not isinstance(data, dict):
                globals.logger.warning('No a valid server definition!')
                return

            if 'name' not in data or 'url' not in data or 'user' not in data:
                globals.logger.warning('No a valid server definition! Needs at least a name, url and user')
                return

            self.servers.append({
                'name': data['name'],
                'url': data['url'],
                'user': data['user'],
                "project": data.get('project', ''),
                "require_tags": data.get('require_tags', []),
                "exclude_tags": data.get('exclude_tags', []),
            })
        except ValueError:
            globals.logger.warning('Not a valid JSON file!')

        self.update_server_list(current_index=len(self.servers) - 1)
        self.change_server()
        self.write_config()

    def save_server(self):
        self.servers[self.current_index] = self.get_server()
        self.update_server_list()
        self.write_config()

    def export_server(self):
        data = self.get_server()
        filepath = globals.MLAB.MLABFileDialog.getSaveFileName(os.path.expanduser('~/viewr_{}.json'.format(data['name'])),
                                                  "JSON server files (*.json)", "Save file")

        with open(filepath, 'w') as fh:
            json.dump(data, fh)

    def get_server(self):
        return {
            "name": ctx.control('taskmanager_name').text(),
            "url": ctx.control('taskmanager_url').text(),
            "user": ctx.control('taskmanager_user').text(),
            "project": ctx.control('project_filter').text(),
            "require_tags": [x.strip() for x in ctx.control('require_tags').text().split(',') if x.strip()],
            "exclude_tags": [x.strip() for x in ctx.control('exclude_tags').text().split(',') if x.strip()],
        }

    def set_server(self, server):
        ctx.control('taskmanager_name').setText(server['name'])
        ctx.control('taskmanager_url').setText(server['url'])
        ctx.control('taskmanager_user').setText(server['user'])
        ctx.control('project_filter').setText(server['project'])
        ctx.control('require_tags').setText(', '.join(server['require_tags']))
        ctx.control('exclude_tags').setText(', '.join(server['exclude_tags']))
        ctx.field('SelectedConfigServer').value = server['name']

    def select_server(self):
        server = ctx.control("server_selector").currentText()
        try:
            server = next(x for x in self.servers if x['name'] == server)
        except StopIteration:
            return

        ctx.field("TaskmanagerName").value = server['name']
        ctx.field("TaskmanagerURL").value = server['url']
        ctx.field("TaskmanagerUser").value = server['user']
        ctx.field("ProjectFilter").value = server['project']
        ctx.field("RequireTags").value = server['require_tags']
        ctx.field("ExcludeTags").value = server['exclude_tags']


config = JSONConfig()


class NetRCManager(object):
    _netrc_data = None

    @property
    def netrc_data(self):
        if self._netrc_data is None:
            self._netrc_data = get_netrc()

        return self._netrc_data

    @property
    def list_control(self):
        return ctx.control("login_list")

    def update_server_list(self, current_index=None):
        if current_index is None:
            current_index = self.current_index
        if current_index is None:
            current_index = 0
        selected_item = None

        # Add clean and add columns
        list_control = self.list_control
        list_control.clearItems()
        list_control.removeColumns()
        list_control.addColumn("Id")
        list_control.addColumn("Hostname")
        list_control.addColumn("Username")

        # Add rows
        for id_, (hostname, auth) in enumerate(self.netrc_data.hosts.items()):
            selected_item_data = [
                id_,
                hostname,
                auth[0],
            ]

            item = list_control.appendItem(None, selected_item_data)

            # Set selected
            if id_ == current_index:
                selected_item = item

        list_control.resizeColumnsToContents()
        if selected_item is not None:
            list_control.ensureItemVisible(selected_item)
            list_control.setCurrentItem(selected_item)

    def update_login(self):
        hostname = ctx.control('login_hostname').text()
        username = ctx.control('login_username').text()
        password = ctx.control('login_password').text()

        # Update netrc file and wipe current data
        proceed = globals.MLAB.showWarning(("Are you want to update your login information"
                                            " for {}?\n If you are not sure what to do press"
                                            " \"Cancel\"").format(hostname),
                                           "Return to Home",
                                           ["Cancel", "Update"],
                                           0)

        if proceed:
            update_netrc_entry(hostname, username, password)
            self._netrc_data = None
            self.update_server_list()

    @property
    def current_index(self):
        selected_item = self.list_control.selectedItem()
        if not selected_item:
            return None

        return int(selected_item.text(0))

    def change_hostname(self):
        index = self.current_index or 0
        servers = list(self.netrc_data.hosts.items())

        if len(servers) == 0:
            return

        if index < len(servers):
            server = servers[index]

        ctx.control("login_hostname").setText(server[0])
        ctx.control("login_username").setText(server[1][0])
        ctx.control("login_password").setText(server[1][2])


login_info = NetRCManager()