""" This module initialises the logger. """

import os
import sys
import errno
import logging
import logging.handlers


def mkdir_p(path):
    """http://stackoverflow.com/a/600612/190597 (tzot)"""
    try:
        os.makedirs(path, exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if not (exc.errno == errno.EEXIST and os.path.isdir(path)):
                raise


class MakeTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    def __init__(self, filename, when='midnight', interval=1, backupCount=30):
        mkdir_p(os.path.dirname(filename))
        super(MakeTimedRotatingFileHandler, self).__init__(filename=filename, when=when, interval=interval, backupCount=backupCount)


class NullHandler(logging.Handler):
    def emit(self, record):
        pass


def config_logger(name,
                  format='%(levelname)-7s | %(name)-11s | %(funcName)s() | %(lineno)d | %(message)s',
                  datefmt=None,
                  stream=sys.stdout,
                  level=logging.DEBUG,
                  filename=None,
                  file_format='%(asctime)s [%(levelname)s] %(module) 9s:%(lineno)d >> %(message)s',
                  filelevel=logging.DEBUG,
                  propagate=False):
    """
    Do basic configuration for the logging system. Similar to
    logging.basicConfig but the logger ``name`` is configurable and both a file
    output and a stream output can be created. Returns a logger object.

    The default behaviour is to create a StreamHandler which writes to
    sys.stdout, set a formatter using the "%(message)s" format string, and
    add the handler to the ``name`` logger.

    A number of optional keyword arguments may be specified, which can alter
    the default behaviour.

    :param name: Logger name
    :param format: handler format string
    :param datefmt: handler date/time format specifier
    :param stream: initialize the StreamHandler using ``stream``
            (None disables the stream, default=sys.stdout)
    :param level: logger level (default=INFO).
    :param filename: create FileHandler using ``filename`` (default=None)
    :param filelevel: logger level for file logger (default=``level``)
    :param propagate: propagate message to parent (default=False)

    :returns: logging.Logger object
    """
    # Get a logger for the specified name
    logger = logging.getLogger(name)
    logger.setLevel(level)
    fmt = logging.Formatter(format, datefmt)
    file_fmt = logging.Formatter(file_format, datefmt)
    logger.propagate = propagate

    # Remove existing handlers, otherwise multiple handlers can accrue
    for handler in logger.handlers:
        logger.removeHandler(handler)

    # Add handlers. Add NullHandler if no file or stream output so that
    # modules don't emit a warning about no handler.
    if not (filename or stream):
        logger.addHandler(NullHandler())

    if filename:
        handler = MakeTimedRotatingFileHandler(filename, when='midnight', interval=1, backupCount=30)
        if filelevel is None:
            filelevel = level
        handler.setLevel(filelevel)
        handler.setFormatter(file_fmt)
        logger.addHandler(handler)

    if stream:
        handler = logging.StreamHandler(stream)
        handler.setLevel(level)
        handler.setFormatter(fmt)
        logger.addHandler(handler)

    return logger

