from abc import ABCMeta, abstractmethod, abstractproperty
import os

from . import globals, log
from .helpers import get_viewport, get_context

ctx = globals.ctx


class EditorManager(object):
    def __init__(self):
        self._log = log.config_logger('editor', filename=None)
        self.editor_classes = [PixelEditor]
        self.available_editors = {x.editor_type: x for x in self.editor_classes}
        self.active_editor = None

    def apply_editor(self, name):
        context_name, viewport_name, editor = name.split('::')
        viewport = get_viewport(context_name, viewport_name)
        context = get_context(context_name)

        # Disable annotation_selector
        viewport.annotation_selector.setEnabled(False)

        if self.deactivate_editor():
            ctx.callLater(0.1, self.available_editors[editor], [viewport, context])

    def deactivate_editor(self):
        try:

            if self.active_editor is not None:
                viewport = self.active_editor.active_viewport
                self._log.info('&&& Number of undos: {}'.format(self.active_editor.number_of_undos()))
                if self.active_editor.number_of_undos() != "(0)" and self.discard_question() == 1:
                    return False

                ctx.callLater(0.0, self.active_editor.deactivate)

                # Enable annotation_selector
                viewport.annotation_selector.setEnabled(True)
            else:
                print('%%%% ACTIVE EDITOR IS NONE!')

            self.active_editor = None
            return True
        except Exception as e:
            self._log.error('deactivate editor caused an exception: {}'.format(e))
            raise
            return True

    def save_current(self):
        current_annotation = self.active_editor.active_viewport.annotation_selector.currentText()
        annotation_resource = self.active_editor.active_data_context.annotations[current_annotation]

        edited_filename = os.path.basename(annotation_resource.original_filename)

        # Create correct basename
        if not edited_filename.startswith('edited_'):
            edited_filename = 'edited_' + edited_filename

        # Determine where to write the file
        edited_path = os.path.join(
            annotation_resource.directory,
            edited_filename
        )

        # Use editor class to export the annotation to a file
        edited_path = self.active_editor.save_file(edited_path)
        
        # Upload the file as desired
        current_annotation = self.active_editor.active_viewport.annotation_selector.currentText()
        annotation_resource = self.active_editor.active_data_context.annotations[current_annotation]
        annotation_resource.edited_filename = edited_path
        self._log.info('Syncing resource {}'.format(annotation_resource.name))

        # Disable navigation and editting
        ctx.field("EnableNavigation").value = False
        self.active_editor.disable_drawing()

        globals.errors.add_error(*globals.UPLOAD_MESSAGE)

        annotation_resource.sync(callback=self.save_finished, kwargs=None)
        
        self._log.debug("save editor {} active in viewport {}, annotation {}".format(self.active_editor.editor_type,
                                                                                     self.active_editor.active_viewport.name,
                                                                                     annotation_resource))

    def save_finished(self, upload_resource):
        # Do something checking / error handling
        globals.logger.info('EDITTED MASK UPLOAD FINISHED')

        # Re-enable navigation and editting
        ctx.field("EnableNavigation").value = True

        globals.errors.dismiss_error_by_value(*globals.UPLOAD_MESSAGE)

        # Abort on failed upload
        if not upload_resource.success:
            globals.errors.add_error('Encountered a problem uploading the data!', 'error')
            for error in upload_resource.errors:
                globals.logger.error("[UPLOAD ERROR] {}".format(error))
            self.active_editor.enable_drawing()
            return

        # Re-open saved file, reset undo and re-enable drawing
        current_annotation = self.active_editor.active_viewport.annotation_selector.currentText()
        current_annotation = self.active_editor.active_data_context.annotations[current_annotation]
        current_annotation.reader.field("open").touch()
        self.active_editor.enable_drawing()

        # Display result
        globals.errors.add_error('Upload successfully finished! (Validated {})'.format(upload_resource.validated), 'message', 1.0)

    def discard_current(self):
        self._log.info("discard editor {} {} active in viewport {}".format(self.active_editor,
                                                                        self.active_editor.editor_type,
                                                                        self.active_editor.active_viewport.name))
        self.deactivate_editor()

    def discard_question(self):
        return globals.MLAB.showQuestion(
            "Do you really want to discard the changes you made to {}?".format(self.active_editor.editor_type),
            "Your changes will be lost if you don't save them.",
            "ViewR",
            ["Discard", "Cancel"],
            1)


class BaseEditor(metaclass=ABCMeta):

    MDL_TEMPLATE = """
    Horizontal {{
        {buttons}
        expandX=True
        expandY=True
        ph =24
        //Label = "{viewport.name} : {editor_type}"
        Separator {{ direction = "Vertical" }}
        {editor_content}
        //SpacerX {{}}
    }}
    """

    SAVE_CANCEL_BUTTONS = """
    ToolButton {{
        name = "save"
        command = globals.editor.save_current
        normalOffImage = "{local}/Modules/Resources/Icons/document-save-16.png"
        normalOnImage = "{local}/Modules/Resources/Icons/document-save-16.png"
    }}
    ToolButton {{
        name = "discard"
        command = globals.editor.discard_current
        normalOffImage = "{local}/Modules/Resources/Icons/stop.png"
        normalOnImage = "{local}/Modules/Resources/Icons/stop.png"
    }}""".format(local=ctx.expandFilename("$(MLAB_EMC_ViewR)"))

    def __init__(self, viewport, context):
        self.log = log.config_logger('base_editor', level=10, filename=None)
        self._field_listener = None
        self.active_viewport = viewport
        self.active_context = context
        self.active_data_context = globals.task_manager.current_item.contexts[self.active_context.name]
        globals.editor.active_editor = self
        self.activate()

    @abstractmethod
    def clean(self):
        """
        Clean all resources of the editor
        """

    @abstractproperty
    def editor_type(self):
        pass

    @abstractproperty
    def mdl_content(self):
        pass

    def activate(self):
        self.add_border()
        self.apply_mdl()
        self.populate_dynamic_content()
        ctx.field("EnableNavigation").value = False

    @abstractmethod
    def populate_dynamic_content(self):
        pass

    def apply_mdl(self):
        mdl_string = self.MDL_TEMPLATE.format(viewport=self.active_viewport,
                                              editor_type=self.editor_type,
                                              buttons=self.SAVE_CANCEL_BUTTONS,
                                              editor_content=self.mdl_content)
        self.apply_editor_dynamic_frame(mdl_string)

    def apply_editor_dynamic_frame(self, mdl_string):
        ctx.control("{}_editor".format(self.active_context.name)).setContentString(mdl_string)

    def deactivate(self):
        try:
            self.remove_border()
            self.apply_editor_dynamic_frame("")
        finally:
            ctx.field("EnableNavigation").value = True

    def add_border(self):
        self.active_viewport.viewer.setStyleSheetFromString("border: 1px solid red;")

    def remove_border(self):
        self.active_viewport.viewer.setStyleSheetFromString("border: 1px solid black;")

    @abstractmethod
    def save_file(self, filename):
        pass


class PixelEditor(BaseEditor):
    editor_type = 'pixel'

    def __init__(self, viewport, context):
        super(PixelEditor, self).__init__(viewport, context)

    def clean(self):
        pass

    @property
    def mdl_content(self):
        mdl_content = """
            Label = "Edit from"
            Combobox {{
                name = "source_annotation_label"
                editable = False
                activatedCommand = "*py:globals.editor.active_editor.select_source_label()*"
            }}
            Label = "to"
            Combobox {{
                name = "edit_annotation_label"
                editable = False
                activatedCommand = "*py:globals.editor.active_editor.select_brush_label()*"
            }}
            Field BrushSize {{ title = "Brush size:" step = 1 tooltip = "Brush size" }}
            ToolButton Annotator.undo {{ 
                alignX = Left
                normalOffImage = "{local}/Modules/Resources/Icons/edit-undo.png"
                normalOnImage = "{local}/Modules/Resources/Icons/edit-undo.png"
                tooltip = "Undo last brush stroke"
                styleSheetString = "
                  QToolButton {{ border: 0px solid black; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
                  QToolButton::pressed {{ background-color: gray; }}
                  QToolButton::checked {{ background-color: lightgray; }}
                  "
            }}
            FieldListener BrushSize {{ command = "*py:globals.editor.active_editor.update_brush()*" }}
            FieldListener {context}_{viewport_name}.Orientation {{ command = "*py:globals.editor.active_editor.update_brush()*" }}
            SpacerX {{}}
            """.format(
            local=ctx.expandFilename("$(MLAB_EMC_ViewR)"),
            context=self.active_context.name,
            viewport_name=self.active_viewport.name
        )

        return mdl_content

    def populate_dynamic_content(self):
        annotations = self.active_context.annotations_def
        labels = [a[1]['name'] for a in sorted([a for a in annotations[self.active_viewport.annotation_selector.currentText()]["labels"].items()])]
        ctx.control("edit_annotation_label").setItems(labels)
        labels_and_all = ["all"] + labels
        ctx.control("source_annotation_label").setItems(labels_and_all)
        self.select_brush_label()
        self.select_source_label()
    
    def select_brush_label(self, label=None):
        if label is None:
            label = ctx.control("edit_annotation_label").currentText()
        annotations = self.active_context.annotations_def
        label_map = {v['name']: int(k) for k, v in annotations[self.active_viewport.annotation_selector.currentText()]['labels'].items()}
        self.editor_module.field("writeValue").value = label_map[label]
    
    def select_source_label(self, label=None):
        if label is None:
            label = ctx.control("source_annotation_label").currentText()

        if label == "all":
            ctx.field("Annotator.applyThreshold").value = False
        else:
            annotations = self.active_context.annotations_def
            label_map = {v['name']: int(k) for k, v in annotations[self.active_viewport.annotation_selector.currentText()]['labels'].items()}
            self.editor_module.field("borderMin").value = label_map[label]
            self.editor_module.field("borderMax").value = label_map[label]
            self.editor_module.field("applyThreshold").value = True
    
    def activate(self):
        super(PixelEditor, self).activate()

        self.update_brush()
        self.enable_drawing()

        current_annotation = self.active_data_context.annotations[
            self.active_viewport.annotation_selector.currentText()
        ]
        self.active_context.pixel_editor.input.connectFromUndoable(current_annotation.reader.field("output0"))
        current_annotation.bypass.field("input0").connectFromUndoable(self.active_context.pixel_editor.output)
        self.active_viewport.pixel_editor.connectFrom(self.active_context.pixel_editor.pointer_output)
        self.active_viewport.annotation_label_select("all")

    def deactivate(self):
        self.log.debug('Deactivating pixel editor')
        super(PixelEditor, self).deactivate()
        self.disable_drawing()

        current_annotation = self.active_data_context.annotations[
            self.active_viewport.annotation_selector.currentText()
        ]
        self.active_context.pixel_editor.pointer_output.disconnectAll()
        self.active_viewport.annotation_input.disconnectUndoable()
        self.active_context.pixel_editor.input.disconnectUndoable()
        current_annotation.bypass.field("input0").disconnectUndoable()
        current_annotation.bypass.field("input0").connectFrom(current_annotation.reader.field("output0"))
        self.active_viewport.annotation_select()
        self.active_context.pixel_editor.undo_all()

        self.log.debug('Deactivated pixel editor')

    def save_file(self, filename):
        if not filename.endswith('.gz'):
            filename += '.gz'
        self.log.info('Saving pixel file to: {}'.format(filename))
        self.active_context.pixel_editor.save_module.field("fileName").value = filename
        self.active_context.pixel_editor.save_module.field("save").touch()
        return filename

    @property
    def editor_module(self):
        return self.active_context.pixel_editor.editor_module

    @property
    def position_module(self):
        return self.active_context.pixel_editor.position_module

    def number_of_undos(self):
        return self.editor_module.field("numAvailUndos").value

    def disable_drawing(self):
        self.editor_module.field("drawMode").value = "Nothing"

    def enable_drawing(self):
        self.editor_module.field("drawMode").value = "Voxel"

    def update_brush(self):
        orientation = self.active_viewport.scan_orientation
        brush_size = ctx.field('BrushSize').value

        self.log.info('Scan orientation: {}'.format(orientation))
        self.log.info('Brush size: {}'.format(brush_size))
        editor_module = self.editor_module

        if orientation == 'Transversal':
            editor_module.field('voxSizeX').value = brush_size
            editor_module.field('voxSizeY').value = brush_size
            editor_module.field('voxSizeZ').value = 1
        elif orientation == 'Coronal':
            editor_module.field('voxSizeX').value = brush_size
            editor_module.field('voxSizeY').value = 1
            editor_module.field('voxSizeZ').value = brush_size
        elif orientation == 'Sagittal':
            editor_module.field('voxSizeX').value = 1
            editor_module.field('voxSizeY').value = brush_size
            editor_module.field('voxSizeZ').value = brush_size
        else:
            self.log.error("Unknown orientation: {}".format(orientation))

        self.position_module.field('drawingModelSize').value = brush_size


editor = EditorManager()
