import json
import os
import re
import sys
import time
import traceback
import urllib.parse
import zlib

# First print
print('Loading download script...')

# Add MeVisLab package path
extra_python_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                                  '..', '..', '..', 'Scripts', 'python'))
sys.path = [extra_python_path] + sys.path

# Import xnat
import xnat


class DeCompressWriter(object):
    def __init__(self, target):
        self.target = target
        self.decompressor = zlib.decompressobj(zlib.MAX_WBITS | 32)

    def write(self, data):
        self.target.write(self.decompressor.decompress(data))


def download(url, destination, auto_unpack=False):
    unpack = False

    if auto_unpack and destination.endswith('.gz'):
        destination = destination.rsplit('.', 1)[0]
        unpack = True

    if '{timestamp}' in destination:
        destination = destination.format(timestamp='')
        print('Destination updated to: {}'.format(destination))

    if os.path.exists(destination):
        return destination

    # Find the correct netrc file to use (also for windows)
    netrc_file = os.path.join('~', '_netrc' if os.name == 'nt' else '.netrc')
    netrc_file = os.path.expanduser(netrc_file)

    # Find host to connect to
    split_url = urllib.parse.urlparse(url)
    host = urllib.parse.urlunparse(split_url._replace(path='', params='', query='', fragment=''))

    with xnat.connect(host, netrc_file=netrc_file, verify=False) as session:
        path = split_url.path

        if '{timestamp}' in path:
            resource, filename = path.split('/files/')
            pattern = filename.format(timestamp=r'(_?(?P<timestamp>\d\d\d\d\-\d\d\-\d\dT\d\d:\d\d:\d\d(\.\d+)?)_?)?') + '$'

            # Query all files and sort by timestamp
            files = session.get_json('{}/files'.format(resource))
            files = [x['Name'] for x in files['ResultSet']['Result']]
            print('Found file candidates {}, pattern is {}'.format(files, pattern))
            files = {re.match(pattern, x): x for x in files}
            files = {k.group('timestamp'): v for k, v in files.items() if k is not None}
            print('Found files: {}'.format(files))

            if len(files) == 0:
                raise ValueError('Cannot find file on XNAT: {}'.format(url))

            # None is the first, timestamp come after that, so last one is highest timestamp
            latest_file = sorted(files.items())[-1][1]
            print('Select {} as being the latest file'.format(latest_file))

            # Construct the correct path again
            path = '{}/files/{}'.format(resource, latest_file)

        if unpack:
            print('Using decompressing download')
            chunk_size = 16 * 1024

            with open(destination, 'w') as output_file:
                decompressor = DeCompressWriter(output_file)
                session.download_stream(path, decompressor, verbose=False, chunk_size=chunk_size)
        else:
            print('Using simple download')
            session.download(path, destination, verbose=False)

    return destination


def main():
    if len(sys.argv) == 3:
        _, order, result = sys.argv
        print('Reading {}'.format(order))
        with open(order) as order_in:
            resource_data = json.load(order_in)
    else:
        # Create a task and download scans
        print('Reading stdin')
        resource_data = json.load(sys.stdin)

    print('Found resource data: {}'.format(resource_data))
    resource_data['download_attempted'] = True

    # Uncompress zipped nifti files as a preprocessing step (to speed up actual loading)
    try:
        resource_data['original_filename'] = download(resource_data['source_url'], resource_data['original_filename'])
        resource_data['download_success'] = os.path.isfile(resource_data['original_filename'])
        print('Download successful: {}'.format(resource_data['download_success']))
    except Exception as exception:
        resource_data['download_success'] = False
        exception_type = type(exception).__name__
        exception_message = str(exception)
        exception_traceback = traceback.format_exc()
        error = '[{}] {}:\n{}'.format(exception_type, exception_message, exception_traceback)
        print('Encountered exception: {}'.format(error))
        resource_data['download_errors'].append(error)

    resource_data['uploaded'] = None
    
    if len(sys.argv) == 3:
        for _ in range(3):
            print('Writing result data to {}'.format(result))
            with open(result, 'w') as result_out:
                json.dump(resource_data, result_out)

            for _ in range(10):
                if os.path.exists(result):
                    break
                time.sleep(0.5)
            else:
                print('Could not find result file after 10 tries')

            if os.path.exists(result):
                break
            print('Writing appears to have failed.')
        else:
            print('Could NOT write data to disk! ABORTING')
    else:
        print('Writing result data to stdout')
        print(json.dumps(resource_data))
    

if __name__ == '__main__':
    print('Start download script')
    sys.stdout.flush()
    main()
