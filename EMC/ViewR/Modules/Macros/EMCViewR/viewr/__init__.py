import os
from pathlib import Path
import platform
import re
import sys
import traceback

from mevis import MLAB

from . import globals, log


def get_viewr_version(ctx, logger):
    # If installed there is a prefs file in de MeVisLab/IDE/bin directory that
    # contains the version number of the package
    prefs_file = os.path.abspath(
        ctx.expandFilename('$(MLAB_EMC_ViewR)/../../MeVisLab/IDE/bin/viewr.prefs')
    )

    version = None

    if os.path.isfile(prefs_file):
        with open(prefs_file) as fin:
            data = fin.read()

        match = re.search(r'^ *Version = "(.*)"$', data, re.MULTILINE)
        if match:
            version = match.group(1)
        else:
            logger.warning('Could find version in {}'.format(prefs_file))
    else:
        logger.warning('Could not load {}'.format(prefs_file))

    # If not installed we run from source and there should be the ViewR.mlinstall
    # in which the version number of stored
    if not version:
        prefs_file = os.path.abspath(ctx.expandFilename('$(MLAB_EMC_ViewR)/Configuration/Installers/ViewR/ViewR.mlinstall'))

        if os.path.isfile(prefs_file):
            with open(prefs_file) as fin:
                data = fin.read()

            match = re.search(r'^\$VERSION (.*)$', data, re.MULTILINE)
            if match:
                version = match.group(1)

    # If we cannot find the version, just go for unknown
    if not version:
        version = 'unknown'
    else:
        version = version.strip()

    return version


def init(ctx):
    print('Populating globals module...')
    # Make it visible for non-mevislab editors
    globals.MLAB = MLAB
    globals.ctx = ctx

    # Making exceptions visible using and error pop-up
    def exception_window(exc_type, value, trace):
        text = 'Encountered {} exception:\n{}'.format(exc_type.__name__, value)
        detail = 'Complete traceback:\n{}'.format('\n'.join(traceback.format_tb(trace)))
        title = 'Exception encountered!'

        MLAB.showCritical(
            text,
            detail,
            title,
            ['Ok'],
            0
        )

        sys.__excepthook__(exc_type, value, trace)

    sys.excepthook = exception_window

    # Set up logger
    globals.logger = log.config_logger(
        'ViewR',
        level=20,
        filename=os.path.expanduser(os.path.join('~', '.viewr', 'logs', 'log.txt'))
    )

    # Find the correct netrc file to use (also for windows)
    netrc_file = Path.home() / ('_netrc' if os.name == 'nt' else '.netrc')
    globals.netrc_file = netrc_file
    globals.netrc_backup = netrc_file.with_name(netrc_file.name + '_backup')

    # Mevis directories
    globals.local = ctx.expandFilename("$(MLAB_EMC_ViewR)")
    globals.layout_dir = ctx.expandFilename("$(LOCAL)/layouts")

    # Upload/Download related globals
    globals.DOWNLOAD_SCRIPT = ctx.expandFilename('$(LOCAL)/viewr/download.py')
    globals.UPLOAD_SCRIPT = ctx.expandFilename('$(LOCAL)/viewr/upload.py')
    if os.name == 'nt':
        globals.PYTHON_BIN = ctx.expandFilename('$(MLAB_MeVis_ThirdParty)\\Python\\MeVisPython.exe').replace('/','\\')
    elif platform.system() == 'Darwin':
        globals.PYTHON_BIN = ctx.expandFilename('$(MLAB_MeVis_ThirdParty)/bin/MeVisPython')
    else:
        globals.PYTHON_BIN = ctx.expandFilename('$(MLAB_MeVis_ThirdParty)/Python/Release/bin/python')

    # Initialize data managers
    from .resourcemanager import ResourceManager
    globals.resource_manager = ResourceManager()

    from .taskmanager import TaskManager
    globals.task_manager = TaskManager()

    from .templatemanager import TemplateManager
    globals.template_manager = TemplateManager()

    from .editor import EditorManager
    globals.editor = EditorManager()

    from .errors import Errors
    globals.errors = Errors()

    # Collect all controls
    from .controls.lineedit import LineEdit
    from .controls.numberedit import NumberEdit
    from .controls.checkbox import CheckBox
    from .controls.radiobuttongroup import RadioButtonGroup
    from .controls.conversationwidget import ConversationWidget
    from .controls.textbox import TextBox
    from .controls.combobox import ComboBox
    from .controls.tabswidget import TabsWidget
    from .controls.listingwidget import ListingWidget
    from .controls.markeredit import MarkerEdit, MarkerView
    from .controls.csoedit import CSOEdit
    from .controls.jsondisplay import JSONDisplay
    from .controls.boxwidget import BoxWidget
    from .controls.calculatedscore import CalculatedScore
    from .controls.validationcontrol import ValidationControl
    from .controls.landmarks import Landmarks

    globals.control_dict = {
        'LineEdit': LineEdit,
        'NumberEdit': NumberEdit,
        'CheckBox': CheckBox,
        'RadioButtonGroup': RadioButtonGroup,
        'ConversationWidget': ConversationWidget,
        'TextBox': TextBox,
        'ComboBox': ComboBox,
        'TabsWidget': TabsWidget,
        'ListingWidget': ListingWidget,
        'MarkerEdit': MarkerEdit,
        'MarkerView': MarkerView,
        'CSOEdit': CSOEdit,
        'JSONDisplay': JSONDisplay,
        'BoxWidget': BoxWidget,
        'CalculatedScore': CalculatedScore,
        'ValidationControl': ValidationControl,
        'Landmarks': Landmarks,
    }

    # Get the version of the ViewR
    globals.version = get_viewr_version(ctx, globals.logger)
    globals.user_agent_prefix = 'ViewR/{}'.format(globals.version)
