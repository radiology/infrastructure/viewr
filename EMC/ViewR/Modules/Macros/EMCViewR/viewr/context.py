import datetime
import os
import json
import weakref

from . import globals, log, callbacks
from .viewport import ViewPort
from .controls.boxwidget import BoxWidget
from .controls.csoedit import CSOEditorModule, CSOEdit
from .controls.listingwidget import ListingWidget
from .controls.markeredit import MarkerEditorModule, MarkerEdit
from .controls.pixeledit import PixelEditorModule
from .controls.tabswidget import TabsWidget
from .controls.validationcontrol import ValidationControl
from .helpers import get_username

ctx = globals.ctx


class Context(object):
    RATER_FIELD = '__raters__'

    DEFAULT_MARKER_STYLE = [
        {'color': (1.0, 1.0, 0.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (1.0, 0.0, 1.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.0, 1.0, 1.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.0, 1.0, 0.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (1.0, 0.5, 0.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.0, 0.67, 1.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (1.0, 0.0, 0.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.5, 1.0, 0.5), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (1.0, 0.0, 0.5), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.5, 1.0, 0.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.5, 0.0, 1.0), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4},
        {'color': (0.0, 1.0, 0.5), 'line_width': 2, 'label': '', 'marker_type': u'Square', 'marker_size': 4}
    ]

    def __init__(self,
                 name,
                 parent,
                 viewports_def,
                 scans_def,
                 annotations_def,
                 qa_fields_def,
                 mdl_layout,
                 initial_position=None):
        if not name.isalnum():
            raise ValueError('Name must be alphanumeric!')

        self.name = name
        self.template = weakref.ref(parent)
        self.group_name = 'context: {}'.format(self.name)
        self._log = log.config_logger('context_{}'.format(self.name), level=20, filename=None)

        # Get al template definitions
        self.viewports_def = viewports_def
        self.scans_def = scans_def
        self.annotations_def = annotations_def
        self.qa_fields_def = qa_fields_def
        self.mdl_layout = mdl_layout

        # Initialize all module lists.
        self.viewport_modules = dict()
        self.qa_fields = dict()
        self.toplevel_fields = dict()
        self.rater_history = list()

        # Initial position management
        self.initial_position_set = False

        self._log.info('++++++ Initial position: {}'.format(initial_position))
        if (isinstance(initial_position, (list, tuple))
                and len(initial_position) == 3
                and all(isinstance(x, float) for x in initial_position)):
            self.initial_position = initial_position
        else:
            self.initial_position = None

        self._log.info('++++++ self.initial position: {}'.format(initial_position))

        # Set placeholder for listeners
        self.viewer_scroll_listener = None

        # Create CSO and Marker editing modules
        self.cso_so_group = None
        self.marker_so_group = None
        self.marker_style_module = None
        self.active_marker_style_module = None

        self.cso_editor = None
        self.marker_editor = None
        self.add_csos()
        self.add_markers()

        # Create pixel editor module
        self.pixel_editor = PixelEditorModule('{}_PixelEditor'.format(self.name), self.group_name)

        # Dynamically created macro to hold qa_fields.
        self.qa_macro = None

    def __str__(self):
        return '<Context {}>'.format(self.name)

    def clean(self):
        # Clean qa_fields, qa_macro and viewports
        self.remove_all_qa_fields()
        self.remove_qa_macro()
        self.remove_all_viewports()

        # Remove marker and cso related items
        self.marker_editor.clean()
        self.cso_editor.clean()

        # Remove pixel editor
        self.pixel_editor.clean()

        if self.marker_so_group:
            self.marker_so_group.remove()
            self.marker_so_group = None

        if self.marker_style_module:
            self.marker_style_module.remove()
            self.marker_style_module = None

        if self.active_marker_style_module:
            self.active_marker_style_module.remove()
            self.active_marker_style_module = None

        if self.cso_so_group:
            self.cso_so_group.remove()
            self.cso_so_group = None

    def get_initial_position(self):
        # Use own initial position
        if self.initial_position:
            return self.initial_position

        # Check other contexts
        for context in self.template().contexts.values():
            if context.initial_position:
                return context.initial_position

        # If nothing is set, use middle of the image
        self._log.info('++++++ Using DEFAULT initial position')
        return (0.5, 0.5, 0.5)

    def generate_mdl(self):
        # Create GUI MDL based on template
        viewports_mdl = self.create_viewport_grid()
        qa_fields_mdl = self.create_qa_field_grid()
        editor_mdl = 'DynamicFrame Editor {{  expandY = False name = "{}_editor" }} '.format(self.name)

        # Combine all MDL into final string
        content_string = self.mdl_layout.format(viewports=viewports_mdl, editor=editor_mdl, qa_fields=qa_fields_mdl)

        return content_string

    def create_viewport(self, viewport):
        name = viewport['name']

        # Create the module
        module = ctx.addModule("ViewPort")
        module.name = '{}_{}'.format(self.name, name)
        module.addToGroup(self.group_name)

        # Create viewport control
        viewport = ViewPort(self, module, **viewport)

        # Save viewport control
        self.viewport_modules[name] = viewport
        return viewport

    def create_viewport_grid(self):
        if self.remove_all_viewports():
            viewports = []
            for viewport in self.viewports_def:
                viewports += [self.create_viewport(viewport)]
            return "\n".join([vp.mdl for vp in viewports])
        else:
            msg = "Because not all viewports could be removed, the grid is not created."
            self._log.error(msg)
            globals.errors.add_error(msg, "error")

    def perform_validation_actions(self):
        for field in self.qa_fields.values():
            if not isinstance(field, ValidationControl):
                continue

            if not field.value:
                continue

            if field.action == 'validate':
                continue

            # Make sure the action exists in callbacks
            if not hasattr(callbacks, field.action):
                self._log.warning('Cannot find action {}, skipping control {}'.format(
                    field.action,
                    field.name,
                ))
                continue

            # Perform the callback using the args as dict of keyword arguments
            action_function = getattr(callbacks, field.action)
            action_function(**field.args)

    def remove_viewport(self, name):
        try:
            if self.viewport_modules[name].popout_window is not None:
                try:
                    self.viewport_modules[name].popout_window.close()
                except ValueError:
                    pass

            self.viewport_modules[name].module.remove()
            self.viewport_modules.pop(name)
        except KeyError:
            self._log.warning("ViewPort named '{}' does not exist and therefore cannot not be removed.".format(name))

    def remove_all_viewports(self):
        # Create a list to avoid changing dictionary during iteration
        viewport_keys = list(self.viewport_modules.keys())
        for name in viewport_keys:
            self.remove_viewport(name)
        if len(self.viewport_modules) > 0:
            self._log.error("Something went wrong, not all viewport modules were removed.")
            return False
        return True

    def remove_all_qa_fields(self):
        self.remove_qa_macro()

        # Clean the QA field controls
        for field in self.qa_fields.values():
            field.clean()

        self.qa_fields = dict()
        self.toplevel_fields = dict()
        return True

    def add_qa_fields(self, fields, toplevel=False):
        if self.qa_macro is None:
            # Create a macro from string to dynamically create Fields for the qa_fields. The macro is accessible on self.qa_macro.
            for name, field in fields.items():
                try:
                    class_ = globals.control_dict[field['control']]
                except (KeyError, AttributeError):
                    self._log.error("Could not create qa field for: [{}] {}".format(name, field))
                    self._log.error("Wrong control definition in the task description: {} not implemented!".format(field['control']))
                    continue
                if name in self.qa_fields:
                    message = "QA Field name is already in use, should be unique: name = {}".format(name)
                    self._log.error(message)
                    globals.errors.add_error(message)
                    continue
                self.qa_fields[name] = class_(self, name, field)
                self._log.debug("QA Field {} ({}, {}) created.".format(name, field['type'], field['control']))
                
            if toplevel:
                self.create_qa_macro(self.qa_fields)
                for fieldname in fields.keys():
                    self.toplevel_fields[fieldname] = self.qa_fields[fieldname]
        else:
            self._log.error("There is already a QA Macro. Make sure this is removed before you try to add fields. (This should be done via the task load pipeline).")

    def remove_qa_macro(self):
        if self.qa_macro is not None:
            self.qa_macro.remove()
            self.qa_macro = None
        return True

    def create_qa_macro(self, fields):
        if self.qa_macro is None:
            header = "Interface { Parameters {\n" \
                     "Field __file__ { type = String }\n"
            footer = '}}'
            parameters = ''.join(x.qa_macro_field() for x in fields.values() if x.type is not None)
            self._log.info('Creating QA macro')
            self.qa_macro = ctx.addMacroModuleFromString(header + parameters + footer)
            self.qa_macro.addToGroup(self.group_name)

            self._log.info('Finished creating QA macro')
            self.qa_macro.name = "{}_QaFields".format(self.name)

        else:
            self._log.error("There is already a QA Macro. Make sure this is removed before you try to add fields. (This should be done via the task load pipeline).")

    def create_qa_field_grid(self):
        if self.remove_all_qa_fields():
            self.add_qa_fields(self.qa_fields_def, toplevel=True)
            mdl = [self.qa_fields[qa_field].mdl for qa_field in self.qa_fields_def.keys()]
            mdl.append("""FieldListener {context_name}_QaFields.__file__ {{
                command =   "*py:get_context('{context_name}').load_qa_json()*"
            }}""".format(context_name=self.name))
            mdl = "\n".join(mdl)
            return mdl

    def add_csos(self):
        # Create CSO modules and group
        self.cso_editor = CSOEditorModule('{}_AllCsos'.format(self.name), self.group_name)
        self.cso_so_group = ctx.addModule('SoGroup')
        self.cso_so_group.name = '{}_CsoSoGroup'.format(self.name)
        self.cso_so_group.addToGroup(self.group_name)
        self.cso_so_group.field("child").connectFrom(self.cso_editor.output)

    def add_markers(self):
        # Create marker modules and group
        self.marker_style_module = MarkerEditorModule.add_style_module('{}MarkerStyle'.format(self.name.capitalize()))
        self.marker_style_module.addToGroup(self.group_name)
        self.active_marker_style_module = MarkerEditorModule.add_style_module('{}ActiveMarkerStyle'.format(self.name.capitalize()))
        self.active_marker_style_module.addToGroup(self.group_name)

        self.marker_editor = MarkerEditorModule('{}_AllMarkers'.format(self.name), self, all_markers=True)

        # Set default styles
        for index, item in enumerate(self.DEFAULT_MARKER_STYLE):
            self.marker_editor.set_style(index, item)

        self.marker_so_group = ctx.addModule('SoGroup')
        self.marker_so_group.name = '{}_MarkerSoGroup'.format(self.name)
        self.marker_so_group.addToGroup(self.group_name)
        self.marker_so_group.field("child").connectFrom(self.marker_editor.output)

    def setup_viewport_linking(self):
        for viewport in self.viewport_modules.values():
            ctx.addFieldListener(viewport.module.field("ViewMouse.cursorPresent"),
                                 "py:viewers_switch_fieldlistener('{context_name}', '{viewport}')".format(
                                     context_name=self.name,
                                     viewport=viewport.name,
                                 ),
                                 False)

    def populate_viewport_comboboxes(self):
        self._log.info("[Context {}] Populate viewport comboboxes!".format(self.name))
        scans = list(self.scans_def.keys())
        if self.annotations_def is None:
            annotations = None
        else:
            annotations = list(self.annotations_def.keys())

        # Fill the comboboxes
        for viewport in self.viewport_modules.values():
            viewport.scan_selector.setItems(scans)
            if annotations is None:
                viewport.annotation_selector.setItems([''])
                viewport.annotation_selector.setVisible(False)
            else:
                viewport.annotation_selector.setItems(annotations)
                viewport.annotation_selector.setVisible(True)
                viewport.annotation_alpha_selector.setVisible(True)

        ctx.callLater(0.0, self.make_links, [])

    def make_links(self):
        self._log.info("++++++ Making links!")
        for viewport in self.viewports_def:
            viewport_target = self.viewport_modules[viewport['name']]
            self._log.debug("Linking scan {} to {}".format(viewport['name'], viewport['scan']))
            viewport_target.scan_select(viewport['scan'])
            if viewport['annotation'] is not None:
                self._log.debug("Linking annotation {} to {}".format(viewport['name'], viewport['annotation']))
                viewport_target.annotation_select(viewport['annotation'])
            else:
                # Hide the edit button when there is no annotation to edit
                viewport_target.edit_button.setVisible(False)

    def viewers_switch_fieldlistener(self, viewport_name):
        """ Switch FieldListener to the "active" Viewer. """
        # When {ViewPort.name}.ViewMouse.cursorPresent gives a signal and is True do something.
        viewport_module = self.viewport_modules[viewport_name]

        if not viewport_module.module.field("ViewMouse.cursorPresent").value:
            return

        if not viewport_module.image_data_available:
            return

        self._log.info("++++++ Switching listeners for position to {}!".format(viewport_module.name))

        ctx.field("ActiveViewPort").value = viewport_module.name
        ctx.field("ActiveContext").value = self.name

        if not self.template().link_localizers:
            viewports = self.viewport_modules.values()
        else:
            viewports = []
            for context in self.template().contexts.values():
                viewports.extend(context.viewport_modules.values())

        for vp in viewports:
            vp.module.field("LocalizerPosition").disconnect()

            # Make sure both the source and target viewport are linked and that we are not linking to self
            if vp is not viewport_module and vp.linked and viewport_module.linked:
                vp.module.field("LocalizerPosition").connectFrom(viewport_module.module.field("LocalizerPosition"))

    def set_position(self, position, initial=False):
        if initial and self.initial_position_set:
            self._log.info('++++++ context initial position already set')
            return

        if not self.template().link_localizers:
            self._log.info('++++++ set only this context')
            self.initial_position_set = True
            viewports = self.viewport_modules.values()
        else:
            self._log.info('++++++ set all linked contexts')
            viewports = []
            for context in self.template().contexts.values():
                context.initial_position_set = True
                viewports.extend(context.viewport_modules.values())

        for vp in viewports:
            if vp.linked:
                vp.set_position(position)

    def toggle_localizer(self, show_localizer):
        if not self.template().link_localizers:
            viewports = self.viewport_modules.values()
        else:
            viewports = []
            for context in self.template().contexts.values():
                viewports.extend(context.viewport_modules.values())

        for vp in viewports:
            vp.module.field("ShowLocalizer").value = show_localizer

            # Turn of updating when no localizer is present
            position = vp.module.module("position")

            # Connect / disconnect localizer to slice
            if show_localizer:
                position.field("updateOnPress").value = True
                position.field("updateOnMotion").value = True
            else:
                position.field("updateOnPress").value = False
                position.field("updateOnMotion").value = False

    def check_qc_ready(self):
        if all(x.valid for x in self.toplevel_fields.values()):
            # Set the status of the current task to "done"
            return True
        else:
            return False

    def load_qa_json(self):
        field = self.qa_macro.field("__file__")
        self._log.info('Load QA JSON with: {}'.format(field))
        data = None  # Use non to indicate data isn't loaded
        try:
            # Unpack the data
            if hasattr(field, 'value'):
                field = field.value

            self._log.info('Attempting to load JSON from [{field_type}] {field}'.format(
                field_type=type(field).__name__,
                field=field
            ))
            if isinstance(field, str):
                if os.path.isfile(field):
                    with open(field) as fin:
                        data = json.load(fin)
                else:
                    self._log.error("Could not find QA data file {}".format(data))
            else:
                self._log.warning('QA path is of wrong type!')
        except Exception as exception:
            # If anything goes wrong, use no data
            self._log.error("Something went wrong with loading the json data: [{e_type}] {e}".format(
                e_type=type(exception).__name__,
                e=exception
            ))

        self._log.info('Applying QA data: {}'.format(data))

        try:
            # Save the rater history into the network, make sure to copy the list
            history = data.get(self.RATER_FIELD, None) if isinstance(data, dict) else None
            if history is not None:
                self.rater_history = list(history)
            else:
                self.rater_history = []

            ctx.field("UpdateRaterHistory").touch()

            print('GOT DATA: {}'.format(data))
            # Only apply data if a file actually got loaded (e.g. data is not None)
            if data is not None:
                # Set all QA values
                for key, value in self.toplevel_fields.items():
                    # Use get so that a None is used to unset fields if there is not data found
                    self._log.info('Setting {} to {}  [v {}  k {}]'.format(value, data.get(key), key, value))
                    value.value = data.get(key)
        finally:
            # Re-enable navigation
            ctx.field('EnableNavigation').value = True
            ctx.field('EnableEditing').value = True

            # Set all CSOs and markers
            self.update_all_markers()
            self.update_all_csos()

    def save_qa_json(self):
        self._log.info("Calling save_qa_json for {}".format(self.name))
        result = {k: v.value for k, v in self.toplevel_fields.items()}

        # Make sure there is a raters field
        result[self.RATER_FIELD] = self.rater_history

        # Append the rater (username + timestamp) to list of raters
        result[self.RATER_FIELD].append({
            'username': get_username(),
            'timestamp': str(datetime.datetime.now().isoformat())
        })

        ctx.field("UpdateRaterHistory").touch()

        self._log.info("QA results: {}".format(result))
        current_task = globals.task_manager.current_item

        fields_resource = current_task.contexts[self.name].fields_resource
        if fields_resource.edited_filename is None:
            edited_filename = os.path.basename(fields_resource.original_filename)

            # Create correct basename
            if not edited_filename.startswith('edited_'):
                edited_filename = 'edited_' + edited_filename

            if '{timestamp}' in edited_filename:
                edited_filename = edited_filename.format(timestamp='timestamped')

            edited_filename = os.path.join(os.path.dirname(fields_resource.original_filename), edited_filename)
            fields_resource.edited_filename = edited_filename

        with open(fields_resource.edited_filename, 'w') as fout:
            json.dump(result, fout)

        return fields_resource

    def find_csos(self, data=None, ignore=None):
        if data is None:
            data = {k: v.value for k, v in self.toplevel_fields.items()}

        csos = []
        for key, value in data.items():
            # If this value is not in the control, ignore it
            if key not in self.qa_fields or value is None:
                continue

            control = self.qa_fields[key]

            if isinstance(control, CSOEdit):
                if value is not None and value != ignore and value['csos']:
                    for item in value['csos']:
                        csos.append(((control.name, value['group']), item))
            elif isinstance(control, (TabsWidget, BoxWidget)):
                csos.extend(self.find_csos(value, ignore=ignore))
            elif isinstance(control, ListingWidget):
                for entry in value:
                    entry = dict(entry)  # Make a copy
                    entry.pop('Id')  # Remove Id field that is a non-qa-field
                    csos.extend(self.find_csos(entry, ignore=ignore))
        return csos

    def update_all_csos(self, ignore=None):
        self.cso_editor.remove_csos()
        for group, cso in self.find_csos(ignore=ignore):
            self.cso_editor.add_cso(cso, group)

    def find_markers(self, data=None):
        if data is None:
            data = {k: v.value for k, v in self.toplevel_fields.items()}

        markers = []
        for key, value in data.items():
            # If this value is not in the control, ignore it
            if key not in self.qa_fields or value is None:
                continue

            control = self.qa_fields[key]
            if isinstance(control, MarkerEdit):
                if value is not None:
                    if isinstance(control.marker_type, int):
                        marker_type = control.marker_type
                    elif isinstance(control.marker_type, str):
                        marker_type = control.marker_type
                        marker_type_control = self.qa_fields[marker_type]
                        if marker_type in data:
                            marker_type = data.get(control.marker_type)

                        if marker_type in marker_type_control.options:
                            marker_type = marker_type_control.options.index(marker_type)

                        if not isinstance(marker_type, int):
                            marker_type = None
                    else:
                        marker_type = None

                    if marker_type is not None:
                        for marker in value:
                            marker['type'] = marker_type

                    markers.extend(value)

            elif isinstance(control, (TabsWidget, BoxWidget)):
                markers.extend(self.find_markers(value))
            elif isinstance(control, ListingWidget):
                for entry in value:
                    entry = dict(entry)  # Make a copy
                    entry.pop('Id')  # Remove Id field that is a non-qa-field
                    markers.extend(self.find_markers(entry))
        return markers

    def update_all_markers(self):
        self.marker_editor.markers = self.find_markers()
