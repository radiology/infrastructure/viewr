# This module is populate via the init() function to hold all globals
# Add placeholders so we know what to expect

# Make mevislab specials visible for non-mevislab editors
MLAB = None
ctx = None

# Logger
logger = None

# Mevis directories
local = None
layout_dir = None

# ViewR globals
SELECTED_TAGS = None
MAIN_WINDOW = None
TITLE_STYLE = "font-size: 20px; font-weight: bold;"

# Upload/Download related globals
UPLOAD_MESSAGE = "Upload in progress...", "message"
DOWNLOAD_SCRIPT = None
UPLOAD_SCRIPT = None
PYTHON_BIN = None
POLL_INTERVAL = 0.5

# Initialize data managers
resource_manager = None
task_manager = None
template_manager = None
editor = None
errors = None

control_dict = None

# ViewR version
version = None
user_agent_prefix = None

# .netrc file location
netrc_file = None
netrc_backup = None