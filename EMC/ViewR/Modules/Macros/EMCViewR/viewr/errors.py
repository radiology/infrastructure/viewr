from . import log
from .globals import ctx, MLAB


class Errors:
    """ Show errors."""
    html_template = """
    <html>
    <head>
        <style type="text/css">
            body {{
                margin: 0px;
                padding: 0px;
                font-family: "arial,sans-serif";
                font-size: 120%;
            }}
            .error {{
                border: 3px solid #f00;
                background: #ffc0cb;
                margin: 0 0 0 0;
                padding: 4px;
                height: 25px;
            }}
            
            .warning {{
                border: 3px solid #ff0;
                background: #fff5c2;
                margin: 0 0 0 0;
                padding: 4px;
                height: 25px;
            }}

            .message {{
                border: 3px solid #0f0;
                background: #c0ffcb;
                margin: 0 0 0 0;
                padding: 4px;
                height: 25px;
            }}
            .dismiss {{
                display:inline-block;
                color:#444;
                border:1px solid #CCC;
                background:#DDD;
                box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
                cursor: pointer;
                vertical-align:middle;
                max-width: 100px;
                padding: 2px;
                text-align: center;
                font-size: 50%;
            }}
        </style>
    </head>
    <body>
    {errors}
    </body></html>
    """
    error_list = []

    def __init__(self):
        """ Initialisation of the Error class. Some parameters are defined here. """
        self._log = log.config_logger('errors', filename=None)
        self.levels = ['message', 'warning', 'error']

    def show_error_panel(self):
        ctx.control("error_panel").setVisible(True)

    def hide_error_panel(self):
        ctx.control("error_panel").setVisible(False)

    def details_popup(self, index):
        # Seems the call in the html field converts the argument to a float, convert back to int
        index = int(index)

        try:
            MLAB.showInformation(self.error_list[index][2], "Error details", ["Okay"], 0)
        except IndexError:
            MLAB.showWarning("Error details not found", "Error details", ["Okay"], 0)

    def update_error_panel(self):
        if not len(self.error_list) == 0:
            errors = []

            for index, (message, level, details) in enumerate(self.error_list):
                if details:
                    details = "<div class='dismiss' onClick=\"ctx.call('globals.errors.details_popup',[{}])\">DETAILS</div>".format(index)
                else:
                    details = ''

                errors.append(
                    """        <div class='{level}'>
                <div class='dismiss' onClick=\"ctx.call('globals.errors.dismiss_error',[{index}])\">
                    DISMISS
                </div>
                {details}
                {message}
            </div>""".format(index=index, level=level, details=details, message=message)
                )

            self.html_string = self.html_template.format(errors='\n'.join(errors))

            ctx.control("error_panel").setHtmlFromString(self.html_string)

            self.show_error_panel()
            ctx.control("error_panel").setMaxHeight(len(self.error_list) * 39)
            ctx.control("error_panel").setMinHeight(len(self.error_list) * 39)
        else:
            self.hide_error_panel()

    def add_error(self, error_description, error_level="warning", lifetime=None, details=None):
        if error_level in self.levels:
            list_item = (error_description, error_level, details)
            self.error_list.append(list_item)
            ctx.field("UpdateErrors").touch()

            if lifetime is not None:
                ctx.callLater(lifetime, self.dismiss_error_by_value, list_item)
        else:
            self._log.error("Incorrect loglevel ({}). Choose one of {}.".format(error_level, ", ".join(self.levels)))

    def dismiss_error(self, index):
        self.error_list.pop(int(index))
        ctx.field("UpdateErrors").touch()

    def dismiss_error_by_value(self, value, error_level, details=None):
        try:
            self.error_list.remove((value, error_level, details))
        except ValueError:
            return
        ctx.field("UpdateErrors").touch()

    def dismiss_all_errors(self):
        self.error_list = []
        ctx.field("UpdateErrors").touch()
