import weakref
from PythonQt.QtGui import QColor

from . import log
from .import globals
from .task import Task

ctx = globals.ctx


class ViewPort(object):
    INTERPOLATION_MAP = {
        'nearest': 'FILTER_NEAREST',
        'linear': 'FILTER_LINEAR'
    }

    def __init__(self,
                 context,
                 module,
                 type,
                 name,
                 scan,
                 annotation,
                 x,
                 y,
                 linked,
                 orientation=None,
                 label=None,
                 projection=None,
                 projection_slab=None,
                 slice_highlight_interval=None,
                 interpolation='nearest'):

        if not name.isalnum():
            raise ValueError('Name must be alphanumeric!')

        self.context = weakref.ref(context)
        self.module = module
        self._log = log.config_logger(name, level=10, filename=None)
        self.type = type
        self.name = name
        self.scan = scan
        self.annotation = annotation
        self.x = x
        self.y = y
        self._linked = linked
        self.initial_label = label
        self.popout_window = None

        self.position.field("button1").value = "PRESSED"
        self.position.field("shift").value = "IGNORED"
        self.position.field("editingOn").value = True
        self.position.field("drawEditingRect").value = False

        # Connect markers/cso from context
        self.markers.connectFrom(context.marker_so_group.field("self"))
        self.csos.connectFrom(context.cso_so_group.field("self"))

        # Make sure the initial orientation is valid
        if orientation not in ["Transversal", "Coronal", "Sagittal"]:
            orientation = "Transversal"
        self.initial_orientation = orientation

        if projection not in ["none", "min", "max"]:
            projection = "none"
        self.initial_mip = projection

        if not isinstance(projection_slab, int):
            projection_slab = 5
        self.initial_mip_slab = projection_slab

        self.initial_position_listener = None
        self.initial_position_set = False

        if isinstance(slice_highlight_interval, int):
            self.module.field('SliceHighlightInterval').value = slice_highlight_interval

        interpolation = self.INTERPOLATION_MAP.get(interpolation)
        if interpolation:
            self.interpolation_field.value = interpolation

    @property
    def context_name(self):
        return self.context().name

    @property
    def full_name(self):
        return '{}_{}'.format(self.context_name, self.name)

    @property
    def current_data_context(self):
        return globals.task_manager.current_item.contexts[self.context_name]

    @property
    def current_annotation_template(self):
        return self.context().annotations_def[self.annotation_selector.currentText()]

    @property
    def current_annotation_resource(self):
        return self.current_data_context.annotations[self.annotation_selector.currentText()]

    @property
    def image_data_available(self):
        return self.module.field("ImageDataAvailable").value

    @property
    def position(self):
        return self.module.module("position")

    def set_slice(self, slice):
        view = self.module.module("background")
        view.field("startSlice").value = slice

    @property
    def initial_position(self):
        return self.context().get_initial_position()

    def set_initial_position(self):
        self._log.info('++++++ set_initial_position called')
        # Make sure initial position is set only once
        if self.initial_position_set:
            self._log.info('++++++ set_initial_position already set')
            return

        # Make sure an image is loaded
        if not self.module.field('Info.dataValid').value:
            self._log.info('++++++ no image loaded yet')
            return

        # If other image in context is loaded, use that position
        if self.linked and self.context().initial_position_set:
            self._log.info('++++++ trying to find other viewport')

            # Get all viewports
            template = globals.task_manager.current_item.template
            if not template.link_localizers:
                viewports = self.context().viewport_modules.values()
            else:
                viewports = []
                for context in template.contexts.values():
                    viewports.extend(context.viewport_modules.values())

            for vp in viewports:
                if vp.linked and vp is not self:
                    if not vp.initial_position_set:
                        continue

                    position = vp.module.field('LocalizerPosition').value
                    self._log.info('++++++ using position of other viewport: {}'.format(position))
                    self.module.field('LocalizerPosition').value = position
                    ctx.callLater(0.0, self.module.field('LocalizerPosition').touch, [])
                    self.initial_position_listener.blockSignals(True)
                    self.initial_position_set = True
                    break
            return

        size = (self.module.field('Info.sizeX').value,
                self.module.field('Info.sizeY').value,
                self.module.field('Info.sizeZ').value)

        # Figure out and set position
        target_position = [s * p for s, p in zip(size, self.initial_position)]
        target_position = self.voxel_to_world(target_position)
        self._log.info('++++++ setting initial position to {}'.format(target_position))
        self.context().set_position(target_position, initial=True)
        ctx.callLater(0.0, self.module.field('LocalizerPosition').touch, [])
        self.initial_position_listener.blockSignals(True)
        self.initial_position_set = True

    def set_position(self, value):
        position = self.module.field('LocalizerPosition')
        position.value = value

    def position_update_required(self):
        value = self.module.field('ToWorld.worldPos').value
        self.set_position(value)

    def world_to_voxel(self, x, y=None, z=None):
        if y is None and z is None:
            x, y, z = x
        convert = self.module.module("WorldVoxelConvert")
        convert.field("keepConstant").value = "World"
        convert.field("worldX").value = x
        convert.field("worldY").value = y
        convert.field("worldZ").value = z

        result = (
            convert.field("voxelX").value,
            convert.field("voxelY").value,
            convert.field("voxelZ").value,
        )

        return result

    def voxel_to_world(self, x, y=None, z=None):
        if y is None and z is None:
            x, y, z = x
        convert = self.module.module("WorldVoxelConvert")
        convert.field("keepConstant").value = "Voxel"
        convert.field("voxelX").value = x
        convert.field("voxelY").value = y
        convert.field("voxelZ").value = z

        result = (
            convert.field("worldX").value,
            convert.field("worldY").value,
            convert.field("worldZ").value,
        )

        return result

    @property
    def projection_mode_field(self):
        return self.module.field("projectionMode")

    @property
    def projection_slab_field(self):
        return self.module.field("projectionSlabSize")

    @property
    def interpolation_field(self):
        return self.module.field("filterMode")

    @property
    def min_scan_value_field(self):
        return self.module.field("ScanMin")

    @property
    def max_scan_value_field(self):
        return self.module.field("ScanMax")

    def update_mip_annot(self):
        view = self.module.module("view")
        view_annotation = self.module.module("annotation")
        nslices = self.projection_slab_field.value

        blend_mode = view.field("blendMode").value

        if blend_mode == 'BLEND_REPLACE':
            view_annotation.field("annotationUserTopRight").value = ''
        elif blend_mode == 'BLEND_MINIMUM':
            view_annotation.field("annotationUserTopRight").value = 'Projection: Minimum\n({} slices)'.format(nslices)
        elif blend_mode == 'BLEND_MAXIMUM':
            view_annotation.field("annotationUserTopRight").value = 'Projection: Maximum\n({} slices)'.format(nslices)
        else:
            view_annotation.field("annotationUserTopRight").value = 'Unknown projection!'

    def toggle_mip(self, mode=None):
        self._log.info('Setting MIP to: {}'.format(mode))
        view = self.module.module("view")
        view.field("slab").disconnectAll()

        if mode is None:
            view.field("slab").value = 1
            view.field("blendMode").value = "BLEND_REPLACE"
            self.projection_mode_field.value = 'none'
        elif mode == 'min':
            view.field("slab").connectFrom(self.projection_slab_field)
            view.field("blendMode").value = "BLEND_MINIMUM"
            self.projection_mode_field.value = 'min'
        elif mode == 'max':
            view.field("slab").connectFrom(self.projection_slab_field)
            view.field("blendMode").value = "BLEND_MAXIMUM"
            self.projection_mode_field.value = 'max'

    def toggle_localizer(self):
        show_localizer = self.module.field("ShowLocalizer").value
        context = self.context()
        if context:
            context.toggle_localizer(show_localizer)

    def set_loading(self, loading):
        view_annotation = self.module.module("annotation")

        if not view_annotation:
            return

        if loading:
            view_annotation.field("annotationUserTopLeft").value = 'Task is loading...\n($(input00)): $(input01)'
        else:
            view_annotation.field("annotationUserTopLeft").value = '($(input00)): $(input01)'

    def slab_editor(self):
        self.module.showModalDialog("slabSizeDialog")

    def set_slab_size(self, value):
        if not isinstance(value, int):
            self._log.error('Slab size should be an int!')
        else:
            self.projection_slab_field.value = value

    def create_popout(self):
        if self.popout_window is not None:
            # Try to see if the window is open and alive, if so
            # there is nothing to do, otherwise continue
            try:
                self.popout_window.show()
                return
            except ValueError:
                pass

        current_scan = self.scan_selector.currentText()
        current_annotation = self.annotation_selector.currentText()
        current_label = self.annotation_label_selector.currentText()

        content_string = f"""
Window "WindowPopOut_{self.full_name}" {{
    style = Panel.default
    name  = "window_{self.full_name}_popout"
    title = "{self.full_name}"
    expandX = True
    expandY = True

    Grid {{ expandX=True expandY=True 
        {self.generate_mdl(content=True, executes=False, listeners=False)}
    }}
}}"""

        # self._log.info('Creating window from: {}'.format(content_string))
        parent_window = globals.MAIN_WINDOW.control("main_window")
        window = ctx.showWindowFromString(content_string, parent_window)
        self.popout_window = window

        task = globals.task_manager.current_item

        if not isinstance(task, Task):
            return

        scans = list(task.template.json['scans'].keys())
        if task.template.json['annotations'] is None:
            annotations = None
        else:
            annotations = list(task.template.json['annotations'].keys())

        # Get controls
        annotation_selector = window.control(f"annotation_select_{self.full_name}")
        annotation_alpha_selector = window.control(f"annotation_alpha_select_{self.full_name}")
        annotation_label_selector = window.control(f"annotation_label_select_{self.full_name}")
        scan_selector = window.control(f"scan_select_{self.full_name}")

        # Hide pop-out button
        popout_button = window.control(f"popout_{self.full_name}")
        edit_button = window.control(f"edit_{self.full_name}")

        popout_button.setVisible(False)  # Popout should not have popout button
        edit_button.setVisible(False)  # Popout should not have popout button

        # Fill the comboboxes
        scan_selector.setItems(scans)
        scan_selector.setCurrentText(current_scan)

        if annotations is None:
            annotation_selector.setItems([''])
            annotation_selector.setVisible(False)
        else:
            annotation_selector.setItems(annotations)
            annotation_selector.setCurrentText(current_annotation)
            annotation_selector.setVisible(True)
            annotation_label_selector.setVisible(True)
            annotation_alpha_selector.setVisible(True)
            self.populate_label_combobox(current_annotation, only_popout=True)
            annotation_label_selector.setCurrentText(current_label)

    @property
    def show_overlay_field(self):
        return self.module.field("OverlayToggle")

    @property
    def show_marker_field(self):
        return self.module.field("MarkerToggle")

    @property
    def show_csos_field(self):
        return self.module.field("CSOToggle")

    @property
    def lut_module(self):
        return self.module.module("LUT")

    @property
    def scan_input(self):
        return self.module.field('scan')

    @property
    def annotation_input(self):
        return self.module.field('annotation')

    @property
    def pixel_editor(self):
        return self.module.field('pixeleditor')

    @property
    def markers(self):
        return self.module.field('markers')

    @property
    def csos(self):
        return self.module.field('csos')

    @property
    def scan_selector(self):
        return globals.MAIN_WINDOW.control("scan_select_" + self.full_name)

    @property
    def scan_orienter(self):
        return globals.MAIN_WINDOW.control("scan_orient_" + self.full_name)

    @property
    def linked(self):
        return self._linked

    @linked.setter
    def linked(self, linked):
        self._log.debug("Set {}.linked = {}".format(self.full_name, linked))
        self._linked = linked

        # Set position back to other linked viewports (if any)
        if linked:
            for vp in self.context().viewport_modules.values():
                if vp.linked and vp is not self:
                    self.module.field('LocalizerPosition').value = vp.module.field('LocalizerPosition').value
                    break

    def add_initial_position_listener(self):
        self._log.info('++++++ Adding initial position field listener to {}'.format(self.name))
        self.initial_position_listener = ctx.addFieldListener(
            self.module.field("Info.dataValid"),
            "py:viewport_set_initial_position('{self.context_name}', '{self.name}')".format(
               self=self,
            ),
            False
        )

    @property
    def annotation_selector(self):
        return globals.MAIN_WINDOW.control("annotation_select_" + self.full_name)

    @property
    def annotation_alpha_selector(self):
        return globals.MAIN_WINDOW.control("annotation_alpha_select_" + self.full_name)

    @property
    def popout_annotation_alpha_selector(self):
        if not self.popout_window:
            return None

        return self.popout_window.control("annotation_alpha_select_" + self.full_name)

    @property
    def annotation_label_selector(self):
        return globals.MAIN_WINDOW.control("annotation_label_select_" + self.full_name)

    @property
    def popout_annotation_label_selector(self):
        if not self.popout_window:
            return None

        return self.popout_window.control("annotation_label_select_" + self.full_name)

    @property
    def edit_button(self):
        return globals.MAIN_WINDOW.control("edit_" + self.full_name)

    @property
    def edit_button_menu(self):
        return globals.MAIN_WINDOW.control("edit_menu_" + self.full_name)

    @property
    def viewer(self):
        return globals.MAIN_WINDOW.control("viewer_" + self.full_name)

    def scan_select(self, scan=None):
        if scan is None:
            scan = self.scan_selector.currentText()
        else:
            # Set the combobox to the wanted scan
            self.scan_selector.setCurrentText(scan)

        self._log.debug("Switching scan to {}".format(scan))
        self.scan_input.connectFrom(globals.task_manager.current_item.contexts[self.context_name].scans[scan].output)
        self.min_scan_value_field.connectFrom(
            globals.task_manager.current_item.contexts[self.context_name].scans[scan].bypass.field('resultLow')
        )
        self.max_scan_value_field.connectFrom(
            globals.task_manager.current_item.contexts[self.context_name].scans[scan].bypass.field('resultHigh')
        )
        self.module.field("LocalizerPosition").touch()

    @property
    def scan_orientation(self):
        return self.scan_orienter.currentText()

    def scan_orient(self, orientation=None):
        # Remember world position value
        world_position_field = self.position.field('worldPosition')
        world_position = world_position_field.value

        if orientation is None:
            orientation = self.scan_orientation
        else:
            # Set the combobox to the wanted scan
            self.scan_orienter.setCurrentText(orientation)

        self._log.debug("Switching orientation to {}".format(orientation))

        self.module.module("OrthoSwapFlipScan").field("view").value = orientation
        self.module.module("OrthoSwapFlipAnnotation").field("view").value = orientation

        ctx.callLater(0.0, self.set_position, [world_position])

        # Reset to proper world position field
        world_position_field.value = world_position

    def annotation_select(self, annotation=None):
        if annotation is None:
            annotation = self.annotation_selector.currentText()
        else:
            # Set the combobox to the wanted annotation.
            self.annotation_selector.setCurrentText(annotation)
        self._log.debug("Switching annotation to {}".format(annotation))

        context = self.context()
        if context is None:
            return

        if self.current_annotation_resource.type == 'pixel':
            self.annotation_input.connectFrom(self.current_data_context.annotations[annotation].output)
        ctx.callLater(0.0, self.update_edit_button)
        ctx.callLater(0.0, self.populate_label_combobox, [annotation])
        ctx.callLater(0.1, self.load_labels)

    def populate_label_combobox(self,
                                annotation: str,
                                only_popout: bool = False):
        annotations = self.context().annotations_def
        if self.current_annotation_resource.type == "pixel":
            # Label list sorted by voxel value
            labels = [a[1]['name'] for a in sorted([a for a in annotations[annotation]["labels"].items()])]
            labels = ['all'] + labels
            if not only_popout:
                self.annotation_label_selector.setItems(labels)
                self.annotation_label_selector.setVisible(True)
                self.annotation_alpha_selector.setVisible(True)

            if self.popout_window:
                self.popout_annotation_label_selector.setItems(labels)
                self.popout_annotation_label_selector.setVisible(True)
                self.popout_annotation_alpha_selector.setVisible(True)
        elif self.current_annotation_resource.type == "marker" or self.current_annotation_resource.type == "contour":
            labels = annotations[annotation]["labels"]
            labels = [labels[label]['name'] for label in sorted(labels.keys())]
            if not only_popout:
                self.annotation_label_selector.setItems(labels)
                self.annotation_label_selector.setVisible(True)
                self.annotation_alpha_selector.setVisible(False)

            if self.popout_window:
                self.popout_annotation_label_selector.setItems(labels)
                self.popout_annotation_label_selector.setVisible(True)
                self.popout_annotation_alpha_selector.setVisible(True)

    def annotation_label_select(self, label=None):
        if self.current_annotation_resource.type == "pixel":
            self._annotation_label_select_pixel(label)
        elif self.current_annotation_resource.type == "marker":
            self._annotation_label_select_marker(label)
        elif self.current_annotation_resource.type == "contour":
            self._annotation_label_select_contour(label)

    def _annotation_label_select_pixel(self, label=None):
        self._log.debug("{}._annotation_label_select_pixel(label={})".format(self.__class__.__name__, label))
        if label == "all" or self.annotation_label_selector.currentText() == "all":
            if label is None:
                label = "all"
            self.annotation_label_selector.setCurrentText(label)
            self._log.debug("{}.annotation_label_select(label={}): calling load_labels".format(self.__class__.__name__, label))
            self._log.debug("initial_label = {}".format(self.initial_label))
            self.load_labels()
            return
        annotations = self.context().annotations_def
        label_values = [int(a) for a in annotations[self.annotation_selector.currentText()]["labels"].keys()]
        label_values = sorted(label_values)
        if label is None:
            label = self.annotation_label_selector.currentText()
            self._log.debug("taking label from selector {}".format(label))
        else:
            # Set the combobox to the wanted annotation.
            self.annotation_label_selector.setCurrentText(label)
            self._log.debug("setting selector to {}".format(label))
        self._log.debug("Switching label to {}".format(label))
        # Label name to voxel value mapping
        labelmapping = {v['name']: int(k) for k, v in annotations[self.annotation_selector.currentText()]['labels'].items()}
        value = labelmapping[label]
        alphas = "[ "
        for l in label_values:
            if l == value:
                alphas += str(l) + " 1"
            else:
                alphas += str(l) + " 0"
            if l == label_values[-1]:
                alphas += ", " + str(l+1) + " 0 ]"
            else:
                alphas += ", "
        self.lut_module.field("alphaPoints").value = alphas
        # Update the viewer
        self.annotation_input.touch()

    def _annotation_label_select_marker(self, label=None):
        self._log.debug("{}._annotation_label_select_marker(label={})".format(self.__class__.__name__, label))

    def _annotation_label_select_contour(self, label=None):
        self._log.debug("{}._annotation_label_select_contour(label={})".format(self.__class__.__name__, label))

    def load_labels(self):
        # make a dict with: {'key': "label type", 'item': "load_label_handler"}
        available_label_handlers = {'pixel': self.load_pixel_labels, 'marker': self.load_marker_labels, 'contour': self.load_contour_labels}

        annotation = self.current_annotation_template
        # Call correct label handler based on label type.
        self._log.debug("Loading annotation [{}] of type {}".format(self.annotation_selector.currentText(), annotation['type']))
        available_label_handlers[annotation["type"]](annotation)
        # Update the viewer
        self.annotation_input.touch()

    def load_pixel_labels(self, annotation):
        label_values = [int(a) for a in annotation["labels"].keys()]
        label_values = sorted(label_values)
        alphas = "[ "
        colours = "[ "
        for l in label_values:
            colourvec = [float(a)/255 for a in annotation["labels"][str(l)]["colour"]]
            if l == label_values[-1]:
                alphas += str(l) + " " + annotation["labels"][str(l)]["opacity"] + " , " + str(l+1) + " 0 ]"
                colours += str(l) + " " + str(colourvec[0]) + " " + str(colourvec[1]) + " " + str(colourvec[2]) + " , " + str(l+1) + " 0 0 0 ]"
            else:
                alphas += str(l) + " " + annotation["labels"][str(l)]["opacity"] + ", "
                colours += str(l) + " " + str(colourvec[0]) + " " + str(colourvec[1]) + " " + str(colourvec[2]) + ", "
        self.lut_module.field("alphaPoints").value = alphas
        self.lut_module.field("colorPoints").value = colours
        if self.initial_label is not None:
            self._log.debug("There is an initial label set, load it ({})".format(self.initial_label))
            self._log.debug("label string type {}".format(type(self.initial_label)))
            self.annotation_label_selector.setCurrentText(self.initial_label)
            self.annotation_label_select(self.initial_label)
            self.initial_label = None

    def load_marker_labels(self, annotation):
        for key, label in annotation['labels'].items():
            ctx.field("StylePalette.color{}".format(key)).setColorValue(QColor(*label['colour']))
            if len(label['description']) > 0:
                ctx.field("StylePalette.name{}".format(key)).value = label['description']
            else:
                ctx.field("StylePalette.name{}".format(key)).value = label['name']

    def load_contour_labels(self, annotation):
        context = self.context()

        cso_list = context.cso_editor.cso_list

        # Check if there are groups defined in the cso manager, if so, skip action
        if len(cso_list.getGroups()) > 0:
            self._log.error("The cso manager contains groups, skipping ...")
        else:
            # Loop over all labels
            # First create a list sorted on label keys for consistency.
            labels = [label for key, label in sorted(annotation['labels'].items())]
            for label in labels:
                cso_group = cso_list.addGroup(label['name'])
                cso_group.setDescription(label['description'])
                cso_group.setPathPointColor(label['colour'])
                cso_group.usePathPointColor = True
                cso_group.setSeedPointColor(label['colour'])
                cso_group.useSeedPointColor = True

    def update_edit_button(self):
        self.edit_button_menu.clearItems()
        if globals.task_manager.current_item.template.json['annotations'] is None:
            annotation_info = {'editable': False}
        else:
            annotation_info = globals.task_manager.current_item.template.json['annotations'][self.annotation_selector.currentText()]
        if annotation_info['editable']:
            self.edit_button.setVisible(True)
            self.edit_button.setEnabled(True)
            self.edit_button.setToolTip("Edit options")
            for annotation_type in (x for x in annotation_info['editors'] if x in globals.editor.available_editors):
                self.edit_button_menu.appendItem(annotation_type, globals.editor.apply_editor, "{}::{}::{}".format(
                    self.context_name, self.name, annotation_type
                ))
        else:
            self.edit_button.setVisible(False)
            self.edit_button.setToolTip("Not allowed to edit this type")

    def update_ruler(self):
        ruler_module = self.module.module("ruler")
        if ruler_module and not ruler_module.field("editingOn").value:
            ruler_module.field("deleteAll").touch()

    @property
    def mdl(self):
        return self.generate_mdl()

    def generate_mdl(self,
                     content: bool = True,
                     executes: bool = True,
                     listeners: bool = True):
        mdl = []
        alpha = "{}.{}".format(self.module.name, self.module.module("SoOverlay").field("alphaFactor").fullName())
        local = ctx.expandFilename("$(MLAB_EMC_ViewR)")
        if content:
            mdl.append(f"""
      Vertical {{
        expandX = True
        expandY = True
        x = {self.x}
        y = {self.y}
        Horizontal {{
          expandX = True
          expandY = False
          ToolButton {self.module.name}.Linked {{
              name                  = "link_viewer_{self.full_name}"
              alignX                = Left
              normalOffImage        = "{local}/Modules/Resources/Icons/link-broken.png"
              normalOnImage         = "{local}/Modules/Resources/Icons/link.png"
              scaleIconSetToMinSize = False
              mw                    = 18
              mh                    = 18
              tooltip               = "Link the viewer to other viewers"
              command               = "*py:get_viewport('{self.context_name}', '{self.name}').linked = globals.MAIN_WINDOW.control("link_viewer_{self.full_name}").widget().checked*"
              styleSheetString      = "
                  QToolButton {{ border: 0px solid black; background-color: lightcoral; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
                  QToolButton::pressed {{ background-color: gray; }}
                  QToolButton::checked {{ background-color: lightgreen; }}
          "
          }}
          Execute = "*py:globals.MAIN_WINDOW.control("link_viewer_{self.full_name}").widget().checkable = True*"
          Execute = "*py:globals.MAIN_WINDOW.control("link_viewer_{self.full_name}").widget().checked = {self.linked}*"
          
          ToolButton {self.module.name}.ShowLocalizer {{
              image = "{local}/Modules/Resources/Icons/position-16.png"
              scaleIconSetToMinSize = False
              command = "*py:get_viewport('{self.context_name}', '{self.name}').toggle_localizer() *"
              mw = 18
              mh = 18
              tooltip = "Show localizer."
              styleSheetString = "
                  QToolButton {{ border: 0px solid black; background-color: lightcoral; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
                  QToolButton::pressed {{ background-color: gray; }}
                  QToolButton::checked {{ background-color: lightgreen; }}"
          }}
          
          ToolButton {self.module.name}.ShowRuler {{
              image = "{local}/Modules/Resources/Icons/marker-16.png"
              scaleIconSetToMinSize = False
              mw = 18
              mh = 18
              tooltip = "Show ruler."
              styleSheetString = "
                  QToolButton {{ border: 0px solid black; background-color: lightcoral; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
                  QToolButton::pressed {{ background-color: gray; }}
                  QToolButton::checked {{ background-color: lightgreen; }}"
          }}

          ToolButton {{
              image = "{local}/Modules/Resources/Icons/threshold-16.png"
              scaleIconSetToMinSize = False
              mw = 18
              mh = 18
              tooltip = "Change intensity projection over number of slices (no, minimum, or maximum intensity projection)."
              styleSheetString = "
                  QToolButton {{ border: 0px solid black; background-color: lightgray; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
              "

              popupDelay = 0.0
              popupMenu projection {{
                  MenuItem = "No Intensity Projection"  {{
                      name = "none"
                      command = "*py:get_viewport('{self.context_name}', '{self.name}').toggle_mip()*"
                  }}
                  MenuItem = "Minimum Intensity Projection" {{
                      name = "minimum"
                      command = "*py:get_viewport('{self.context_name}', '{self.name}').toggle_mip('min')*"
                  }}
                  MenuItem = "Maximum Intensity Projection" {{
                      name = "maximum"
                      command = "*py:get_viewport('{self.context_name}', '{self.name}').toggle_mip('max')*"
                  }}
                  MenuItem = "Edit number of slices" {{
                      name = "edit_slices"
                      command = "*py:get_viewport('{self.context_name}', '{self.name}').slab_editor()*"
                  }}
              }}
          }}

          ToolButton {{
              image = "{local}/Modules/Resources/Icons/window-new.png"
              name                  = "popout_{self.full_name}"
              scaleIconSetToMinSize = False
              command = "*py:get_viewport('{self.context_name}', '{self.name}').create_popout()*"
              mw = 18
              mh = 18
              tooltip = "Create a seperate window for this view port."
              styleSheetString = "
                  QToolButton {{ border: 0px solid black; background-color: lightgray; }}
                  QToolButton::hover {{ border-radius: 2; border: 1px solid darkgray; }}
                  QToolButton::pressed {{ background-color: gray; }}
                  QToolButton::checked {{ background-color: lightgreen; }}"
          }}

          ToolButton {{
            enabled = False
            dependsOn = EnableNavigation
            tooltip = "Not allowed to edit this type"
            name = "edit_{self.full_name}"
            normalOffImage = "{local}/Modules/Resources/Icons/applications-graphics.png"
            normalOnImage = "{local}/Modules/Resources/Icons/applications-graphics.png"
            disabledOffImage = "{local}/Modules/Resources/Icons/forbidden.png"
            disabledOnImage = "{local}/Modules/Resources/Icons/forbidden.png"
            popupMenu {{
              name = "edit_menu_{self.full_name}"
            }}
            popupDelay=0.0
          }}

          ComboBox {self.module.name}.SelectedScan {{
            name = "scan_select_{self.full_name}"
            editable = False
            activatedCommand = "*py:get_viewport('{self.context_name}', '{self.name}').scan_select()*"
          }}
          ComboBox {self.module.name}.SelectedOrientation {{
            name = "scan_orient_{self.full_name}"
            editable = False
            activatedCommand = "*py:get_viewport('{self.context_name}', '{self.name}').scan_orient()*"
          }}
          ComboBox {self.module.name}.SelectedAnnotation {{
            dependsOn = EnableNavigation
            name = "annotation_select_{self.full_name}"
            editable = False
            activatedCommand = "*py:get_viewport('{self.context_name}', '{self.name}').annotation_select()*"
          }}
          ComboBox {self.module.name}.SelectedLabel {{
            dependsOn = EnableNavigation
            name = "annotation_label_select_{self.full_name}"
            visible = False
            editable = False
            activatedCommand = "*py:get_viewport('{self.context_name}', '{self.name}').annotation_label_select()*"
          }}
          Slider {alpha} {{
            name = "annotation_alpha_select_{self.full_name}"
            expandX = True
            visible = False
          }}
          SpacerX {{}}
        }}
        Viewer {self.module.name}.viewer.self {{
          name = "viewer_{self.full_name}"
          expandX = True
          expandY = True
          //pw = 333
          //ph = 333
          type = SoRenderArea
          styleSheetString = "border: 1px solid black;"
        }}
      }}
      """)

        if executes:
            mdl.append(f"""
      Execute = "*py:get_viewport('{self.context_name}', '{self.name}').scan_orient("{self.initial_orientation}")*"
      Execute = "*py:get_viewport('{self.context_name}', '{self.name}').set_slab_size({self.initial_mip_slab})*"
      Execute = "*py:get_viewport('{self.context_name}', '{self.name}').toggle_mip("{self.initial_mip}")*"
      Execute = "*py:get_viewport('{self.context_name}', '{self.name}').update_mip_annot()*"
      Execute = "*py:get_viewport('{self.context_name}', '{self.name}').add_initial_position_listener()*"
    """)

        if listeners:
            mdl.append(f"""
      FieldListener {self.module.name}.ToWorld.voxelZ {{ command = "*py:viewport_position_update_required('{self.context_name}', '{self.name}')*" }}
      FieldListener {self.module.name}.view.blendMode {{ command = "*py:get_viewport('{self.context_name}', '{self.name}').update_mip_annot()*" }}
      FieldListener {self.module.name}.view.slab {{ command = "*py:get_viewport('{self.context_name}', '{self.name}').update_mip_annot()*" }}
      FieldListener {self.module.name}.ShowRuler {{ command = "*py:get_viewport('{self.context_name}', '{self.name}').update_ruler()*" }}
      """)

        return '\n'.join(mdl)
