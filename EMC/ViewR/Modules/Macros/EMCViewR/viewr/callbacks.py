from . import log

import requests


class Callbacks(object):
    def __init__(self):
        self._log = log.config_logger('callbacks', filename=None)

    def update_state(self, state):
        task_manager.resolve_task()  # This removes the task and sets the next safe item


callbacks = Callbacks()
