import netrc
import os
import re

from . import globals
ctx = globals.ctx


def get_context(context):
    current_task = globals.task_manager.current_item

    if isinstance(current_task, str) or current_task is None:
        globals.logger.warning('Cannot get context, no Task is currently loaded!')
        return

    return current_task.template.contexts.get(context)


def get_control(context, name):
    context_obj = get_context(context)

    if context_obj is None:
        globals.logger.warning('Cannot get field {c}/{n}, context {c} not found!'.format(c=context, n=name))
        return

    return context_obj.qa_fields.get(name)


def get_viewport(context, name):
    context_obj = get_context(context)

    if context_obj is None:
        return

    return context_obj.viewport_modules.get(name)


# Next few functions are safe targets for field listeners to hook into, that check if target object is still there
def viewers_switch_fieldlistener(context_name, viewport):
    context = get_context(context_name)
    # Make sure there is a context and call actual function
    if context:
        context.viewers_switch_fieldlistener(viewport)


def viewport_set_initial_position(context, name):
    viewport = get_viewport(context, name)

    # Make sure there is a viewport and call actual function
    if viewport:
        viewport.set_initial_position()


def viewport_position_update_required(context, name):
    viewport = get_viewport(context, name)

    # Make sure there is a viewport and call actual function
    if viewport:
        viewport.position_update_required()

# End of field listener targets

def ensure_valid_name(name):
    return ''.join(x for x in name if x.isalnum() or x == '_')


def get_username():
    return ctx.field('TaskmanagerUser').value


def set_template_name(template_name):
    ctx.control("template_name").setTitle(template_name)
    ctx.control("template_name").setStyleSheetFromString(globals.TITLE_STYLE)


def load_tags():
    globals.logger.info('Reloading tags')
    globals.MAIN_WINDOW.control("tag_list").clearItems()
    tags_names = globals.task_manager.get_tags()
    if tags_names is None:
        ctx.field("TaskmanagerStatus").value = "Offline"
        globals.MAIN_WINDOW.control("taskmanager_status").setStyleSheetFromString('''
            color: red;
            font-weight: bold;
            background-color: transparent;
        ''')
        tags_names = []
    else:
        ctx.field("TaskmanagerStatus").value = "Online"
        globals.MAIN_WINDOW.control("taskmanager_status").setStyleSheetFromString('''
            color: green;
            font-weight: bold;
            background-color: transparent;
        ''')

    globals.MAIN_WINDOW.control("tag_list").setVisible(True)
    globals.MAIN_WINDOW.control("tag_list").setItems([name for name, uri in tags_names])


def starting_template():
    try:
        content = """
      Vertical {
          Horizontal {
            alignX = Center
            alignY = Center
            Box {
              title = "Taskmanager selection"
              expandX = True
              expandY = True
              ComboBox SelectedServer {
                editable = No
                name = "server_selector"
              }
              FieldListener SelectedServer {
                command = "*py:config.select_server()*"
              }
              FieldListener SelectedServer {
                command = "*py:load_tags()*"
              }
              Box {
                title = "Settings"
                expandX = True
                layout = Grid
                
                Label {
                  title = "Taskmanager name:"
                  x = 0
                  y = 0
                }
                Field TaskmanagerName {
                  edit = False
                  minLength = 32
                  title = ""
                  x = 1
                  y = 0
                }
                Label {
                  title = "Taskmanager URL:"
                  x = 0
                  y = 1
                }
                Field TaskmanagerURL {
                  minLength = 32
                  edit = False
                  title = ""
                  x = 1
                  y = 1
                }
                Label {
                  title = "Taskmanager status:"
                  x = 0
                  y = 2
                }
                Field TaskmanagerStatus {
                  name = "taskmanager_status"
                  title = ""
                  edit = False
                  x = 1
                  y = 2
                }
                Label {
                  title = "Taskmanager username:"
                  x = 0
                  y = 3
                }
                Field TaskmanagerUser {
                  minLength = 32
                  edit = False
                  title = ""
                  x = 1
                  y = 3
                }
                Label {
                  title = "Project filter:"
                  x = 0
                  y = 4
                }
                Field ProjectFilter {
                  minLength = 32
                  edit = False
                  title = ""
                  x = 1
                  y = 4
                }
              }
            }
            Box {
              title = "Tag selection"
              expandX = True
              expandY = True
              Label = "<b>Filter on the following tags</b>:<br><font size=small>Select none to get all, multiple selections possible</font>"
              ListBox {
                visible = False
                expandY = True
                name = "tag_list"
                visibleRows = 6
                selectionMode = Multi
              }
              Button {
                name = "set_tag_filter"
                title = "Reset"
                command = reset_tags_filter
                image   = $(MLAB_EMC_ViewR)/Modules/Resources/Icons/update-16.png
              }
            }
          }
          Horizontal {
            SpacerX {}
            Button {
              name = "start_button"
              title = "Start first task"
              command = "*py:start_with_tasks()*"
              dependsOn = "* TaskmanagerStatus == "Online" *"
            }
            Button {
              name = "taskbrowser_button"
              title = "Task browser"
              command = open_task_browser
              dependsOn = "* TaskmanagerStatus == "Online" *"
            }
            SpacerX {}
          }
          Execute = "update_servers"
      }
      """
        ctx.field("ShowTaskNavigation").value = False
        globals.MAIN_WINDOW.control("task_layout").setContentString(content)
        globals.MAIN_WINDOW.control("start_button").setStyleSheetFromString("QPushButton {font-size: 16px; font-weight: bold;}")

    finally:
        # Re-enable navigation
        ctx.field('EnableNavigation').value = True
        ctx.field('EnableEditing').value = True


def end_template():
    try:
        content = """
      Horizontal {
        alignX = Center
        alignY = Center
        SpacerX {}
        Vertical {
          expandX = False
          expandY = False
          Label { name = "greeter" title = "You have reached the end of the task list."  }
          Button {
            name = "reload_button"
            title = "Go home"
            command = "*py:go_home(force=True)*"
          }
        }
        SpacerX {}
      }
      """
        ctx.field("ShowTaskNavigation").value = False
        globals.MAIN_WINDOW.control("task_layout").setContentString(content)
        globals.MAIN_WINDOW.control("reload_button").setStyleSheetFromString("QPushButton {font-size: 20px; font-weight: bold;}")
    finally:
        # Re-enable navigation
        ctx.field('EnableNavigation').value = True
        ctx.field('EnableEditing').value = True
    

def update_netrc_entry(hostname, username, password):
    # Add line to netrc!
    new_entry = "machine {hostname}\n        login {username}\n        password {password}\n\n".format(hostname=hostname,
                                                                                                       username=username,
                                                                                                       password=password)

    if os.path.isfile(globals.netrc_file):
        with open(globals.netrc_file, 'r') as netrc_fh_in:
            netrc_data = netrc_fh_in.read()

        match = re.search(r'^\s*machine\s+{hostname}\s*$'.format(hostname=hostname),
                          netrc_data,
                          flags=re.MULTILINE)

        if match is None:
            new_entry = netrc_data.rstrip('\n') + "\n\n" + new_entry
        else:
            new_entry = "\n\n" + new_entry
            hostname = re.escape(hostname)
            new_entry = re.sub(r'(\s+machine\s+{hostname}\s*\n.*?(?=machine|$))'.format(hostname=hostname),
                               new_entry,
                               netrc_data,
                               flags=re.DOTALL)

    with open(globals.netrc_file, 'w') as netrc_fh:
        netrc_fh.write(new_entry)

    # Set file permission to 600 (read/write user only)
    os.chmod(globals.netrc_file, 0o600)
    globals.logger.info('Updated .netrc with auth')


def get_netrc():
    if not globals.netrc_file.is_file():
        # Create a user read/write only file to start with
        globals.netrc_file.touch(mode=0o600)

    try:
        return netrc.netrc(globals.netrc_file)
    except netrc.NetrcParseError:
        # Show a prompt about moving the netrc out of the way?
        action = globals.MLAB.showQuestion(
            "Netrc file corrupted!",
            "The login storage (netrc) seems to be corrupted, do you want to reset it to an"
            " empty state? This is needed to continue using the ViewR but you will loose all"
            " saved login information. Optionally we can move the older netrc out of the way "
            " but keep back backup for you.",
            "Netrc file corrupted!",
            ["Yes, without backup", "Yes, with backup", "No"],
            1)

        # If needed, wipe the old file
        if action == 0:
            globals.netrc_file.unlink()
        # If needed, move for a backup
        elif action == 1:
            globals.netrc_file.replace(globals.netrc_backup)
        # Just throw the error
        elif action == 2:
            raise

        globals.netrc_file.touch(mode=0o600)

        return netrc.netrc(globals.netrc_file)

