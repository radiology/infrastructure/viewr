import yaml

from . import globals, log
from .context import Context
from .helpers import set_template_name, get_viewport
from taskclient.exceptions import TaskManagerResponseError

ctx = globals.ctx


class TaskTemplate:
    def __init__(self, template):
        self.json = yaml.safe_load(template)

        if 'contexts' in self.json:
            self.layout = self.json['mdl_layout']
        else:
            # Default layout for one context
            self.layout = 'Horizontal {{ expandX=True expandY=True {default} }}'

        self.name = self.json['template_name']

        # Link localizers between contexts
        self.link_localizers = self.json.get('link_localizers', False)
        self.sop = self.json['sop']
        self._log = log.config_logger('tasktemplate', level=20, filename=None)
        self.contexts = {}

    @property
    def presets(self):
        return self.json.get('presets', [])

    def apply_preset(self, index):
        try:
            preset = self.presets[index]
        except IndexError:
            self._log.warning('Cannot find preset {}'.format(index))
            return

        self._log.info('Apply preset {}'.format(preset))

        for context_name, settings in preset.items():
            if context_name.startswith('_'):
                continue

            for viewport_name, viewport_settings in settings.items():
                viewport = get_viewport(context_name, viewport_name)

                if viewport is None:
                    self._log.warning('Cannot find viewport {} in context {}'.format(
                        viewport_name, context_name)
                    )

                scan = viewport_settings.get('scan')
                if scan:
                    self._log.info('Setting {} scan to {}'.format(viewport, scan))
                    viewport.scan_select(scan)

                orientation = viewport_settings.get('orientation')
                if orientation:
                    self._log.info('Setting {} orientation to {}'.format(viewport, orientation))
                    viewport.scan_orient(orientation)

                annotation = viewport_settings.get('annotation')
                if annotation:
                    self._log.info('Setting {} annotation to {}'.format(viewport, annotation))
                    viewport.annotation_select(annotation)

                # This cannot use get with a None default as None is a valid value
                # that can be given. So as long as the fields is given we use it.
                if 'projection' in viewport_settings:
                    mip = viewport_settings['projection']
                    self._log.info('Setting {} mip to {}'.format(viewport, mip))
                    viewport.toggle_mip(mip)

                slab = viewport_settings.get('projection_slab')
                if slab:
                    self._log.info('Setting {} slab to {}'.format(viewport, slab))
                    viewport.set_slab_size(slab)

    def create_contexts(self):
        self.contexts = {}
        for context_name, context_data in self.get_contexts_data().items():
            self.contexts[context_name] = Context(context_name,
                                                  parent=self,
                                                  viewports_def=context_data['viewports'],
                                                  scans_def=context_data['scans'],
                                                  annotations_def=context_data['annotations'],
                                                  qa_fields_def=context_data['qa_fields'],
                                                  mdl_layout=context_data['mdl_layout'],
                                                  initial_position=context_data.get('initial_position', None))

    def remove_contexts(self):
        self._log.info('Cleaning all contexts...')
        while self.contexts:
            context_name, context = self.contexts.popitem()
            self._log.info('Cleaning context: {}'.format(context_name))
            context.clean()

    def apply(self):
        self._log.info("Applying template!")
        contexts_mdl = {}
        for context in self.contexts.values():
            contexts_mdl[context.name] = context.generate_mdl()

        set_template_name(self.name)

        # Combine MDL of all contexts into final string
        content_string = self.layout.format(**contexts_mdl)

        # Apply MDL to update GUI
        ctx.callLater(0.0, self.update_layout, [content_string])

        # Update loading annotation
        # FIXME!  update_loading_message()

    def update_layout(self, content):
        self._log.info("Update task layout!")
        #for number, line in enumerate(content.splitlines()):
        #    logger.info('{:04d} | {}'.format(number, line))
        ctx.control("task_layout").setContentString(content)
        ctx.callLater(0.0, self.update_presets_frame, [])

        # Populate the ViewPort ComboBoxes and setup viewport linking
        for context in self.contexts.values():
            ctx.callLater(0.0, context.populate_viewport_comboboxes, [])
            ctx.callLater(0.0, context.setup_viewport_linking, [])

        # After template is loaded, we can safely trigger qa field loading
        ctx.callLater(0.0, globals.task_manager.current_item.trigger_qa_field_loading, [])

    def update_presets_frame(self):
        if not self.presets:
            ctx.control("presets_frame").setContentString("")
            return

        tool_buttons = []
        for nr, preset in enumerate(self.presets):
            name = preset.get('__name__', nr + 1)
            tool_buttons.append("""
                ToolButton {{
                    expandX = False
                    name    = previous_task
                    title   = "{name}"
                    tooltip = "Preset {name} (CTRL+{number})"
                    accel   = CTRL+{number}
                    command = "*py:apply_preset({index})*"
                }}
            """.strip().format(index=nr, number=nr + 1, name=name))

        content = """
        Horizontal "TaskNavigation"  {{
            name = task_navigation_bar
            ph = 24
            expandX = False
            Label {{
                title = "Presets:"
            }}
            {tool_buttons}
        }}
        """.format(tool_buttons='\n'.join(tool_buttons))

        ctx.control("presets_frame").setContentString(content)

    def get_contexts_data(self):
        try:
            return self.json['contexts']
        except KeyError:
            return {
                'default': {
                    'viewports': self.json['viewports'],
                    'scans': self.json['scans'],
                    'annotations': self.json['annotations'],
                    'qa_fields': self.json['qa_fields'],
                    'mdl_layout': self.json['mdl_layout'],
                    'initial_position': self.json.get('initial_position', None),
                }
            }


class TemplateManager:
    def __init__(self):
        self._template_cache = {}
        self._log = log.config_logger('templatemanager', level=20, filename=None)

    def get_url(self):
        return ctx.field("TaskmanagerURL").value
    
    def get_template_by_name(self, name):
        if name not in self._template_cache:
            try:
                template = globals.task_manager.task_client.get_template(name)
            except TaskManagerResponseError as exception:
                self._log.error("Got no invalid response from the server: {}".format(exception))
                return None

            self._template_cache[name] = TaskTemplate(template.content)

        return self._template_cache[name]
