import yaml

from . import globals, log
from .helpers import get_username

ctx = globals.ctx


class DataContext(object):
    def __init__(self, name, task_name, template, context_data):
        self._template = template
        self.name = name
        self.task_name = task_name
        self.acquired = False

        self._log = log.config_logger(self.name, level=20, filename=None)
        self._log.info('Creating DataContext {} from: {}'.format(name, context_data))

        self.fields = context_data['fields']
        self.fields_resource = globals.resource_manager.create(
            source_url=context_data['fields_file'],
            type="qafields",
            name="{}_qa_fields".format(self.name),
            callback=self.parse_qa
        )
        if context_data.get('init_fields_file'):
            self.init_fields_resource = globals.resource_manager.create(
                source_url=context_data['init_fields_file'],
                type="qafields",
                name="{}_qa_init".format(self.name),
                callback=self.parse_init_qa
            )
        else:
            self.init_fields_resource = None

        self.init_fields_resource_acquired = False

        # Hold the url of the images to load
        self.scans = {k: globals.resource_manager.create(source_url=v, name=k) for k, v in context_data['scan_sources'].items()}

        annotations = context_data.get('annotations')
        if annotations is None:
            self.annotations = {}
        else:
            self.annotations = {k: globals.resource_manager.create(
                source_url=v,
                type='pixel',
                name=k
            ) for k, v in annotations.items()}

    def __str__(self):
        return '<DataContext {}.{}>'.format(self.task_name, self.name)

    def __repr__(self):
        return str(self)

    @property
    def currently_active(self):
        if not isinstance(globals.task_manager.current_item, Task):
            return False

        return self in globals.task_manager.current_item.contexts.values()

    @property
    def context_template(self):
        return self._template.contexts.get(self.name)

    def acquire(self):
        self._log.info('Acquiring: {} [{}]'.format(self.name, self.acquired))
        if not self.acquired:
            self.acquired = True

            globals.resource_manager.acquire(self.fields_resource)
            for scan in self.scans.values():
                globals.resource_manager.acquire(scan)
            for annotation in self.annotations.values():
                globals.resource_manager.acquire(annotation)

        return self.acquired

    def release(self):
        if self.acquired:
            self._log.debug('Releasing: {}'.format(self.name))
            self.acquired = False

            globals.resource_manager.release(self.fields_resource)
            if self.init_fields_resource_acquired:
                globals.resource_manager.release(self.init_fields_resource)
            for scan in self.scans.values():
                globals.resource_manager.release(scan)
            for annotation in self.annotations.values():
                globals.resource_manager.release(annotation)

    def mask_annotations(self):
        self._log.info('Setting mask_withs')
        if self.context_template.annotations_def is not None:
            for key, annotation in self.context_template.annotations_def.items():
                mask_with = annotation.get('mask_with', None)
                self._log.info('Annotation {} has mask with {}'.format(key, mask_with))
                if mask_with is not None and mask_with in self.annotations:
                    mask = self.annotations[mask_with]
                else:
                    mask = None

                self._log.info('Calling mask with, arg: {}'.format(mask))
                self.annotations[key].mask_with(mask)

    def image_resources(self):
        resources = set(self.scans.values())
        resources.update(self.annotations.values())
        return resources

    @property
    def qa_file_field(self):
        return self.context_template.qa_macro.field('__file__')

    def parse_qa_new(self, resource):
        task = globals.task_manager.current_item

        # First check and verify the fields resource
        if task.fields_resource.downloaded:
            self._log.info('Loading qa fields from {} -> {}'.format(
                task.fields_resource.source_url,
                task.fields_resource.original_filename)
            )
            self.qa_file_field.value = task.fields_resource.original_filename
        elif task.fields_resource.listeners == 0:
            globals.resource_manager.acquire(task.fields_resource, priority=True)
        elif task.init_fields_resource.download_attempted:
            # Now we can consider the initial fields instead
            if task.init_fields_resource.downloaded:
                self._log.info('Loading init qa fields from {} -> {}'.format(
                    task.init_fields_resource.source_url,
                    task.init_fields_resource.original_filename)
                )
                self.qa_file_field.value = task.init_fields_resource.original_filename
            elif not task.init_fields_resource.download_attempted and task.init_fields_resource.listeners == 0:
                globals.resource_manager.acquire(task.init_fields_resource, priority=True)

    def parse_qa(self, resource):
        # This is the callback for the field_resource download, not need to check
        # if it has been attempted otherwise we would not be here
        if resource.downloaded:
            self._log.info('Downloaded {}, checking for load'.format(resource.source_url))
            # Check if the fields resource is the same, if the task changed but the fields
            # was already added to download queue by another task, we still need to apply
            if self.currently_active:
                self._log.info('Loading qa fields from {} -> {}'.format(resource.source_url, resource.original_filename))
                self.qa_file_field.value = resource.original_filename
            else:
                self._log.info('Task {!r} is not current task {!r}. Skipping apply.'.format(self,
                                                                                            globals.task_manager.current_item))
                return
        elif self.init_fields_resource:
            self._log.info('Could not download {}, downloading init file'.format(resource.source_url))
            self.init_fields_resource_acquired = True
            globals.resource_manager.acquire(self.init_fields_resource, priority=True)
        elif self.currently_active:
            self._log.warning('Could not download fields file {}, no initial fields file!'.format(resource.source_url))
            self.qa_file_field.value = ''

    def parse_init_qa(self, resource):
        # This is the callback for the init_field_resource download, not need to check
        # if it has been attempted otherwise we would not be here

        # Check if the fields resource is the same, if the task changed but the fields
        # was already added to download queue by another task, we still need to apply
        if not self.currently_active:
            self._log('DataContext is currently not active.')
            return

        if resource.downloaded:
            self._log.info('Loading initial qa fields from {} -> {}'.format(resource.source_url, resource.original_filename))
            self.qa_file_field.value = resource.original_filename
        else:
            self._log.warning('Could not download initial fields file {}'.format(resource.source_url))
            self.qa_file_field.value = ''

    def trigger_qa_field_loading(self):
        # Load QA fields if available
        self._log.info('## LOAD QA JSON CALLED FROM TEMPLATE')
        if self.fields_resource.downloaded:
            self._log.info('### FIELDS RESOURCE DOWNLOADED, LOAD QA FROM {}'.format(self.fields_resource.original_filename))
            self.qa_file_field.value = self.fields_resource.original_filename
        elif self.init_fields_resource and self.init_fields_resource.downloaded:
            self._log.info('### INIT FIELDS RESOURCE DOWNLOADED, LOAD QA')
            self.qa_file_field.value = self.init_fields_resource.original_filename
        elif self.fields_resource.download_attempted and \
                ((not self.init_fields_resource) or self.init_fields_resource.download_attempted):
            # All downloads were tried, but nothing gave a result, start with empty QA fields
            self.qa_file_field.value = ''
        else:
            self._log.info('### NO RESOURCE DOWNLOADED YET WAIT!')


class Task(object):
    def __init__(self, remote_task=None):
        self._log = log.config_logger(remote_task.uri, level=20, filename=None)
        self._remote_task = remote_task

        # cache variables
        self._sample = None
        self._name = None
        self._content = None
        self._contexts = None

        # Set the template
        template = remote_task.template
        self.template = globals.template_manager.get_template_by_name(template.label)

        self.acquired = False

        # Uploads queued/in progress for this task
        self._uploads = set()
        self._uploading = False
        self._upload_errors = []
        self._upload_message = None

    def __repr__(self):
        return '<Task {} {} [{}]>'.format(self.sample, self.name, self.acquired)

    # helper function to update name and sample (share between two properties)
    def _update_sample_and_name(self):
        name = self.content['name']
        if self.content.get('sample') is None and ' ' in name:
            self._sample, self._name = name.split(' ', 1)
        else:
            self._sample = self.content.get('sample')
            self._name = name

    # properties for easy access
    @property
    def sample(self):
        if self._sample is None:
            self._update_sample_and_name()
        return self._sample

    @property
    def name(self):
        if self._name is None:
            self._update_sample_and_name()
        return self._name

    @property
    def uri(self):
        return self._remote_task.uri

    @property
    def tags(self):
        return self._remote_task.tags

    @property
    def locked_by(self):
        return self._remote_task.lock.get('username')

    @property
    def callback_url(self):
        return self._remote_task.callback_url

    @property
    def content(self):
        if self._content is None:
            content = yaml.safe_load(self._remote_task.content)

            if isinstance(content, list):
                if len(content) != 1:
                    raise NotImplementedError("SubTasks are no longer supported!")

                content = content[0]

            self._content = content

        return self._content

    @property
    def contexts(self):
        if self._contexts is None:
            contexts_data = self.content.get('contexts', None)

            if not contexts_data:
                contexts_data = {
                    'default': {
                        'scan_sources': self.content.get('scan_sources', None),
                        'annotations': self.content.get('annotations', None),
                        'fields': self.content.get('fields', None),
                        'fields_file': self.content.get('fields_file', None),
                        'init_fields_file': self.content.get('init_fields_file', None),
                    }
                }

            self._contexts = {}
            # Create data structure per context
            for context_name, context_data in contexts_data.items():
                print('Processing context: {}'.format(context_name))
                self._contexts[context_name] = DataContext(context_name, self.name, self.template, context_data)

        return self._contexts

    def apply_preset(self, index):
        self.template.apply_preset(index)

    @property
    def loading(self):
        finished_loading = True
        finished_loading &= self.fields_resource.download_attempted

        if self.init_fields_resource and not self.fields_resource.downloaded:
            finished_loading &= self.init_fields_resource.download_attempted
        for resource in self.scans.values():
            finished_loading &= resource.download_attempted
        for resource in self.annotations.values():
            finished_loading &= resource.download_attempted

        return not finished_loading

    def acquire(self):
        self._log.info('Acquiring: {} {} [{}]'.format(self.sample, self.name, self.acquired))
        if not self.acquired:
            self._log.debug('Locking for acquisition: {} {}'.format(self.sample, self.name))
            acquired_lock = self.acquire_lock()
            self._log.info('Acquiring lock {}'.format('succeeded' if acquired_lock else 'failed'))

            if acquired_lock:
                self.acquired = True
                for data_context in self.contexts.values():
                    data_context.acquire()
            else:
                self.acquired = False

        return self.acquired

    def release(self):
        if self.acquired:
            self._log.debug('Releasing: {} {}'.format(self.sample, self.name))
            self.release_lock()

            self.acquired = False
            for data_context in self.contexts.values():
                data_context.release()

    def image_resources(self):
        resources = set()
        for data_context in self.contexts.values():
            resources.update(data_context.image_resources())
        return resources

    def check_qc_ready(self):
        for context in self.template.contexts.values():
            if not context.check_qc_ready():
                return False
        return True

    def trigger_qa_field_loading(self):
        for context in self.contexts.values():
            context.trigger_qa_field_loading()

    def perform_validation_actions(self):
        for context in self.template.contexts.values():
            context.perform_validation_actions()

    def acquire_lock(self):
        self._log.debug('Locking {}'.format(self._remote_task.uri))
        return self._remote_task.change_lock(get_username())

    def release_lock(self):
        self._log.debug('UnLocking {}'.format(self._remote_task.uri))
        return self._remote_task.change_lock(None)

    def mask_annotations(self):
        for context in self.contexts.values():
            context.mask_annotations()

    def save(self, new_task, finish):
        if self._uploading:
            return

        self._uploading = True
        self._upload_errors = []

        ctx.field("EnableNavigation").value = False

        for context in self.template.contexts.values():
            resource = context.save_qa_json()
            self._uploads.add(resource)
            resource.sync(callback=self.upload_finished, kwargs={'task': new_task, 'finish': finish})

        self.update_upload_message('Uploading... {} items left'.format(len(self._uploads)))

    def update_upload_message(self, message=None):
        if self._upload_message is not None:
            self._log.info('===> DIMISS {}'.format(self._upload_message))
            globals.errors.dismiss_error_by_value(self._upload_message, 'message')

        self._upload_message = message

        if message is not None:
            self._log.info('===> ADD {}'.format(self._upload_message))
            globals.errors.add_error(message, 'message')

    def upload_finished(self, upload_resource):
        # Do something checking / error handling
        self._log.info('UPLOAD_FINISHED')

        if not upload_resource.success:
            for error in upload_resource.errors:
                self._log.error("[UPLOAD ERROR] {}".format(error))
                self._upload_errors.append(error)

        # Remove from upload set
        self._uploads.remove(upload_resource.resource)

        if len(self._uploads) > 0:
            self.update_upload_message('Uploading... {} items left'.format(len(self._uploads)))
            return

        self._uploading = False
        ctx.field("EnableNavigation").value = True
        self.update_upload_message(None)

        # Abort on failed upload
        if self._upload_errors:
            globals.errors.add_error('Encountered a problem uploading the data!', 'error', details='\n'.join(self._upload_errors))
            return

        # Display result
        globals.errors.add_error('Upload successfully finished! (Validated)', 'message', 1.0)

        # Change task if required using the kwargs of the upload
        task = upload_resource.kwargs.get('task')
        if isinstance(task, (int, str)):
            finish = upload_resource.kwargs.get('finish')
            globals.task_manager.init_task_transition(task, finish=finish)

    @property
    def substitution_vars(self):
        return self.content.get('vars')

    @property
    def lazy_status(self):
        return self._remote_task.status

    @property
    def status(self):
        self._remote_task.clearcache()
        return self._remote_task.status

    @status.setter
    def status(self, value):
        if self.status == value:
            return

        self._remote_task.status = value
        self._remote_task.clearcache()  # FIXME: taskmanager-client should do this!
        if value == 'done':
            globals.errors.add_error("Task for session {} is done! All results will be shortly available in XNAT.".format(
                globals.MAIN_WINDOW.control("session_id").title()
            ), "message", lifetime=10.0)

        ctx.field("UpdateTaskList").touch()
