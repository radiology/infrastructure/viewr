# ----------------------------------------------------------------------------

# 
#  \file    ViewR.py
#  \author  Marcel Koek
#  \date    2015-12-01
#
#  

# ----------------------------------------------------------------------------

from PythonQt.QtGui import QColor, QIcon, QPixmap

import datetime
import os
from collections import OrderedDict
import sys

import markdown

from . import viewr
viewr.init(ctx)

from .viewr import globals
from .viewr.config import config, login_info

# Note that the helpers ARE needed as they are used from script files elsewhere
# out of the view of IDEs. Do NOT remove unused imports here
from .viewr.helpers import get_viewport, starting_template, load_tags, get_control, get_context

# These are field listener endpoints, they HAVE to be imported here
from .viewr.helpers import viewers_switch_fieldlistener
from .viewr.helpers import viewport_set_initial_position
from .viewr.helpers import viewport_position_update_required


from .viewr.task import Task

ctx = globals.ctx
MLAB = globals.MLAB
logger = globals.logger


def init_main_window():
    logger.info("Main Window is initialized")
    globals.MAIN_WINDOW = ctx.window()
    
    # Set the window icon.
    window_icon = QIcon(ctx.expandFilename("$(MLAB_EMC_ViewR)/Modules/Resources/Icons/ViewR.ico"))
    ctx.window().widget().setWindowIcon(window_icon)
    
    # Set task navigation bar to invisible
    ctx.field("ShowTaskNavigation").value = False
    
    read_config_file()

    # Load the starting template.
    starting_template()


def close_window():
    logger.info("Closing main window... clean up (current task {})".format(globals.task_manager.current_item))

    # Remove error popup exception handler
    sys.excepthook = sys.__excepthook__

    if globals.task_manager.current_item is not None and isinstance(globals.task_manager.current_item, Task):
        logger.info("Cleaning current task")
        globals.task_manager.current_item.template.remove_contexts()
        globals.task_manager.current_item.release()

    globals.task_manager.release_all()


def close_auth_window():
    globals.task_manager.auth_window.close()


def read_config_file():
    for loc in os.path.expanduser("~/.viewr"), os.environ.get("VIEWR_CONFIG_DIR"):
        try:
            if loc:
                source = os.path.normpath(os.path.join(loc, "viewr.json"))
                config.read_config(source)
        except IOError:
            try:
                os.makedirs(os.path.dirname(source))
            except:
                pass
              
            try:
                os.mknod(source)
            except AttributeError:
                with open(source, 'w') as fout:
                    fout.write("{}")
            config.filename = source


def marker_style_listener():
    ctx.field("SoView2DMarkerEditor.currentType").value = ctx.field("StylePalette.currentStyle").value - 1


def next_task(force_skip=False, finish=True):
    """
    Function called by buttons for going to the next task

    :param bool force_skip: Skip without checking and saving the QA fields
    :param bool finish: Mark the current task as finished
    """
    change_task('NEXT', force_skip, finish)


def previous_task(force_skip=False, finish=False):
    """
    Function called by buttons for going to the previous task

    :param bool force_skip: Skip without checking and saving the QA fields
    :param bool finish: Mark the current task as finished
    """
    change_task('PREVIOUS', force_skip, finish)


def jump_to_task(item):
    """
    Function called by buttons for going to the previous task

    :param MLABListViewItem item: The task to jump to
    """
    print('JUMP TO TASK ITEM: [{}] {}'.format(type(item).__name__, item))

    try:
        task_id = int(item.text(0))
    except ValueError:
        logger.info("Cannot jump to task parent! Please jump to an actual task.")
        return

    print('TASK ID: {}'.format(task_id))

    if isinstance(globals.task_manager.current_item, Task):
        result = ctx.showModalDialog('JumpConfirm')
    else:
        # No need to save, no task really loaded
        result = 2
        
    print('MODEL RESULT: {}'.format(result))

    if result == 1:
        # First save than jump
        change_task(task_id, force_skip=False, finish=False)
    elif result == 2:
        # Jump without saving
        change_task(task_id, force_skip=True, finish=False)


def change_task(task, force_skip, finish):
    logger.info('CHANGE TASK TO {}, FORCE {}, FINISH {}'.format(task, force_skip, finish))
    # We have to work with indexes of tasks from here on
    if isinstance(task, int):
        task = globals.task_manager[task]

    if isinstance(task, Task) and finish:
        logger.warning('Cannot change to a specific task and finish the task')
        return

    # Validate task value
    if not (isinstance(task, Task) or task in ('START', 'END', 'NEXT', 'PREVIOUS')):
        logger.error('Cannot change to invalid task value: [{}] {}'.format(
            type(task).__name__,
            task
        ))
        return

    # Start transition if editor is not activated, check if upload is required
    current_task = globals.task_manager.current_item
    if globals.editor.deactivate_editor():
        if force_skip or isinstance(current_task, str):
            globals.task_manager.init_task_transition(task, finish=finish)
        elif (not finish) or current_task.check_qc_ready():

            # If finishing the task, take actions from validation controls
            if finish:
                current_task.perform_validation_actions()

            # Re-check next safe item because the validation actions could
            # have changed the status of (parent) tasks
            current_task.save(new_task=globals.task_manager.check_safety_new_task(task),
                              finish=finish)
        else:
            MLAB.showWarning("Please fill-out all quality ratings.", "", "Quality Ratings")


def change_to_task(task_index=None):
    """
    Function to start changing of the task, should be called via ChangeToTask field.
    If called with an task_index argument, it will set the field and let itself be
    called via the FieldListener

    :param int task_index: optional task_index to change to, will call itself
                           again via FieldListener
    """
    # If changing by code, call field and handle from there
    # TODO: This needs to be done differently: update_loading_message(True)

    if task_index is not None and isinstance(task_index, int):
        ctx.field('ChangeToTask').value = task_index
        return

    # Get task index from field
    task_index = ctx.field('ChangeToTask').value

    # Get relevant task objects from task manager
    old_task = globals.task_manager.current_item
    new_task = globals.task_manager[task_index]

    # Transition task manager
    logger.info('---> Setting new task: {}'.format(new_task))
    globals.task_manager.current_item = new_task
    logger.info('===> Transitioning {} --> {}'.format(old_task, globals.task_manager.current_item))
    globals.task_manager.transition(globals.task_manager.current_item, old_task)

    # Load task in the GUI if needed
    if old_task is not new_task:
        logger.debug('Previous task: {}, new task: {}'.format(old_task, new_task))
        load_task(new_task)


def open_task_browser():
    # Make sure tag filter are set
    update_tags()

    # Load data
    if len(globals.task_manager) == 0:
        globals.task_manager.init_data(tags=SELECTED_TAGS, transition_to_start=False)
    
    # Open task window
    ctx.showWindow("Tasks", ctx.window())

    
def start_with_tasks():
    # Make sure tag filter are set
    update_tags()

    # Load data
    globals.task_manager.init_data(tags=SELECTED_TAGS)

    # Find first task and transition
    next_task(force_skip=True, finish=False)


def update_tags():
    global SELECTED_TAGS
    if globals.MAIN_WINDOW.control("tag_list") is not None:
        tags = []
        for idx in range(globals.MAIN_WINDOW.control("tag_list").countItems()):
            if globals.MAIN_WINDOW.control("tag_list").isItemSelected(idx):
                tags += [globals.task_manager.tags_map[globals.MAIN_WINDOW.control("tag_list").itemText(idx)]]
        if len(tags) == 0:
            tags = None

        SELECTED_TAGS = tags


def load_task(task=None):
    logger.info('CALL load_task')
    if globals.editor.deactivate_editor():
        # Show task navigation bar
        ctx.field("ShowTaskNavigation").value = True
        ctx.field("UpdateTaskList").touch()

        logger.info('==> TASK = {}'.format(task))

        if task is None or (not isinstance(task, Task) and task not in ['START', 'END']):
            logger.info('SET CURRENT ITEM AS TASK')
            task = globals.task_manager.current_item

        if task is None or not isinstance(task, Task):
            logger.info('return')
            return
        else:
            logger.info('apply template')
            set_session_id(task.sample)
            ctx.field("TaskLoaded").value = datetime.datetime.now().isoformat()
            ctx.callLater(0.0, task.template.apply, [])

            # Trigger the loading of the qa_field's (This has to be done after the task_layout is set)
            # MOVED TO TEMPLATE TO ENSURE IT WILL BE DONE AFTER THE QA FIELDS ARE SET
            # ctx.callLater(0.5, task.trigger_qa_field_loading, [])


def update_loading_message(loading=None):
    if loading is None:
        loading = globals.task_manager.current_item.loading

    for context in globals.task_manager.current_item.contexts.values():
        for viewport in context.viewport_modules.values():
            viewport.set_loading(loading)


def go_home(force=False):
    if not force:
        proceed = MLAB.showWarning("Are you sure you want to return to the start screen?\nIf you are not sure what to do press \"Cancel\"", "Return to Home", ["Cancel", "Proceed"], 0)
    else:
        proceed = True

    if proceed:
        globals.task_manager.clear()
        ctx.callLater(0.0, starting_template, [])


def update_servers():
    # Get connected combobox
    combobox = ctx.control("server_selector")
    if not combobox:
        return

    # Remember selection
    current_selection = combobox.currentText()

    # Set items
    items = [x['name'] for x in config.servers]
    combobox.setItems(items)

    # Set correct selection after change
    try:
        current_index = items.index(current_selection)
    except ValueError:
        current_index = 0

    combobox.setCurrentItem(current_index)

    # Load data from current selection
    config.select_server()


def reset_tags_filter():
    control = globals.MAIN_WINDOW.control("tag_list")
    if not control:
        return

    for index in range(control.countItems()):
        control.selectItem(index, False)


def test_server():
    config.test_server()


def task_tree(extended: bool =None,
              force_task_reload: bool = False):
    """
    Refresh the task list window

    :param bool extended: Flag to control if the status and lock will be show,
                          those are slow to retrieve and will make the refresh
                          costly.
    """
    logger.info(f"task_tree called extended={extended}  force_task_reload={force_task_reload}")

    try:
        w = ctx.control("task_window")
    except:
        return

    if extended is None:
        extended = ctx.field("TaskListExtended").value

    if force_task_reload:
        globals.task_manager.init_data(tags=SELECTED_TAGS, transition_to_start=False)

    task_list_view = ctx.control("task_overview")
    task_list_view.clearItems()
    task_list_view.removeColumns()
    task_list_view.addColumn("Id")
    task_list_view.addColumn("Name")
    task_list_view.addColumn("Sample")
    task_list_view.addColumn("Template")
    task_list_view.addColumn("Status")
    if extended:
        task_list_view.addColumn("Locked by")
    task_list_view.addColumn("Tags")

    for id, task in enumerate(globals.task_manager):
        if extended:
            task_item = task_list_view.appendItem(
                None, [
                    id,
                    task.name,
                    task.sample,
                    task.template.name,
                    task.lazy_status,
                    task.locked_by,  # TODO: Fix the lock retrieval?
                    ", ".join(x.name for x in task.tags)
                ]
            )
        else:
            task_item = task_list_view.appendItem(
                None, [
                    id,
                    task.name,
                    task.sample,
                    task.template.name,
                    task.lazy_status,
                    ", ".join(x.name for x in task.tags)
                ]
            )

        task_item.setToolTip(0, task.uri)

    task_list_view.resizeColumnsToContents()
    update_task_list_colors()


CURRENT_ITEM_COLOR = QColor(20, 220, 240)
COLOR_MAP = {
    'done': QColor(0, 210, 0),
    'aborted': QColor(210, 50, 0),
    'queued': QColor(225, 225, 235),
}


def update_task_list_colors():
    # Try to get the task overview control
    try:
        task_list_view = ctx.control("task_overview")
    except:
        return

    for task_item in task_list_view.topLevelItems():
        status = task_item.value(4)  # Get status from 4th column entry
        color = COLOR_MAP.get(status, None)
        if int(task_item.value(0)) == globals.task_manager.current_index():
            print(f'Hit current item with {task_item}')
            for count in range(task_list_view.columnCount()):
                task_item.setBackgroundColor(count, CURRENT_ITEM_COLOR)
        elif color is not None:
            for count in range(task_list_view.columnCount()):
                task_item.setBackgroundColor(count, color)


def update_rater_history(context_name):
    rater_history(show=True, update_view=True, context_name=context_name)


def rater_history(show=False, update_view=False, context_name=None):
    logger.info("rater_history called")

    tree = OrderedDict()

    try:
        window = ctx.control("rater_history_window")
    except:
        return tree

    context_switch = window.control("rater_history_context_switch")
    current_task = globals.task_manager.current_item

    if isinstance(current_task, str) or current_task is None:
        globals.logger.warning('Cannot get context, no Task is currently loaded!')
        return

    context_names = tuple(current_task.template.contexts.keys())

    if context_switch.getButtonNames() != context_names:
        context_switch.removeAllButtons()
        for name in context_names:
            context_switch.addButton(name, name, QPixmap())

    if context_name is None:
        for button_name in context_switch.getButtonNames():
            if context_switch.isButtonChecked(button_name):
                context_name = button_name
                break
        else:
            context_name = context_names[0]

    context = get_context(context_name)

    if update_view:
        rater_history_list = window.control("rater_history")
        rater_history_list.clearItems()
        rater_history_list.removeColumns()
        rater_history_list.addColumn("Date")
        rater_history_list.addColumn("Time")
        rater_history_list.addColumn("Rater")

        for item in context.rater_history:
            date = item['timestamp'][:10]
            time = item['timestamp'][11:19]
            rater = item['username']

            list_item = rater_history_list.insertItem(None, [date, time, rater])
            list_item.setToolTip(0, "change by {} at {} {}".format(rater, date, time))
        rater_history_list.resizeColumnsToContents()
    return tree


def toggle_annotations():
    active_context = ctx.field("ActiveContext").value
    active_viewport = ctx.field("ActiveViewPort").value
    viewport = get_viewport(active_context, active_viewport)
    toggels = [
        viewport.show_overlay_field.value,
        viewport.show_marker_field.value,
        viewport.show_csos_field.value,
    ]

    viewport.show_overlay_field.value = not all(toggels)
    viewport.show_marker_field.value = not all(toggels)
    viewport.show_csos_field.value = not all(toggels)


def toggle_overlays():
    active_context = ctx.field("ActiveContext").value
    active_viewport = ctx.field("ActiveViewPort").value
    viewport = get_viewport(active_context, active_viewport)
    viewport.show_overlay_field.value = not viewport.show_overlay_field.value


def toggle_markers():
    active_context = ctx.field("ActiveContext").value
    active_viewport = ctx.field("ActiveViewPort").value
    viewport = get_viewport(active_context, active_viewport)
    viewport.show_marker_field.value = not viewport.show_marker_field.value


def toggle_csos():
    active_context = ctx.field("ActiveContext").value
    active_viewport = ctx.field("ActiveViewPort").value
    viewport = get_viewport(active_context, active_viewport)
    viewport.show_csos_field.value = not viewport.show_csos_field.value


def toggle_localizer():
    ctx.field("ShowLocalizer").value = show_localizer = not ctx.field("ShowLocalizer").value
    for context in globals.task_manager.current_item.template.contexts.values():
        for vp in context.viewport_modules.values():
            vp.module.field("ShowLocalizer").value = ctx.field("ShowLocalizer").value

            # Turn of updating when no localizer is present
            position = vp.module.module("position")

            # Connect / disconnect localizer to slice
            if show_localizer:
                position.field("updateOnPress").value = True
                position.field("updateOnMotion").value = True
            else:
                position.field("updateOnPress").value = False
                position.field("updateOnMotion").value = False


def show_changelog():
    md_file = ctx.expandFilename("$(MLAB_EMC_ViewR)/CHANGELOG")
    with open(md_file, 'r') as fin:
        md_text = fin.read()
    html_text = markdown.markdown(md_text)
    ctx.field('ChangeLog').value = html_text

    ctx.showWindow("ChangeLog", ctx.window())


def apply_preset(index):
    if not isinstance(globals.task_manager.current_item, Task):
        logger.warning('Cannot apply preset {}, no task loaded!'.format(index))
        return

    globals.task_manager.current_item.apply_preset(index)


def show_about():
    about_text = """
<h1>ViewR (version {})</h1>
<h3>by BIGR</h3>

<p>ViewR is a viewing and annotation tool created by the departament of radiology in the Erasmus MC</p>
""".format(globals.version)

    ctx.field('About').value = about_text

    ctx.showWindow("About", ctx.window())


def set_session_id(session_id):
    ctx.control("session_id").setTitle(session_id)
    ctx.control("session_id").setStyleSheetFromString(globals.TITLE_STYLE)


def set_task_name(task_name):
    ctx.control("task_name").setTitle(task_name)
    ctx.control("task_name").setStyleSheetFromString(globals.TITLE_STYLE)
