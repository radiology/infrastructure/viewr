=====
ViewR
=====

The ViewR is a image viewer for presenting inspection and manual annotation
tasks to the user based on task list. The tasks are retrieved from a
taskmanager server and images from an XNAT server.

--------------------
General architecture
--------------------

The ViewR is not a stand-alone application, but rather it works together with
the taskmanager and an XNAT server. This is schematically shown in the following
figure:

.. image:: images/viewr_communication_overview.svg

When a user starts the ViewR, it connects to a taskmanager. From there it will
retrieve a task list. The task includes information about:

 * Which task template the ViewR should used
 * Which scans the ViewR should load
 * Where the ViewR should store the resulting information

The task template is a file that describes the required layout of the ViewR for
a particular group of tasks. This is described in detail in :ref:`templates <task_templates_reference>`.

Once a task is to be loaded the ViewR will:

* Download the template (if not yet on the client)
* Apply the layout described in the template
* Download the files required to execute the task
* Present the downloaded resource files in the ViewR


