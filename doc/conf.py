# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import os
import sys

# -- Add ViewR path ----------------------------------------------------------
# THIS DOES NOT WORK WITHOUT MEVISLAB, BUT LETS NOT TIE THE DOCS TO MEVISLAB!
# path = os.path.join(os.path.dirname(__file__), '..', 'EMC', 'ViewR', 'Modules', 'Macros', 'EMCViewR')
# path = os.path.abspath(path)
# print('ViewR Python code located in {path}'.format(path=path))

# sys.path.append(path)

# from viewr import globals
# print('Found control dict: {}'.format(globals.control_dict))

# -- Project information -----------------------------------------------------

project = 'viewr'
copyright = '2019, H.C. Achterberg, M.Koek'
author = 'H.C. Achterberg, M.Koek'

# The full version, including alpha/beta/rc tags
release = '6.4.0'

# Read the docs hacks
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
if on_rtd:
    print('[conf.py] On Read the Docs')

# -- General configuration ---------------------------------------------------

master_doc = 'index'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
if not on_rtd:
    import sphinx_rtd_theme
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
else:
    html_theme = 'default'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = 'static/images/logo.svg'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = 'static/images/favicon.ico'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['static/sources']

html_css_files = ['extra.css']


# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'https://docs.python.org/3/': None}
