.. viewr documentation master file, created by
   sphinx-quickstart on Tue Oct 29 09:37:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to viewr's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   static/introduction.rst
   static/templates.rst
   static/widgets.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
